import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GradingUiComponent} from './grading-ui/grading-ui.component';
import {GradingReviewUIComponent} from './grading-ui/grading-review-ui/grading-review-ui.component';
import {CurrentExposureUiComponent} from './grading-ui/current-exposure-ui/current-exposure-ui.component';
import {RemarksUiComponent} from './grading-ui/remarks-ui/remarks-ui.component';
import {GradingBoxComponent} from './grading-ui/grading-box/grading-box.component';
import {SharedModule} from '../shared/shared.module';
import {SimplebarAngularModule} from 'simplebar-angular';
import {NewsReaderComponent} from './grading-ui/news-reader/news-reader.component';
import {CompanyRelationsComponent} from './grading-ui/company-relations/company-relations.component';
import {CommentBoxComponent} from './grading-ui/comment-box/comment-box.component';
import {CompanyDashboardComponent } from './grading-ui/company-dashboard/company-dashboard.component';
import {GradingHomeComponent} from './home/grading-home.component';


@NgModule({
  declarations: [
    GradingHomeComponent,
    GradingUiComponent,
    GradingReviewUIComponent,
    CurrentExposureUiComponent,
    RemarksUiComponent, GradingBoxComponent,
    NewsReaderComponent, CompanyRelationsComponent, CommentBoxComponent, CompanyDashboardComponent],
  imports: [
    CommonModule,
    SharedModule,
    SimplebarAngularModule,
  ]
})
export class GradingModule {
}
