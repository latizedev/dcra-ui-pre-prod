import {Component, Input, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {CompanyRelation, CytoElement} from '../../../classes';
import {Node, Edge, ClusterNode} from '@swimlane/ngx-graph';
import {NodeX} from './node-x';
import {faChevronDown, faStar} from '@fortawesome/free-solid-svg-icons';
import {VisualMakerService} from '../../../services/visual-maker.service';

@Component({
  selector: 'app-company-relations',
  templateUrl: './company-relations.component.html',
  styleUrls: ['./company-relations.component.scss']
})

export class CompanyRelationsComponent implements OnInit {

  @Input() company: string;
  @Input() relations: CompanyRelation[];

  cytoElements: CytoElement;

  nodes: Node [];

  icons = {
    generic: 'star-of-life',
    Location: 'map-marker-alt',
    Product: 'cube',
    Transaction: 'exchange-alt',
    Company: 'building',
    literal: 'hashtag',
    Industry: 'hard-hat',
    chevron: faChevronDown,
  };

  showChildFor: string[] = []; // array of relations for which children should be added

  links: Edge [];
  ready = false;
  isCollapsed: {
    [key: string]: boolean
  } = {};

  graphType: 'NgxGraph' | 'Cytoscape' = 'Cytoscape';

  constructor(
    private vsm: VisualMakerService
  ) {
  }

  createNodesAndLinks(): void {
    this.ready = false;
    this.nodes = [];
    this.links = [];
    this.nodes.push({
      id: '0',
      label: this.company,
      data: {
        class: 'Company',
        cluster: false,
        size: 30,
        nodeType: 'source'
      }
    });
    this.relations.forEach((r, i) => {
      const target = r.values[0];

      if (r.values.length > 1) { // cluster nodes
        this.nodes.push({
          id: (i + 1).toString(),
          label: (r.relation.class || r.relation.id) + ' &times; ' + r.values.length,
          data: {
            relation: r.relation.id,
            class: r.relation.class,
            size: 30,
            nodeType: 'cluster'
          }
        });
        if (this.showChildFor.indexOf(r.relation.id) < 0) {

        } else {

          r.values.forEach((v, x) => {
            this.nodes.push({
              id: ((i + 1).toString()) + (x + 1).toString(),
              label: v.label || v.id,
              data: {
                relation: r.relation.id,
                class: r.relation.class,
                size: 20,
                nodeType: 'cluster-instance'
              }
            });
            this.links.push({
              id: 'l' + ((i + 1).toString()) + (x + 1).toString(),
              source: (i + 1).toString(),
              target: ((i + 1).toString()) + (x + 1).toString(),
            });
          });

        }

      } else { // single nodes
        this.nodes.push({
          id: (i + 1).toString(),
          label: target.label || target.id,
          data: {
            relation: r.relation.label || r.relation.id || '',
            class: r.relation.class,
            size: 20,
            nodeType: 'instance'
          }
        });
      }
      this.links.push({
        id: 'l' + (i + 1) + target.id,
        source: '0',
        target: (i + 1).toString(),
        // label: r.relation.label || r.relation.id || '',
      });
    });
    this.nodes.forEach(n => {
      n.dimension = {
        width: 100,
        height: 100
      };
      // n.data.labelParts = n.label.split(' ');
    });
    // console.log(this.nodes, this.links);

    this.cytoElements = this.vsm.createCytoNetwork(this.company, this.relations);
    console.log(this.cytoElements);
    this.ready = true;
  }

  NodeClicked(node: Node): void {
    console.log(node);
    if (node.data.nodeType === 'cluster') {
      // console.log(this.showChildFor.indexOf(node.data.relation));
      // console.log(this.showChildFor);
      if (this.showChildFor.indexOf(node.data.relation) < 0) {
        this.showChildFor.push(node.data.relation);
        this.isCollapsed[node.data.id] = false;
      } else {
        this.showChildFor.splice(this.showChildFor.indexOf(node.data.relation), 1);
        this.isCollapsed[node.data.id] = true;
      }
      // console.log(this.showChildFor);
      this.createNodesAndLinks();
    } else {

    }
  }

  ngOnInit(): void {
    console.log(this.relations);
    this.createNodesAndLinks();
    this.relations.forEach(r => {
      this.isCollapsed[r.relation.id] = true;
    });
  }


}
