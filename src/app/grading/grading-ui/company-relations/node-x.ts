import { Node, Edge, ClusterNode } from '@swimlane/ngx-graph';

export class NodeX extends Node {
  class: string;
  cluster: boolean;
  size: number;
}
