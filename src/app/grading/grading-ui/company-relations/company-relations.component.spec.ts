import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyRelationsComponent } from './company-relations.component';

describe('CompanyRelationsComponent', () => {
  let component: CompanyRelationsComponent;
  let fixture: ComponentFixture<CompanyRelationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyRelationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyRelationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
