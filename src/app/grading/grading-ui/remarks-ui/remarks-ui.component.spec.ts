import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemarksUiComponent } from './remarks-ui.component';

describe('RemarksUiComponent', () => {
  let component: RemarksUiComponent;
  let fixture: ComponentFixture<RemarksUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemarksUiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemarksUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
