import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GradingUiComponent } from './grading-ui.component';

describe('GradingUiComponent', () => {
  let component: GradingUiComponent;
  let fixture: ComponentFixture<GradingUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GradingUiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GradingUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
