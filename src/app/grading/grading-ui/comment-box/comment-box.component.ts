import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {ApiOverrideData, ApiOverrideLog, DcraComment, DcraUser} from '../../../classes';
import {faCheck, faFilter, faTimes, faTrashAlt, faUser} from '@fortawesome/free-solid-svg-icons';
import {Comment} from "@angular/compiler";
import {DataService} from "../../../services/data.service";

@Component({
  selector: 'app-comment-box',
  templateUrl: './comment-box.component.html',
  styleUrls: ['./comment-box.component.scss']
})
export class CommentBoxComponent implements OnInit {
  @Input() comments: DcraComment[];
  @Output() commentAdded = new EventEmitter<string>();
  @Output() commentDeleted = new EventEmitter<number>();

  @Input() currentUser: DcraUser;
  @Input() override: ApiOverrideData;
  newComment: string;
  icons = {
    user: faUser,
    save: faCheck,
    cancel: faTimes,
    filter: faFilter,
    delete: faTrashAlt
  };

  constructor(
    private ds: DataService
  ) {
  }

  addComment(): void {
    /*this.comments.push({
      date: new Date(),
      ratingFrom: '',
      text: this.newComment,
      userName: this.currentUser.userName
    });*/
    this.commentAdded.emit(this.newComment);
    this.newComment = null;
  }

  deleteComment(i: number): void {
    this.commentDeleted.emit(i);
  }

  cancelComment(): void {
    this.newComment = null;
  }

  ngOnInit(): void {
    if (this.override) {
      this.comments = [];
      if (this.override.auditLog && this.override.auditLog.length) {


        this.override.auditLog.forEach((a, i) => {

          /*
    createdAt: string; // "2021-06-22T10:10:21.04413+00:00"
    currentStep: number;
    finalizedMasGrading: number;
    id: number;
    justification: string; // "seems ok with me"
    modifiedBy: string; // "guest1@zflexsoftware.com"
    status: string; // "Approved"
    updatedAt: string; // "2021-06-22T10:10:35.579+00:00
    */
          if (i || a.status !== 'Awaiting') {

            let comment: DcraComment = {
              ratingFrom: a.status,
              ratingTo: this.ds.gradingNumberToString(a.finalizedMasGrading),
              userName: a.modifiedBy,
              text: a.justification,
              date: new Date(a.updatedAt)
            }
            this.comments.unshift(comment);
          }
        })
      }
      this.comments.unshift({
        ratingFrom: this.ds.gradingNumberToString(this.override.calculatedMasGrading),
        ratingTo: this.ds.gradingNumberToString(this.override.finalizedMasGrading),
        userName: this.override.createdBy,
        text: this.override.justification,
        date: this.override.auditLog ? new Date(this.override.auditLog[0].createdAt) : null
      })
    }
  }

}
