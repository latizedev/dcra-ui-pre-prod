import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GradingBoxComponent } from './grading-box.component';

describe('GradingBoxComponent', () => {
  let component: GradingBoxComponent;
  let fixture: ComponentFixture<GradingBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GradingBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GradingBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
