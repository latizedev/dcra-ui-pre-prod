import {Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {
  faUndoAlt,
  faStar,
  faCog,
  faCrown,
  faThumbsUp,
  faThumbsDown,
  faList,
  faRecycle,
  faTrash
} from '@fortawesome/free-solid-svg-icons';
import {ApiOverrideData, DcraUser} from "../../../classes";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-grading-box',
  templateUrl: './grading-box.component.html',
  styleUrls: ['./grading-box.component.scss']
})
export class GradingBoxComponent implements OnInit, OnChanges {

  faUndoAlt = faUndoAlt;
  faStar = faStar;
  faCog = faCog;
  faList = faList;
  faCrown = faCrown;
  faThumbsUp = faThumbsUp;
  faCancel = faTrash;
  faThumbsDown = faThumbsDown;

  currentUser: DcraUser;

  gradingOptions: ('1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS')[] = ['1.PASS', '2.SM', '3.SS', '4.DF', '5.LOSS'];

  @Input() type: 'previous' | 'recommended' | 'review' | 'final';
  @Input() grade: '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS';
  @Input() overrideData: ApiOverrideData;
  @Input() canAssign = false;
  @Input() canApprove = false;
  @Output() gradingUpdated = new EventEmitter<string>();

  status: string;

  iconMap = {
    previous: faUndoAlt,
    review: faStar,
    recommended: faCog,
    final: faCrown,
  };

  constructor(
    private users: UserService
  ) {
  }

  updateGrading(action: '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS' | 'approve' | 'reject' | 'cancel'): void {
    console.log(action);
    if (action !== 'approve' && action !== 'reject' && action !== 'cancel') {
      this.grade = action;
    }
    this.gradingUpdated.emit(action);
  }

  ngOnInit(): void {

    this.currentUser = this.users.getActiveUser();
    // console.log('logged in as', this.currentUser);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.grade);
    if (this.overrideData) {
      console.log(this.overrideData.auditLog);

      this.status =  this.overrideData.auditLog ? (this.overrideData.auditLog.length ? this.overrideData.auditLog[0].status : '') : '';
    }
  }

}
