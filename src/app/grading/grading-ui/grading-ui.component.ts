import {Component, OnInit} from '@angular/core';
import {
  ApiOverrideData,
  ApiOverrideResponse,
  CompanyData,
  CompanyRow,
  DcraUser,
} from '../../classes';
import {
  faBalanceScale,
  faListUl,
  faNewspaper,
  faProjectDiagram,
  faThLarge,
  faTrashRestore,
  faSave,
  faUndoAlt, faComments, faExternalLinkAlt, faFlag,  faStarHalfAlt
} from '@fortawesome/free-solid-svg-icons';
import {faFlag as faFlag2, faThumbsUp, faStarHalf} from '@fortawesome/free-regular-svg-icons';
import {UserService} from '../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DataService} from '../../services/data.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ApiService} from '../../services/api.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-grading-ui',
  templateUrl: './grading-ui.component.html',
  styleUrls: ['./grading-ui.component.scss']
})

export class GradingUiComponent implements OnInit {

  theme: 'light' | 'dark' = 'light';

  companyId: string;
  // overrideId: number;
  overrideStatus;

  faBalanceScale = faBalanceScale;
  faListUl = faListUl;
  faStar = faStarHalf;
  faNewspaper = faNewspaper;
  faProjectDiagram = faProjectDiagram;
  faThLarge = faThLarge;
  faRestore = faTrashRestore;
  faSave = faSave;
  faUndo = faUndoAlt;
  faComments = faComments;
  faLink = faExternalLinkAlt;
  faFlag = faFlag;
  faNoFlag = faFlag2;
  faApprove = faThumbsUp;

  saving = false;
  updated = false;

  currentUser: DcraUser;

  canAssign = false;
  canApprove = false;

  lastReviewScore: '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS';

  gradings: {
    type: 'previous' | 'recommended' | 'review' | 'final',
    approvedBy: number[],
    assignedBy: number[]
  } [] = [{
    type: 'previous',
    approvedBy: [],
    assignedBy: []
  }, {
    type: 'recommended',
    approvedBy: [],
    assignedBy: []
  }, {
    type: 'review',
    approvedBy: [],
    assignedBy: []
  }, /*{
    grade: 'analyst',
    judgedBy: 'R2',
    reviewedBy: 'R1'
  }, {
    grade: 'supervisor',
    judgedBy: 'R3',
    reviewedBy: 'R2'
  },*/ {
    type: 'final',
    approvedBy: [],
    assignedBy: []
  }];
  overrideData: ApiOverrideData;

  tabs = ['grading review', 'current exposure', 'news', 'dashboard']; //'explore',
  activeTab = 'grading review';
  tabsToIcons = {
    'grading review': faListUl,
    'current exposure': faBalanceScale,
    news: faNewspaper,
    explore: faProjectDiagram,
    dashboard: faThLarge
  };

  company: CompanyData;
  companies: CompanyData[];
  companyBeforeUpdate: string;

  gradingFrom: string;
  gradingTo: string;
  gradingAllowed = true;

  gradingOverrideComment: string;
  overrideAction: string;
  submittingOverride = false;

  showComments = false;

  showReport = false;
  reportStatus: string;
  reportLink: string;
  reportFileName: string;
  reportInputs = {
    cif: 'string',
    fiscal_year: 2018,
    fiscal_month: 0
  }

  fiscalMonth: number;
  months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
  fiscalYear: number;
  years = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userManager: UserService,
    private ds: DataService,
    private modalService: NgbModal,
    private apis: ApiService,
    private titleService: Title
  ) {
  }

  getCompanyFromApi() {
    this.company = new CompanyData(this.companyId);
    this.apis.getFinalizedGradeForCIF(this.companyId).subscribe((q: { id: string }) => {
      this.apis.getResultFromQueryID(q.id, (qr: { rows: CompanyRow[] }) => {
        if (qr.rows.length) {

          console.log('all results for this cif', qr);
          qr.rows.sort((a, b) => {
            return b.Fiscal_Month - a.Fiscal_Month
          }).sort((a, b) => {
            return b.Fiscal_Year - a.Fiscal_Year
          })
          /*let date = new Date();
          this.fiscalYear = date.getFullYear();
          this.fiscalMonth = date.getMonth() + 1;
          console.log(this.fiscalYear, this.fiscalMonth);
          this.years = [this.fiscalYear, (this.fiscalYear - 1), (this.fiscalYear - 2)]*/

          const latest = qr.rows[0];
          this.fiscalYear = latest.Fiscal_Year;
          this.fiscalMonth = latest.Fiscal_Month;

          this.reportInputs.fiscal_month = latest.Fiscal_Month;
          this.reportInputs.fiscal_year = latest.Fiscal_Year;

          console.log(this.fiscalYear, this.fiscalMonth);
          this.years = [this.fiscalYear, (this.fiscalYear - 1), (this.fiscalYear - 2)]
          if (latest) {
            this.company.assignGrading('previous', this.apis.gradingNumberToString(latest.MAS_Grading));
            this.company.assignGrading('recommended', this.apis.gradingNumberToString(latest.Calculated_MAS_Grading));
            this.company.assignGrading('final', this.apis.gradingNumberToString(latest.Finalized_MAS_Grading));
            this.company.flagged = !!latest.riskFlag;
            // check for grading override / finalized
            this.hasOverride();

            // this.company.assignGrading('review', this.apis.gradingNumberToString(qr.rows[0].Calculated_MAS_Grading));
          }
        } else {
          alert('No records found for this cif');
        }
      })
    }, e => {
      this.sessionExpired(e);
    })
  }

  sessionExpired(e) {
    if (e.status === 401) {
      alert('session expired');
      this.userManager.logOut();
    }
  }


  /*commentAdded(comment: string): void {
    this.company.comments.push({
      date: new Date(),
      ratingFrom: this.company.gradings[this.gradingFrom].rejected ?
        ('<span class="strike-through">' + this.company.gradings[this.gradingFrom].value + '</span>') :
        this.company.gradings[this.gradingFrom].value,
      ratingTo: this.company.gradings[this.gradingTo] ? this.company.gradings[this.gradingTo].value : null,
      text: comment,
      userName: this.currentUser.email
    });
    this.updated = true;
    // this.saveCompanyToLocalStorage();
  }

  commentDeleted(i: number): void {
    this.company.comments.splice(i, 1);
    this.updated = true;
    // this.saveCompanyToLocalStorage();
  }*/

  toggleComments(): void {
    this.showComments = !this.showComments;
    this.company.gradingReview = JSON.parse(JSON.stringify(this.company.gradingReview));
  }

  gradingUpdated(update: string, gradeType: string, elem): void {
    console.log(update, gradeType);
    this.overrideAction = update;

    if (update === 'approve') {
      this.openModal(elem);

    } else if (update === 'reject') {

      // this.company.assignGrading('review', null);
      this.openModal(elem);

    } else if (update === 'cancel') {

      // this.company.assignGrading('review', null);
      this.openModal(elem);

    } else {
      this.company.assignGrading('review', update as '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS');
      this.openModal(elem);
    }
    this.updated = true;
    // this.showComments = true;
    // this.saveCompanyToLocalStorage();
  }

  analystApproveOverride() {
    this.apis.gradeOverride(
      Number(this.companyId),
      this.fiscalYear,
      this.fiscalMonth,
      parseInt(this.company.gradings.recommended),
      parseInt(this.company.gradings.review),
      this.gradingOverrideComment).subscribe(r => {
      console.log(r);
      alert('override submitted');
      this.canAssign = false;
      this.canApprove = false;
      this.submittingOverride = false;
      this.hasOverride();
    }, (error: { error: { error: { message: string } } }) => {
      console.log(error);
      this.sessionExpired(error);
      alert(error.error.error.message);
      this.submittingOverride = false;
    });
  }

  submitOverride() {
      console.log('submitting ' + this.overrideAction);
      this.submittingOverride = true;

    if (this.overrideAction === 'approve') {
      if (this.currentUser.userRole) {
        // approver
        this.apis.gradeOverrideApprove(this.overrideData.id, this.currentUser.userRole, this.gradingOverrideComment).subscribe(r => {
          console.log('override approve', r);
          alert('override approved');
          this.submittingOverride = false;
          this.canAssign = false;
          this.canApprove = false;
          if (this.currentUser.userRole === 2) {
            this.company.assignGrading('final', this.company.gradings.review);
          }
          this.hasOverride();
        }, e => {
          this.sessionExpired(e);
        });

      } else {
        // analyst
        this.analystApproveOverride();
      }

    } else if (this.overrideAction === 'reject') {
      this.apis.gradeOverrideReject(this.overrideData.id, this.currentUser.userRole, this.gradingOverrideComment).subscribe(r => {
        console.log('override reject', r);
        this.company.assignGrading('review', null);
        alert('override rejected');
        this.submittingOverride = false;
      }, e => {
        this.sessionExpired(e);
      });

    } else if (this.overrideAction === 'cancel') {
      this.apis.gradeOverrideCancel(this.overrideData.id, this.gradingOverrideComment).subscribe(r => {
        console.log('override cancel', r);
        this.overrideData = null;
        this.company.assignGrading('review', null);
        alert('override cancelled');
        this.submittingOverride = false;
      }, e => {
        this.sessionExpired(e);
      });

    } else {

      if (this.currentUser.userRole) {
        this.apis.gradeOverrideApprove(
          this.overrideData.id,
          this.currentUser.userRole,
          this.gradingOverrideComment,
          parseInt(this.company.gradings.review)
        ).subscribe(r => {
          console.log(r);
          alert('new grading submitted');
          this.submittingOverride = false;
          this.canAssign = false;
          this.canApprove = false;
          if (this.currentUser.userRole === 2) {
            this.company.assignGrading('final', this.company.gradings.review)
          }
          this.hasOverride();
        }, (error: { error: { error: { message: string } } }) => {
          console.log(error);
          this.sessionExpired(error);
          alert(error.error.error.message);
          this.submittingOverride = false;
        });
      } else {
        this.analystApproveOverride();
      }
    }


  }

  openModal(elem): void {

    this.modalService.open(elem, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      // this.closeResult = `Closed with: ${result}`;
      console.log('modal closed', result);
      if (result === 'override') {
        this.submitOverride();
      }
      if (result === 'download') {

      }
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  saveCompanyToLocalStorage(): void {
    this.updated = false;
    this.saving = true;
    setTimeout(() => {
      this.saving = false;
    }, 1000);
    this.ds.saveCompany(this.company);
  }

  /*deleteCompanyFromLocalStorage(): void {
    this.ds.deleteCompany(this.company);
    this.getCompanyFromId();
  }*/

  generateReport() {
    this.reportStatus = 'generating report...';
    this.apis.getGradingNarrative(Number(this.companyId), this.reportInputs.fiscal_year, this.reportInputs.fiscal_month).subscribe((r: { filename: string }) => {
      console.log('report result', r);
      if (r.filename) {
        this.reportStatus = 'DONE!'
        this.reportLink = r.filename;
        this.reportFileName = r.filename.split('/').pop();
      }
    }, e => {
      this.sessionExpired(e);
    })
  }

  hasOverride() {
    console.log('checking override status');


    // look for open overrides
    this.apis.getAllOverridesForCIF(this.companyId).subscribe((result: ApiOverrideResponse) => {
      console.log('finding overrides for cif ', this.companyId, result);
      let overrides = result.result.data
        .filter(o => o.cifNo === parseInt(this.companyId) && o.fiscalYear === this.fiscalYear && o.fiscalMonth === this.fiscalMonth);
      if (overrides.length) {
        /*overrides.sort((a, b) => {
          return b.createdAt > a.createdAt ? 1 : (a.createdAt > b.createdAt ? -1 : 0)
        });*/
        console.log('found overrides', overrides);
        this.overrideData = overrides[0];
        if (this.overrideData.auditLog) {
          if (this.overrideData.auditLog.length) {
            this.overrideData.auditLog.sort((a, b) => {
              return new Date(b.updatedAt) > new Date(a.updatedAt) ? 1 : (new Date(a.updatedAt) > new Date(b.updatedAt) ? -1 : 0)
            });
            if (this.overrideData.auditLog[0].finalizedMasGrading) {
              this.company.assignGrading('review', this.ds.gradingNumberToString(this.overrideData.auditLog[0].finalizedMasGrading));
            } else {
              this.company.assignGrading('review', this.ds.gradingNumberToString(this.overrideData.finalizedMasGrading));
            }
            this.lastReviewScore = this.company.gradings.review;
            if (this.overrideData.auditLog[0].status === 'Awaiting') {
              // awaiting action
              if (this.overrideData.auditLog[0].currentStep !== this.currentUser.userRole) {
                // not this approver's role to override
                this.canAssign = false;
                this.canApprove = false;
              } else {
                this.canAssign = true;
                this.canApprove = true;
              }
            } else {
              // not awaiting action
              this.canAssign = false;
              this.canApprove = false;
            }
          }
        }
      } else {
        // no overrides
        this.company.assignGrading('review', this.company.gradings.recommended);
        if (!this.currentUser.userRole) {
          // analyst user
          this.canAssign = true;
          this.canApprove = true;
        } else {
          this.canApprove = true;
        }
      }
    }, e => {
      this.sessionExpired(e);
    })

    /*this.apis.getOverrideStatus(Number(this.overrideId)).subscribe((r: { result: ApiOverrideData }) => {
      console.log('override status', r);
      this.company.assignGrading('review', this.ds.gradingNumberToString(r.result.finalizedMasGrading));
      this.overrideData = r.result;
      if (this.overrideData.auditLog) {
        this.overrideData.auditLog.sort((a, b) => {
          return b.createdAt > a.createdAt ? 1 : (a.createdAt > b.createdAt ? -1 : 0)
        });
      }
    })*/
  }

  ngOnInit(): void {


    this.titleService.setTitle('DCRA');

    this.currentUser = this.userManager.getActiveUser();
    console.log('logged in as', this.currentUser);
    this.apis.token = this.currentUser.token;

    this.companyId = decodeURIComponent(this.route.snapshot.paramMap.get('id'));
    console.log(this.companyId);

    // this.overrideId = Number(decodeURIComponent(this.route.snapshot.paramMap.get('overrideId')));
    // console.log(this.overrideId);
    // this.getCompanyFromIdFromSamples();
    // console.log(JSON.stringify(this.company));
    this.getCompanyFromApi();
  }

  resetData(): void {
    this.ds.clearLocal();
  }
}
