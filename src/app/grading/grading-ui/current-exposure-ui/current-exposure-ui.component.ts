import {Component, Input, OnInit} from '@angular/core';
import {CompanyBackgroundApi, CompanyExposure, CompanyExposureApi} from '../../../classes';
import {faExternalLinkSquareAlt} from '@fortawesome/free-solid-svg-icons';
import {ApiService} from "../../../services/api.service";
import {UserService} from "../../../services/user.service";
import {DataService} from "../../../services/data.service";

@Component({
  selector: 'app-current-exposure-ui',
  templateUrl: './current-exposure-ui.component.html',
  styleUrls: ['./current-exposure-ui.component.scss']
})
export class CurrentExposureUiComponent implements OnInit {

  @Input() exposureData: CompanyExposure;
  @Input() companyId: string;

  icons = {
    url: faExternalLinkSquareAlt
  };

  exposureTop: { key: string, label: string, type?: string, width?: number }[] = [
    /*{
      key: 'customerName',
      label: 'Customer Name',
      width: 2
    },*/
    {
      key: 'cifNumber',
      label: 'CIF Number',
      width: 2
    },
    {
      key: 'rating',
      label: 'Internal Rating',
      width: 2
    },/*
    {
      key: 'existingGrading',
      label: 'Existing Grading',
      width: 2
    },*/
    {
      key: 'date',
      label: 'Last Update Date',
      type: 'date',
      width: 2
    },
    {
      key: 'nextReviewDate',
      label: 'Next Review Date',
      type: 'date',
      width: 2
    },
  ];
  backgroundBlocks:{ key: string, label: string, type?: string, width?: number }[] = [
    {
      key: 'industry',
      label: 'Industry',
      width: 3
    },/*
    {
      key: 'text',
      label: 'Company Background',
      width: 3
    },*/
    {
      key: 'businessActivities',
      label: 'Business Activities',
      width: 3
    },
    {
      key: 'productsAndServices',
      label: 'Products and Services',
      width: 3
    },
  ];
  facilitiesColumns = [
    /*
    Cust_ABBR: string,
    Date: string,
    Credit_Facility: string,
    Credit_limit: string,
    Outstanding: string,
    Purpose: string,
    Maturity: string,
    Security: string,
    Covenants: string*/
    {
      key: 'Cust_ABBR',
      label: 'Customer Abbreviation',
      type: 'string'
    },
    {
      key: 'Fiscal_Month',
      label: 'Fiscal Month',
      type: 'string'
    },
    {
      key: 'Fiscal_Year',
      label: 'Fiscal Year',
      type: 'string'
    },
    {
      key: 'Credit_Facility',
      label: 'Credit Facility',
      type: 'string'
    },
    {
      key: 'Credit_Limit',
      label: 'Credit Limit',
      type: 'string'
    },
    {
      key: 'Outstanding',
      label: 'Outstanding',
      type: 'string'
    },
    {
      key: 'Purpose',
      label: 'Purpose',
      type: 'string'
    },
    {
      key: 'Maturity',
      label: 'Maturity',
      type: 'string'
    },
    {
      key: 'Security',
      label: 'Security',
      type: 'string'
    },
    {
      key: 'Covenants',
      label: 'Covenants',
      type: 'string'
    }
  ];

  positions: { key: string, label: string, type?: string }[] = [];

  constructor(
    private apis: ApiService,
    private userManager: UserService,
    private ds: DataService
  ) {
  }

  ngOnInit(): void {
    console.log(this.exposureData);

    if (!this.exposureData && this.companyId) {

      this.exposureData = new CompanyExposure(this.companyId, new Date())

      this.apis.getCompanyData('backgrounds', this.companyId).subscribe((q: { id: string }) => {
        this.apis.getResultFromQueryID(q.id, (qr: { rows: CompanyBackgroundApi[] }) => {
          console.log('received background data for CIF', qr)
          this.exposureData.updateFromBackgroundApi(qr.rows[0])
        })
      }, e => {

        if (e.status === 401) {
          alert('session expired');
          this.userManager.logOut();
        }
      });
      this.apis.getCompanyData('exposures', this.companyId).subscribe((q: { id: string }) => {
        this.apis.getResultFromQueryID(q.id, (qr: {rows:  CompanyExposureApi[] }) => {
          console.log('received exposure data for CIF', qr)
          this.exposureData.updateFromExposureApi(qr.rows);
          /*console.log('testing exposure strings');
          console.log('FX: USD394k, Currency option: USD40k');
          console.log(this.ds.getCurrencyAmountFromString('FX: USD394k, Currency option: USD40k'));
          console.log('GTee: USD65Kk and USD1.3mil');
          console.log(this.ds.getCurrencyAmountFromString('GTee: USD65Kk and USD1.3mil'));*/

        })
      }, e => {

        if (e.status === 401) {
          alert('session expired');
          this.userManager.logOut();
        }
      });
      // nlg
      this.apis.getCompanyData('nlgData', this.companyId).subscribe((q: { id: string }) => {
        this.apis.getResultFromQueryID(q.id, (qr: { rows: any[] }) => {
          console.log('received nlgData data for CIF', qr)
          if (qr.rows.length) {
            let record = qr.rows[0];
            this.exposureData.date = record.Approved_Date;
            this.exposureData.nextReviewDate = record.Next_Rating_Review_Date;
            this.exposureData.rating = record.Current_Internal_Rating;
          }
        })
      }, e => {

        if (e.status === 401) {
          alert('session expired');
          this.userManager.logOut();
        }
      });
    }

    /*this.positions = Object.keys(this.exposureData.companyBackground.management).map(m => {
      // console.log(m);
      return {
        key: m,
        label: this.exposureData.companyBackground.management[m]
      };
    });*/
    // console.log(this.positions);
  }

}
