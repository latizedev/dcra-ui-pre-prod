import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentExposureUiComponent } from './current-exposure-ui.component';

describe('CurrentExposureUiComponent', () => {
  let component: CurrentExposureUiComponent;
  let fixture: ComponentFixture<CurrentExposureUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurrentExposureUiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentExposureUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
