import {Component, Input, OnInit} from '@angular/core';
import {CompanyNews, CompanyNewsApi} from '../../../classes';
import {faFlag} from '@fortawesome/free-solid-svg-icons';
import {ApiService} from "../../../services/api.service";

@Component({
  selector: 'app-news-reader',
  templateUrl: './news-reader.component.html',
  styleUrls: ['./news-reader.component.scss']
})
export class NewsReaderComponent implements OnInit {

  @Input() news: CompanyNews[];
  @Input() companyId: string;



  icons = {
    flag: faFlag
  };

   constructor(
     private apis: ApiService
   ) { }



  ngOnInit(): void {
     if (this.companyId) {
       this.apis.getCompanyData('news', this.companyId).subscribe((q: { id: string }) => {
         this.apis.getResultFromQueryID(q.id, (qr: { rows: CompanyNewsApi[] }) => {
           console.log('received news data for CIF', qr)
           this.news = qr.rows.map(n => new CompanyNews(n));
         })
       });
     }
  }

}
