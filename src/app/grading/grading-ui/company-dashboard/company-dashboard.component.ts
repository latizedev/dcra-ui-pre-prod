import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {CompanyData, CompanyExposure, CompanyExposureApi, CytoElement} from '../../../classes';
import {VisualMakerService} from '../../../services/visual-maker.service';
import {ApiService} from "../../../services/api.service";
import {DataService} from "../../../services/data.service";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-company-dashboard',
  templateUrl: './company-dashboard.component.html',
  styleUrls: ['./company-dashboard.component.scss']
})
export class CompanyDashboardComponent implements OnInit, AfterViewInit {

  @Input() company: CompanyData;
  @Input() companies: CompanyData[];
  peers: CompanyData[];

  quarters: string[];
  quarterSelected: string;

  selectedSpiderPeerID: string;
  selectedSpiderPeer: CompanyData;

  selectedTablePeerID: string;
  selectedTablePeer: CompanyData;

  showCharts: boolean;

  spiderList: { key: string, name: string, ratio?: {a: string, b: string} }[] = [
    {
      name: 'Investing Cashflow',
      key: 'CF_CASH_FROM_INVST_ACVT'
    },
    {
      name: 'Financing Cashflow',
      key: 'CF_CASH_FROM_FINCNG_ACVT'
    },
    {
      name: 'Total Revenue',
      key: 'PL_REVENUES'
    },
    {
      name: 'Total Liabilities',
      key: 'LIAB_TOTAL'
    },
    {
      name: 'Net Cashflow',
      key: 'CF_NET_CASHFLOW'
    },
    {
      name: 'Total Asset',
      key: 'TOTAL_AST'
    },
    {
      name: 'Revenue Growth',
      key: 'PL_REVENUES_GROWTH',
    },
    {
      name: 'Operating Margin',
      key: 'NA',
      ratio: {a: 'PL_EBIT', b: 'PL_REVENUES'}
    },
    {
      name: 'Revenue / Total Asset',
      key: 'NA',
      ratio: {a: 'PL_REVENUES', b: 'TOTAL_AST'}
    },
    {
      name: 'Current Assets / Current Liabilities',
      key: 'NA',
      ratio: {a: 'CURR_AST', b: 'CURR_LIAB'}
    },
  ];

  exposureNetwork: CytoElement;
  financialStandingSpider;

  "latestFinancials" = [
    {
      name: "Revenue",
      key: 'PL_REVENUES',
      value: 0
    },
    {
      name: "Cash Flow",
      key: 'CF_NET_CASHFLOW',
      value: 0
    },
    {
      name: "EBITDA",
      key: 'PL_EBITDA',
      value: 0
    },
    {
      name: "Total Asset",
      key: 'TOTAL_AST',
      value: 0
    }
  ];
  fiscalYear: number;
  fiscalMonth: number;
  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];


  constructor(
    private vsm: VisualMakerService,
    private apis: ApiService,
    private data: DataService,
    private user: UserService
  ) {
  }

  changeSpiderPeer(): void {
    this.financialStandingSpider = null;
    this.selectedSpiderPeer = this.peers.filter(p => p.cif === this.selectedSpiderPeerID)[0];
    setTimeout(() =>
      this.financialStandingSpider = this.vsm.createFinancialStandingSpiderSinglePeer(this.company, this.selectedSpiderPeer), 100);
  }
  changeTablePeer(): void {
    this.selectedTablePeer = this.peers.filter(c => c.cif === this.selectedTablePeerID)[0];
  }

  changeQuarter(): void {
    this.exposureNetwork = null;
    setTimeout(() => this.exposureNetwork = this.vsm.createCytoExposureNetworkOld(this.quarterSelected, this.company), 100);
  }

  createSpiderList(company: CompanyData, records: any) {
    this.spiderList.forEach(item => {
      if (item.ratio) {
        company.financialStanding[item.name] = records[item.ratio.a] / records[item.ratio.b] //  + '<br>' + this.data.nFormatter(val, 2)
      } else {
        company.financialStanding[item.name] = records[item.key] //  + '<br>' + this.data.nFormatter(val, 2)
      }
    })
  }

  ngOnInit(): void {

    // get exposure data

    this.apis.getCompanyData('exposures', this.company.cif).subscribe((q: { id: string }) => {
      this.apis.getResultFromQueryID(q.id, (qr: {rows:  CompanyExposureApi[] }) => {
        console.log('received exposure data for CIF', qr)
        this.company.currentExposure = new CompanyExposure(this.company.cif, new Date(this.fiscalYear, this.fiscalMonth-1));
        if (qr.rows.length) {
          this.company.currentExposure.updateFromExposureApi(qr.rows);
          this.company.totalRiskExposure = {};
          /*this.company.totalRiskExposure[this.fiscalYear] =
            this.company.currentExposure.detailsOfFacilities
              /!*.map(r => Number(r.Credit_Limit.replace(/[^0-9]/g, ''))).reduce((s, a) => {
                return s + a;
              }, 0);*!/
              .map(r => r.Outstanding_Amount.toLocaleString())
              .reduce((s, a) => {
              return s + a;
            }, 0);*/

          // this.quarters = Object.keys(this.company.totalRiskExposure);
          // this.quarterSelected = this.quarters[0];
          // console.log(this.company.totalRiskExposure);
          // console.log(this.company.currentExposure);

          setTimeout(() => {

            this.exposureNetwork = this.vsm.createCytoExposureNetwork(this.company);
            console.log(this.exposureNetwork);

          }, 500);
        }


      })
    }, e => {

      if (e.status === 401) {
        alert('session expired');
        this.user.logOut();
      }
    });

    // latest financial table
    this.apis.getCompanyData('financials', this.company.cif).subscribe((q: { id: string }) => {
      this.apis.getResultFromQueryID(q.id, (qr: { rows: any[] }) => {
        if (qr.rows.length) {
          qr.rows.sort((a: any, b: any) => {
            return b.Fiscal_Year - a.Fiscal_Year
          });
          console.log('received financial data for CIF', qr);
          this.fiscalYear = qr.rows[0].Fiscal_Year;
          this.fiscalMonth = qr.rows[0].Fiscal_Month;
          this.company.latestFinancials = [];
          this.latestFinancials.forEach(fin => {
            // fin.value = qr.rows[0][fin.key];
            this.company.latestFinancials.push({
              name: fin.name,
              value: qr.rows[0][fin.key]
            })
          });
          this.createSpiderList(this.company, qr.rows[0]);
          setTimeout(() =>
            this.financialStandingSpider = this.vsm.createFinancialStandingSpiderSinglePeer(this.company), 100);
          console.log('created spider data', this.company.financialStanding);

          // get peers
          let peersReady = 0;
          this.apis.getAllCompanyData('backgrounds').subscribe((q: { id: string }) => {
            this.apis.getResultFromQueryID(q.id, (qr: { rows: any[] }) => {
              console.log('received background data for all CIFs', qr);
              this.peers = qr.rows.map(c => new CompanyData(c.CIF_NO)); // todo: filter by industry or similar
              console.log('peers', this.peers);
              this.peers.forEach((peer, p, peers) => {
                this.apis.getCompanyData('financials', peer.cif).subscribe((q: { id: string }) => {
                  this.apis.getResultFromQueryID(q.id, (qr: { rows: any[] }) => {
                    let currentRows = qr.rows.filter(r => r.Fiscal_Year === this.fiscalYear && r.Fiscal_Month === this.fiscalMonth)
                    console.log('matched record for', peer.cif, currentRows);
                    if (currentRows.length) {
                      /*qr.rows.sort((a: any, b: any) => {
                        return b.Fiscal_Year - a.Fiscal_Year
                      });*/
                      console.log('received financial data for peer', peer.cif, currentRows);
                      peer.latestFinancials = [];
                      this.latestFinancials.forEach(fin => {
                        // fin.value = qr.rows[0][fin.key];
                        peer.latestFinancials.push({
                          name: fin.name,
                          value: currentRows[0][fin.key]
                        })
                      });
                      this.createSpiderList(peer, currentRows[0]);
                      console.log(peersReady);
                    } else {
                      // if no records
                      peers.splice(p, 1);
                    }
                    peersReady += 1;
                    if (peersReady === this.peers.length) {
                      setTimeout(() => {
                        // console.log('creating spider chart', this.peers, this.company);
                        // this.financialStandingSpider = this.vsm.createFinancialStandingSpider(this.peers, this.company);
                        // this.exposureNetwork = this.vsm.createCytoExposureNetwork(this.quarterSelected, this.company, null);
                        // this.showCharts = true;
                      }, 200)
                    }
                    // this.news = qr.rows.map(n => new CompanyNews(n));
                  })
                });
              });
            })
          }, e => {

            if (e.status === 401) {
              alert('session expired');
              this.user.logOut();
            }
          });
        }
        // this.company.latestFinancials = this.latestFinancials
        // this.news = qr.rows.map(n => new CompanyNews(n));
      })
    }, e => {

      if (e.status === 401) {
        alert('session expired');
        this.user.logOut();
      }
    });

    /*this.peers = this.companies.filter(c => c.id !== this.company.id);
    this.selectedVs = this.peers[0];
    this.selectedVsId = this.selectedVs.id;
    this.quarters = Object.keys(this.company.totalRiskExposure);
    this.quarterSelected = this.quarters[this.quarters.length - 1];

    setTimeout(() => {

      this.financialStandingSpider = this.vsm.createFinancialStandingSpider(this.companies, this.company);
      this.exposureNetwork = this.vsm.createCytoExposureNetwork(this.quarterSelected, this.company, null);
      this.showCharts = true;
    }, 200);*/
  }

  ngAfterViewInit(): void {
  }
}
