import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges,
  QueryList,
  ViewChildren
} from '@angular/core';
import {
  ApiGradeDistribution,
  CompanyNews,
  CompanyNewsApi,
  GradingBreakdown,
  GradingCategory,
  QueryStatus
} from '../../../classes';
import {
  faTimes,
  faAngleRight,
  faChevronDown,
  faChevronUp,
  faMinus,
  faInfoCircle,
  faCaretUp,
  faCaretDown,
  faCircle
} from '@fortawesome/free-solid-svg-icons';
import {VisualMakerService} from '../../../services/visual-maker.service';
import {ApiService} from "../../../services/api.service";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-grading-review-ui',
  templateUrl: './grading-review-ui.component.html',
  styleUrls: ['./grading-review-ui.component.scss']
})
export class GradingReviewUIComponent implements OnInit, OnChanges, AfterViewInit {

  @ViewChildren('categoryDivs') categoryDivs: QueryList<ElementRef>;
  @ViewChildren('benchmarkDivs') benchmarkDivs: QueryList<ElementRef>;

  // @Input() gradingCategories: GradingCategory[] = [];
  @Input() companyId: string;
  types: {
    name: string,
    movement?: number;
    trend: { up: string[], down: string[] }
  }[] = [];
  benchmarkTypes: {
    name: string,
    movement?: number;
    trend: { up: string[], down: string[] }
  }[] = [];

  chevron = {
    up: faCaretUp,
    down: faCaretDown,
    right: faAngleRight,
    flat: faCircle
  };
  infoIcon = faInfoCircle;
  faTimes = faTimes;

  categories: GradingCategory[] = [
    {
      name: "Revenue",
      type: "PROFIT & LOSS",
      change: 0,
      scoresTrend: [],
      breakdown: [
        {
          text: "<b>Decrease</b> in <b>Revenue</b> by more than <b>40%</b>",
          name: "PL_REVENUES",
          label: 'Total Revenue',
          movement: 0,
          trend: {}
        },
        {
          text: "<b>Increase</b> in <b>Borrowings</b> by more than <b>40%</b>",
          name: "PL_COST_OF_SALES",
          label: 'Cost of Sales',
          movement: 0,
          trend: {}
        },
        {
          text: "<b>Decrease</b> in <b>Cashflow</b> by more than <b>70%</b>",
          name: "PL_GROS_PROFIT",
          label: 'Gross Profit',
          movement: 0,
          trend: {}
        }
      ],
      "comments": []
    },
    {
      name: "Earnings",
      type: "PROFIT & LOSS",
      change: 0,
      scoresTrend: [],
      breakdown: [
        {
          text: '',
          name: 'PL_EBIT',
          label: 'EBIT',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'PL_EBIT_OTHER',
          label: 'FX Gain / Loss',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'PL_EBITDA',
          label: 'EBITDA',
          movement: 0,
          trend: {}
        }
      ],
      "comments": []
    },
    {
      name: "Cashflow",
      type: "CASHFLOW",
      change: 0,
      scoresTrend: [],
      breakdown: [
        {
          text: '',
          name: 'CF_CASH_FROM_OPE_ACVT',
          label: 'Operating Cashflow',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'CF_CASH_FROM_INVST_ACVT',
          label: 'Investing Cashflow',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'CF_CASH_FROM_FINCNG_ACVT',
          label: 'Financing Cashflow',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'CF_OTHER_FLUCTUATION_OF_CASH',
          label: 'Other Fluctuation of Cash',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'CF_NET_CASHFLOW',
          label: 'Net Cashflow',
          movement: 0,
          trend: {}
        }
      ],
      "comments": []
    },
    {
      name: "Income",
      type: "CASHFLOW",
      change: 0,
      scoresTrend: [],
      breakdown: [
        {
          text: '',
          name: 'CF_NET_INCM',
          label: 'Net Income',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'CF_NET_WORKING_CAPITAL',
          label: 'Net Working Capital',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'CF_INVST',
          label: 'Investments',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'CF_ACQUISITIONS',
          label: 'Acquisitions',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'CF_PROCEEDS_FROM_PP_AND_E',
          label: 'Proceeds from Sale of PPE',
          movement: 0,
          trend: {}
        }
      ],
      "comments": []
    },
    {
      name: "Assets & Liabilities",
      type: "BALANCE SHEET",
      change: 0,
      scoresTrend: [],
      breakdown: [
        {
          text: '',
          name: 'TOTAL_AST',
          label: 'Totall Assets',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'CURR_LIAB',
          label: 'Current Liabilities',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'LIAB_TOTAL',
          label: 'Total Liabilities',
          movement: 0,
          trend: {}
        }
      ],
      "comments": []
    },
    {
      name: "Equity",
      type: "BALANCE SHEET",
      change: 0,
      scoresTrend: [],
      breakdown: [
        {
          text: '',
          name: 'RETAINED_EARNINGS',
          label: 'Retained Earnings',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'SHAREHOLDERS_EQUITY',
          label: 'Shareholders Equity',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'GROS_DEBT',
          label: 'Gross Debt',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'UNRALZD_HOLDING_GAIN_LOSSES',
          label: 'Unrealized Holdings',
          movement: 0,
          trend: {}
        }
      ],
      "comments": []
    },
    ]
  benchmarks: GradingCategory[] = [
    {
      name: "Equity Indices",
      // percentage: true,
      type: "BENCHMARK",
      change: 0,
      scoresTrend: [],
      benchmark: true,
      originAdjusted: true,
      breakdown: [
        {
          text: '',
          name: 'SPX Index',
          label: 'S&P 500 INDEX US',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'INDU Index',
          label: 'Dow Jones Industrial Average',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'TPX Index',
          label: 'TOKYO',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'SENSEX',
          label: 'Mumbai',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'STI Index',
          label: 'Singapore',
          movement: 0,
          trend: {}
        }
      ],
      "comments": []
    },
    {
      name: "Currency against USD",
      // percentage: true,
      type: "BENCHMARK",
      change: 0,
      scoresTrend: [],
      benchmark: true,
      originAdjusted: true,
      breakdown: [
        {
          text: '',
          label: 'Yen',
          name: 'JPY BGN Curncy',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'SGD',
          name: 'SGD BGN Curncy',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'CNY',
          name: 'CNYMUSD Index',
          movement: 0,
          trend: {}
        },
      ],
      "comments": []
    },
    {
      name: "Benchmark LNG Prices (US$ per Barrel)\n",
      type: "BENCHMARK",
      change: 0,
      scoresTrend: [],
      benchmark: true,
      breakdown: [
        {
          text: '',
          label: 'Henry Hub',
          name: 'NGUSHHUB Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'Russian LNG',
          name: 'NGP RU Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'Indonesian LNG japan',
          name: 'LNGJLNID Index',
          movement: 0,
          trend: {}
        },
      ],
      "comments": []
    },
    {
      name: "Benchmark Oil Prices (US$ per Barrel)\n",
      type: "BENCHMARK",
      change: 0,
      scoresTrend: [],
      benchmark: true,
      breakdown: [
        {
          text: '',
          label: 'DUBAI',
          name: 'Dubai Oil Price Index',// ''PGCRDUBA Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'WTI',
          name: 'WTI Oil Price Index',// 'USCRWTIC Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'Brent',
          name: 'Brent Oil Index', // 'EUCRBRDT Index',
          movement: 0,
          trend: {}
        },
      ],
      "comments": []
    },
    {
      name: "Manufacturing Purchasing Manager's Index\n",
      type: "BENCHMARK",
      change: 0,
      scoresTrend: [],
      benchmark: true,
      breakdown: [
        {
          text: '',
          label: 'Markit China',
          name: 'MPMICNMA Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'Markit India',
          name: 'MPMIINMA Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'Markit Indonesia',
          name: 'MPMIIDMA Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'SPMMI Spore',
          name: 'SPMINDX Index',
          movement: 0,
          trend: {}
        },
      ],
      "comments": []
    },
    {
      name: "URA Property Price Indices\n",
      type: "BENCHMARK",
      change: 0,
      scoresTrend: [],
      benchmark: true,
      breakdown: [
        {
          text: '',
          name: ' Private Property Price Index - Pte Residential',
          label: 'Private Residential Property Price Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: ' Private Property Price Index -Office',
          label: 'Office Price Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: ' Private Property Price Index -Retail',
          label: 'Retail Price Index',
          movement: 0,
          trend: {}
        },
      ],
      "comments": []
    },
    {
      name: "GDP Growth (%)",
      percentage: true,
      type: "BENCHMARK",
      change: 0,
      scoresTrend: [],
      benchmark: true,
      breakdown: [
        {
          text: '',
          label: 'US',
          name: 'GDP CYOY Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'Japan',
          name: 'JGDPNSAQ Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'China',
          name: 'CNGDPYOY Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'Indonesia',
          name: 'IDGDPY Index',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          label: 'Singapore',
          name: 'SGDPYOY Index',
          movement: 0,
          trend: {}
        },
      ],
      "comments": []
    },
    {
      name: "10-Yr Bond Yield (%)",
      percentage: true,
      type: "BENCHMARK",
      change: 0,
      scoresTrend: [],
      benchmark: true,
      breakdown: [
        {
          text: '',
          name: 'MASB10Y Index',
          label: 'Singapore',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'GDBR10 Index',
          label: 'Germany',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'GJGB10 Index',
          label: 'Japan',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'USGG10YR Index',
          label: 'US',
          movement: 0,
          trend: {}
        },
        {
          text: '',
          name: 'GUKG10 Index',
          label: 'UK',
          movement: 0,
          trend: {}
        },
      ],
      "comments": []
    },
  ]

  text: {
    summary: string,
    reasons: string[];
  } = {
    summary: null,
    reasons: []
  }

  colors = ['#27ABE2', '#29357F', '#999999', '#7458D9', '#d99958', '#d95894'];
  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  latestYear: number;
  latestMonth: number;
  startingYear: number;
  startingMonth: number;
  years: number[];

  constructor(
    private vis: VisualMakerService,
    private apis: ApiService,
    private users: UserService
  ) {
  }

  makeArray(n: number): any[] {
    // console.log(new Array(n));
    return Array(Math.abs(n));
  }

  scrollToCategory(i: number): void {
    console.log(this.categoryDivs.get(i).nativeElement);
    this.categoryDivs.get(i).nativeElement.scrollIntoView({behavior: 'smooth'});
  }
  scrollToBenchmark(i: number): void {
    console.log(this.benchmarkDivs.get(i).nativeElement);
    this.benchmarkDivs.get(i).nativeElement.scrollIntoView({behavior: 'smooth'});
  }

  getChart(breakdown: GradingBreakdown): { data: any, layout: any } {
    return this.vis.createSimpleChartFromTrends([{name: breakdown.name, unit: 'Y-O-Y', steps: breakdown.trend}]);
  }

  getCatChart(category: GradingCategory) {
    return this.vis.createTimeSeries(category.breakdown.map(b => ({
      name: b.label || b.name,
      unit: 'Y-O-Y',
      steps: b.trend
    })));
  }



  getMovementDescription(m: number): string {
    const movement = Math.abs(m);
    if (movement === 0) {
      return 'No';
    }
    if (movement >= 0 && movement < 20) {
      return 'Minor';
    }
    if (movement >= 20 && movement < 50) {
      return 'Major';
    }
    if (movement >= 50) {
      return 'Critical';
    }
  }


  processMovement(gb: GradingBreakdown): void {
    const values = Object.values(gb.trend);
    let n = values.length;
    /*if (n > 12) {
      gb.movement = (values[0] - values[12]) / Math.abs(values[12]) * 100;
    } else {
      gb.movement = (values[0] - values[n-1]) / Math.abs(values[n-1]) * 100;
    }*/
    if (n > 1) {
      gb.movement = (values[0] - values[1]) / Math.abs(values[1]) * 100;
    } else {
      gb.movement = null
    }
  }

  beanCounter(benchmark: GradingCategory, dir: 'up' | 'down') {
    return benchmark.breakdown.filter(b => (dir === 'up' ? (b.movement > 0) : (b.movement < 0) ) ).length;
  }


  getCategoryChange(cat: GradingCategory): void {
    // breakdown movement must already be processed for this
    cat.change = Math.round(cat.breakdown.map(b => b.movement * (b.negativeEffect ? -1 : 1)).reduce((i, s) => (i + s)) / (10 * cat.breakdown.length));
  }

  ngOnInit(): void {

    if (this.companyId) {
      this.apis.getCompanyData('financials', this.companyId).subscribe((q: { id: string }) => {
        this.apis.getResultFromQueryID(q.id, (qr: { rows: any[] }) => {
          if (qr.rows.length) {
            let finRecords = qr.rows.sort((a, b) => {
              return b.Fiscal_Month - a.Fiscal_Month
            }).sort((a, b) => {
              return b.Fiscal_Year - a.Fiscal_Year
            }).slice(0, 100);
            console.log('financial data for CIF', finRecords);
            // let years = finRecords.map(r => r.Fiscal_Year);
            // console.log('years', years);
            this.latestYear = finRecords[0].Fiscal_Year;
            this.startingYear = this.latestYear - 3;
            this.latestMonth = finRecords[0].Fiscal_Month;
            this.startingMonth = this.latestMonth === 12 ? 1 : this.latestMonth + 1;
            this.years = Array(4).fill(0).map((_, idx) => this.latestYear - idx)
            console.log('years', this.years);
            //this.company.news = qr.rows.map(n => new CompanyNews(n));

            // get benchmarks

            /*this.apis.getBenchmarkTickersForDateRange(this.startingYear, this.startingMonth, this.latestYear, this.latestMonth).subscribe((q2: { id: string }) => {
              this.apis.getResultFromQueryID(q2.id, (qr2: { rows: any[] }) => {
                console.log('got benchmarks', qr2);
              })
            })*/

            this.categories.forEach(cat => {
              cat.chart = null
              cat.breakdown.forEach(b => {
                b.trend = {};
                this.years.forEach((y, i) => {
                  let record = finRecords.filter(r => r.Fiscal_Year === y && r.Fiscal_Month === this.latestMonth)[0];
                  // console.log(record);
                  if (record) {
                    b.trend[record.Fiscal_Year + '-' + (record.Fiscal_Month)] = record[b.name];
                  }
                })
                /*finRecords.forEach(record => {
                  b.trend[record.Fiscal_Year + '-' + record.Fiscal_Month] = record[b.name];
                });*/
                // console.log(b.name, b.trend);
                // b.chart = null;
                this.processMovement(b);
              });
              setTimeout(() => {
                cat.chart = this.getCatChart(cat);
              }, 100);
              this.getCategoryChange(cat);

            });
            console.log('charts created', this.categories);

            this.types = [];
            this.categories.forEach(cat => {
              let t = this.types.filter(t => t.name === cat.type)[0];
              if (!t) {
                t = {
                  name: cat.type,
                  trend: {
                    up: [],
                    down: []
                  }
                }
                this.types.push(t);
              }
              cat.breakdown.forEach(b => {
                if (b.movement > 0) {
                  t.trend.up.push(b.label)
                }
                if (b.movement < 0) {
                  t.trend.down.push(b.label)
                }
              });
            })
            console.log('types created', this.types);

            this.apis.getBenchmarkTickersForDateRange(
              this.startingYear + '-' + this.latestMonth,
              this.latestYear + '-' + this.latestMonth
            ).subscribe((q2: { id: string }) => {
              let setsReceived = 0;
              let benchmarkRows = [];
              this.apis.getAllResultsFromQueryID(q2.id, (benchmarks: {rows: any[]}) => {
                console.log('got all results for benchmarks', benchmarks);
                this.benchmarkTypes = [];
                this.benchmarks.forEach(bm => {
                  let trendsReady = 0;
                  let t = {
                    name: bm.name,
                    trend: {
                      up: [],
                      down: []
                    }
                  }
                  bm.breakdown.forEach(b => {
                    // console.log('looking for benchmark', b.name);
                    let bmRows = benchmarks.rows.filter(br => br.Ticker === b.name);
                    bmRows.sort((a, b) => {
                      return new Date(b.d).getTime() - new Date(a.d).getTime()
                    });
                    // console.log('found rows', bmRows);
                    if (bmRows.length) {
                      // console.log('got tickers for given dates for ' + b.name, qr2);
                      b.trend = {};
                      bmRows.forEach(row => {
                        b.trend[row.d] = (bm.originAdjusted) ? (100 * row.v / bmRows[bmRows.length - 1].v) : row.v;
                      });
                      trendsReady += 1;
                      if (trendsReady === bm.breakdown.length) {
                        setTimeout(() => {
                          bm.chart = this.getCatChart(bm);
                        }, 100);
                      }
                      // this.processMovement(b);
                      let lastYear = bmRows.filter(r => r.d.includes(this.latestYear - 1))[0];
                      b.movement = lastYear ? (
                        bm.percentage ? (
                          bmRows[0].v - lastYear.v
                        ) : ((bmRows[0].v - lastYear.v) * 100 / lastYear.v)
                      ) : 0;
                    }
                    if (b.movement > 0) {
                      t.trend.up.push(b.label)
                    }
                    if (b.movement < 0) {
                      t.trend.down.push(b.label)
                    }
                  })
                  this.benchmarkTypes.push(t);

                })
                console.log('benchmark types created', this.benchmarkTypes);
              });
            });



            this.benchmarks.forEach(bm => {
              let trendsReady = 0;
              bm.breakdown.forEach(b => {
                /*this.apis.getBenchmarkForTicker(b.name).subscribe((q2: { id: string }) => {
                  this.apis.getResultFromQueryID(q2.id, (qr2: { rows: any[] }) => {
                    console.log('got tickers for ' + b.name, qr2);
                  },0, 500)
                });*/
                /*this.apis.getBenchmarkForTickerDateRange(
                  b.name,
                  this.latestYear + '-' + this.latestMonth + '-01',
                  this.startingYear + '-' + this.latestMonth + '-01'
                ).subscribe((q2: { id: string }) => {
                  this.apis.getResultFromQueryID(q2.id, (qr2: { rows: any[] }) => {
                    qr2.rows.sort((a, b) => {
                      return b.d - a.d
                    });
                    // console.log('got tickers for given dates for ' + b.name, qr2);
                    b.trend = {};
                    qr2.rows.forEach(row => {
                      b.trend[row.d] = (bm.name.includes(' % Change')) ? (100 * row.v / qr2.rows[qr2.rows.length - 1].v) : row.v;
                    });
                    trendsReady += 1;
                    if (trendsReady === bm.breakdown.length) {
                      setTimeout(() => {
                        bm.chart = this.getCatChart(bm);
                      }, 100);
                    }
                    // this.processMovement(b);
                    let lastYear = qr2.rows.filter(r => r.d.includes(this.latestYear - 1))[0];
                    b.movement = lastYear ? ((qr2.rows[0].v - lastYear.v) * 100 / lastYear.v) : 0;

                  },0, 500)
                }, e => {
                  if (e.status === 401) {
                    alert('session expired');
                    this.users.logOut();
                  }
                });*/
              })

            })


            this.apis.getGradingReasons(Number(this.companyId), this.latestYear, this.latestMonth).subscribe((r: { financial_health_summary: string, reasons: string[] }) => {
              console.log('grading reasons for CIF', r);
              this.text.summary = r.financial_health_summary;
              this.text.reasons = r.reasons || [];
            }, e => {
              if (e.status === 401) {
                alert('session expired');
                this.users.logOut();
              }
            });
          }
        })
      }, e => {

        if (e.status === 401) {
          alert('session expired');
          this.users.logOut();
        }
      });
    }

  }

  ngOnChanges(changes: SimpleChanges): void {
    /*this.gradingCategories.forEach(g => {
      g.breakdown.forEach(b => {
        b.chart = null;
        this.processMovement(b);
        setTimeout(() => {
          b.chart = this.getChart(b);
        }, 100);
      });
      this.getCategoryChange(g);
    });*/
  }

  ngAfterViewInit(): void {
    console.log(this.categoryDivs);
  }

}
