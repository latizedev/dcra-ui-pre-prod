import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GradingReviewUIComponent } from './grading-review-ui.component';

describe('GradingReviewUIComponent', () => {
  let component: GradingReviewUIComponent;
  let fixture: ComponentFixture<GradingReviewUIComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GradingReviewUIComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GradingReviewUIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
