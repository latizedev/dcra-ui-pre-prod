import {Component, Input, OnInit, Output, EventEmitter, Directive} from '@angular/core';
import {
  faBuilding,
  faChartPie,
  faFlag,
  faStar,
  faStarHalfAlt,
  faTable,
  faUndoAlt
} from '@fortawesome/free-solid-svg-icons';
import {faFlag as faFlag2} from '@fortawesome/free-regular-svg-icons';
import {faStar as faStarOutline} from '@fortawesome/free-regular-svg-icons';
import {
  ApiGradeDistribution,
  ApiOverrideData,
  ApiOverrideResponse,
  CompanyData, CompanyRow,
  DcraUser,
  QueryStatus
} from '../../classes';
import {UserService} from '../../services/user.service';
import {DataService} from '../../services/data.service';
import {SimpleNode} from '../../classes';
import {VisualMakerService} from '../../services/visual-maker.service';
import {ApiService} from '../../services/api.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './grading-home.component.html',
  styleUrls: ['./grading-home.component.scss']
})
export class GradingHomeComponent implements OnInit {

  companies: CompanyData[];
  filteredCompanies: CompanyData[];

  icons = {
    Initial: faStarHalfAlt,
    'Pending Review': faStarOutline,
    'My Reviews': faStar,
    Final: faStar,
    company: faBuilding,
    flag: faFlag,
    flag2: faFlag2,
    pie: faChartPie,
    table: faTable,
    reset: faUndoAlt
  };

  activeUser: DcraUser;

  overridesList: ApiOverrideData[] = [];
  myOverridesList: {
    id: number,
    CIF: string,
    'Recommended Rating': string,
    'Updated Rating': string,
    Updated: Date,
    Status: string,
    Comment: string
  }[] = [];
  overrideColumns = ['CIF', 'Recommended Rating', 'Updated Rating', 'Status', 'Comment', 'Updated'];
  overrideSort = 'CreatedAt';
  overrideSortDir = 0;

  myPendingList: {
    id: number,
    CIF: string,
    Previous: string,
    Recommended: string,
    Review: string,
    ReviewedBy: string,
    Comment: string
  }[] = [];
  pendingColumns = ['CIF', 'Previous', 'Recommended', 'Review', 'ReviewedBy', 'Status', 'Comment', 'Created'];
  pendingSort = 'CIF';
  pendingSortDir = 0;


  ratingsChart: any;
  changeChart: any;
  clickedGrade;
  clickedChange;
  highlightGrade: string;
  highlightChange: string;

  filters: {
    name: string,
    flagged: boolean,
    previous: string,
    recommended: string,
    review: string,
    final: string,
    year: string,
    month: string
  } = {
    name: null,
    flagged: false,
    previous: 'ALL',
    recommended: 'ALL',
    review: 'ALL',
    final: 'ALL',
    year: undefined,
    month: '0'
  }
  mainSort = 'year';
  mainSortDir = 0;

  showDataImporter = false;

  loading = false;
  loaderClass = 'fading';

  fiscalMonth: number;
  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  fiscalYear: number;
  years = [];

  showLatest = true;

  logOut(): void {
    this.userManager.logOut();
  }
  checkGradingChange(c: CompanyData): 'UP' | 'SAME' | 'DOWN' {
    if (c.gradings.recommended && c.gradings.recommended && c.gradings.previous && c.gradings.previous) {
      const rec = parseInt(c.gradings.recommended);
      const prev = parseInt(c.gradings.previous);
      if (rec < prev) {
        return 'UP';
      }
      if (rec === prev) {
        return 'SAME';
      }
      if (rec > prev) {
        return 'DOWN';
      }
    } else {
      return null;
    }
  }

  chartClicked(chart: string, points: { i: number, label: string }[]): void {
    /*if (chart === 'grading') {
      this.clickedGrade = points[0].label;
      this.filters.recommended = points[0].label;
    }
    if (chart === 'change') {
      this.clickedChange = points[0].label;
    }*/
  }

  chartHovered(chart: string, point: { i: number, label: string }): void {
    if (chart === 'grading') {
      this.highlightGrade = point ? point.label : null;
    }
    if (chart === 'change') {
      this.highlightChange = point ? point.label : null;
    }
  }

  highlightCompany(company: CompanyData): boolean {
    return company.gradings.recommended === this.highlightGrade || this.checkGradingChange(company) === this.highlightChange;
  }

  /*showCompany(company: CompanyData): boolean {
    return (!this.clickedGrade || this.clickedGrade === 'undefined' || company.gradings.recommended.value === this.clickedGrade) && (!this.clickedChange || this.clickedChange === 'undefined' || this.checkGradingChange(company) === this.clickedChange);
  }*/

  sort(data: any[], col: string, listName: string) {
  }

  showCompany(company: CompanyData): boolean {

    /*console.log(this.filters.previous === 'ALL' || this.filters.previous === company.gradings.previous.value);
    console.log(this.filters.recommended === 'ALL' || (company.gradings.recommended ? (this.filters.recommended === company.gradings.recommended.value) : false));
    console.log(this.filters.review === 'ALL' || (company.gradings.user ? (this.filters.review === company.gradings.user.value) : false));
    console.log(this.filters.final === 'ALL' || (company.gradings.final ? (this.filters.final === company.gradings.final.value) : false));
    console.log(!this.filters.name || company.name.includes(this.filters.name));
    console.log(this.filters.flagged === company.flagged);*/

    return (this.filters.previous === 'ALL' || this.filters.previous === company.gradings.previous) &&
      (this.filters.recommended === 'ALL' || (company.gradings.recommended ? (this.filters.recommended === company.gradings.recommended) : false)) &&
      (this.filters.review === 'ALL' || (company.gradings.review ? (this.filters.review === company.gradings.review) : false)) &&
      (this.filters.final === 'ALL' || (company.gradings.final ? (this.filters.final === company.gradings.final) : false)) &&
      (!this.filters.name || company.cif.toLowerCase().includes(this.filters.name.toLowerCase())) &&
      (!parseInt(this.filters.month) || (company.fiscalMonth === parseInt(this.filters.month))) &&
      (!this.filters.year || (company.fiscalYear === parseInt(this.filters.year))) &&
      (!this.filters.flagged || this.filters.flagged === company.flagged);
  }

  filterCompanies() {
    this.filteredCompanies = this.companies.filter(c => c.gradings.recommended === this.clickedGrade && this.checkGradingChange(c) === this.clickedChange);
  }

  addCompany(company: CompanyData): void {
    this.loading = true;
    console.log(company);
    this.showDataImporter = false;
    const newCompany = JSON.parse(JSON.stringify(this.companies[0])) as CompanyData;
    newCompany.name = company.name;
    newCompany.cif = company.cif;
    newCompany.currentExposure.customerName = company.name;
    newCompany.currentExposure.companyBackground.text = newCompany.currentExposure.companyBackground.text.replace('Aviato', 'Gosfords');
    newCompany.news.forEach(n => {
      n.title = n.title.replace('Aviato', 'Gosfords');
      n.preview = n.preview.replace('Aviato', 'Gosfords');
    });
    console.log(newCompany);
    this.companies.push(newCompany);
    this.ds.saveCompany(newCompany);
    setTimeout(() => this.loaderClass = 'faded', 1000);
    setTimeout(() => this.loading = false, 2000);
  }

  resetData(): void {
    this.ds.clearLocal();
  }

  /*getGradesDistFromAPI(): void {
    this.apis.token = this.activeUser.token;
    // getting grades distribution
    this.apis.getGradesDistribution('calculated').subscribe((q: { id: string }) => {
      this.apis.getResultFromQueryID(q.id, (qr: ApiGradeDistribution) => {
        console.log('grades distribution', qr);
        const distData = {};
        qr.rows.forEach(row => {
          distData[this.ds.gradingNumberToString(row.Calculated_MAS_Grading)] = row.nCount;
        });
        this.ratingsChart = this.vsm.createDcraPie(distData, ['#29357F', '#27ABE2', '#3CAEA3', '#F6D55C', '#ED553B']);
      })
    }, (e) => {
      this.sessionExpired(e);
    });
  }*/

  getPending() {
    this.myPendingList = [];
    if (!this.activeUser.userRole) {
      let overriddenCIFs = this.overridesList.map(o => o.cifNo);
      console.log('overridden cifs', overriddenCIFs);
      this.myPendingList = this.filteredCompanies.filter(c => overriddenCIFs.indexOf(parseInt(c.cif)) === -1).map(c => ({
        CIF: c.cif,
        id: 0,
        Previous: c.gradings.previous,
        Recommended: c.gradings.recommended,
        Review: c.gradings.review,
        ReviewedBy: '',
        Status: 'Pending Review by Analysts',
        Comment: ''
      }))
    } else {
      console.log('getting overrides for ' + this.activeUser.userRole, this.activeUser.userRole);
      this.apis.getMyPending(this.activeUser.userRole).subscribe((r: ApiOverrideResponse) => {
        console.log('my pending', r);
        this.myPendingList = r.result.data
          .filter(d => d.override && d.override.fiscalMonth === this.fiscalMonth && d.override.fiscalYear === this.fiscalYear)
          .map(d => {
          if (d.auditLog) {
            d.auditLog.sort((a, b) => {
              return new Date(b.updatedAt) > new Date(a.updatedAt) ? 1 : (new Date(a.updatedAt) > new Date(b.updatedAt) ? -1 : 0)
            })
          }
          return {
            CIF: (d.override.cifNo / 10000000).toFixed(7).toString().split('.')[1],
            id: d.override.id,
            Previous: null,
            Recommended: this.ds.gradingNumberToString(d.override.calculatedMasGrading),
            Review: this.ds.gradingNumberToString(d.override.finalizedMasGrading),
            ReviewedBy: d.override.createdBy,
            Status: 'Pending Review by Approver' + this.activeUser.userRole,
            Comment: d.override.justification
          }
        });
        if (this.activeUser.userRole > 1) {
          this.myPendingList.forEach(d => {
            this.apis.getOverrideStatus(d.id).subscribe((result: {result: ApiOverrideData }) => {
              let ovr = result.result;
              console.log(ovr);
              if(ovr.auditLog && ovr.auditLog.length > 1) {
                ovr.auditLog.sort((a,b) => {
                  return new Date(b.updatedAt) > new Date(a.updatedAt) ? 1 : (new Date(a.updatedAt) > new Date(b.updatedAt) ? -1 : 0)
                });
                console.log(ovr.auditLog);
                d.Comment = ovr.auditLog[1].justification;
                d.ReviewedBy = ovr.auditLog[1].modifiedBy;
                if (ovr.auditLog[1].finalizedMasGrading) {
                  d.Review = this.ds.gradingNumberToString(ovr.auditLog[1].finalizedMasGrading);
                }
              }
            })
          })
        }

      }, (e) => {
        this.sessionExpired(e);
      });
    }
  }

  getMyOverrides() {
    this.apis.getMyOverrides().subscribe((r: ApiOverrideResponse) => {
      console.log('my overrides', r);
    }, (e) => {
      this.sessionExpired(e);
    });
  }

  getAllOverrides() {
    this.apis.getAllOverrides(0, 500).subscribe((overrideResponse: ApiOverrideResponse) => {
      console.log('all overrides', overrideResponse);
      // filter by month year
      if (this.showLatest) {
        this.overridesList = overrideResponse.result.data.filter(o => o.fiscalYear === this.fiscalYear && o.fiscalMonth === this.fiscalMonth);
      } else {
        this.overridesList = overrideResponse.result.data;
      }
      console.log('filtered overrides', this.overridesList);
      // filter by user role
      let myOverrides: ApiOverrideData[];
      if (this.activeUser.userRole) {
        if (this.activeUser.userRole === 1) {
          myOverrides = this.overridesList.filter(o => o.auditLog ? (o.auditLog.length ? (
            (o.auditLog[0].currentStep === 1 && o.auditLog[0].status !== 'Awaiting') || (o.auditLog[0].currentStep === 2)) : false) : false);
        } else
        if (this.activeUser.userRole === 2) {
          myOverrides = this.overridesList.filter(o => o.auditLog ? (o.auditLog.length ? (o.auditLog[0].currentStep === 2 && o.auditLog[0].status !== 'Awaiting') : false) : false);
        }
        // myOverrides = this.overridesList.filter(o => o.auditLog ? (o.auditLog.length ? (o.auditLog[0].currentStep > this.activeUser.userRole) : false) : false);
      } else {
        myOverrides = this.overridesList.filter(o => o.createdBy === this.activeUser.email);
      }
      console.log('filtered overrides for user', myOverrides);

      this.myOverridesList = myOverrides
        .map(o => {
          if (o.auditLog) {
            o.auditLog.sort((a, b) => {
              return new Date(b.updatedAt) > new Date(a.updatedAt) ? 1 : (new Date(a.updatedAt) > new Date(b.updatedAt) ? -1 : 0)
            });
          }
          return {
            CIF: (o.cifNo / 10000000).toFixed(7).toString().split('.')[1],
            id: o.id,
            'Recommended Rating': this.ds.gradingNumberToString(o.calculatedMasGrading),
            Status: o.auditLog ? (o.auditLog.length ? (o.auditLog[0].status  + ' - Approver' + o.auditLog[0].currentStep) : '') : '',
            'Updated Rating': o.auditLog ? (o.auditLog.length ? this.ds.gradingNumberToString(o.auditLog[0].finalizedMasGrading || o.finalizedMasGrading) : '') : '',
            Comment: o.auditLog ? (o.auditLog.length ? (o.auditLog[0].justification || (o.auditLog[1] ? o.auditLog[1].justification : o.justification)) : '') : '',
            Updated: o.auditLog ? (o.auditLog.length ? new Date(o.auditLog[0].updatedAt) : null) : null
          }
        });
      this.getPending();

    }, (e) => {
      this.sessionExpired(e);
    });
  }

  prepareMasterList() {
    console.log(this.showLatest);
    this.filteredCompanies = this.showLatest ?
      this.companies.filter(c => c.fiscalMonth === this.fiscalMonth && c.fiscalYear === this.fiscalYear) :
      this.companies;
    this.getAllOverrides();
    this.getMyOverrides();
    this.changeChart = null;
    this.ratingsChart = null;
    setTimeout(() => {
      this.createCharts();
    }, 100);
  }

  initFromApi() {
    // this.apis.getGradesForYearMonth(2018, 6).subscribe((q: { id: string }) => {
    this.apis.getFinalizedGrades().subscribe((q: { id: string }) => {
      this.apis.getResultFromQueryID(q.id, (qr: { rows: CompanyRow[] }) => {
        if (qr.rows) {
          qr.rows
            .sort((a, b) => {
              return b.Fiscal_Month - a.Fiscal_Month
            })
            .sort((a, b) => {
              return b.Fiscal_Year - a.Fiscal_Year
            });
          this.fiscalYear = qr.rows[0].Fiscal_Year;
          this.fiscalMonth = qr.rows[0].Fiscal_Month;
          console.log('all grades', qr);
          // console.log(qr.rows.map(r => r.CIF_NO));
          // console.log(qr.rows.map(r => r.Fiscal_Year + '-' + r.Fiscal_Month));
          // since data is not up to date


          let currentRecords = qr.rows;

          this.companies = [];
          let companiesApiCounter = 1;

          currentRecords.forEach((c, ri) => {
            let company = new CompanyData(c.CIF_NO, c.Fiscal_Year, c.Fiscal_Month);

            company.assignGrading('previous', this.ds.gradingNumberToString(c.MAS_Grading));
            company.assignGrading('recommended', this.ds.gradingNumberToString(c.Calculated_MAS_Grading));
            company.assignGrading('final', this.ds.gradingNumberToString(c.Finalized_MAS_Grading));
            company.flagged = !!c.riskFlag;

            this.companies.push(company);

            if (company) {
              // get final grade, if available
              /*this.apis.getFinalizedGradeForCIF(company.cif).subscribe((q2: { id: string }) => {
                this.apis.getResultFromQueryID(q2.id, (qr2: { rows: CompanyRow[] }) => {
                  console.log('finalized grading for', company.cif, qr2);
                  let finalizedRecord = qr2.rows[0];
                  if (finalizedRecord && finalizedRecord.Fiscal_Year === company.fiscalYear && finalizedRecord.Fiscal_Month === company.fiscalMonth) {
                    company.assignGrading('final', this.gradingNumberToString(qr2.rows[0].Finalized_MAS_Grading));
                  }
                });
              });*/
              // if all companies grading data is received, draw the changes chart
              // console.log(companiesApiCounter, qr.rows.length);
              /*if (companiesApiCounter === qr.rows.length / 2) {
                // this.createLists();
              } else {
                companiesApiCounter++;
              }*/
            } else {
              // company = new CompanyData(c.CIF_NO);
              // this.companies.push(company);
            }
          });
          console.log('all companies', this.companies);
          this.prepareMasterList();
        }
      });
    }, (e) => {
      this.sessionExpired(e);
    });
  }

  sessionExpired(e) {
    if (e.status === 401) {
      alert('session expired');
      this.userManager.logOut();
    }
  }

  clearFilters() {
    this.filters = {
      name: null,
      flagged: false,
      previous: 'ALL',
      recommended: 'ALL',
      review: 'ALL',
      final: 'ALL',
      year: undefined,
      month: '0'
    }
  }

  createRatingsChartFromCompanies() {
    this.ratingsChart = this.vsm.createDcraPie({
      '1.PASS': this.filteredCompanies.filter(c => c.gradings.recommended === '1.PASS').length,
      '2.SM': this.filteredCompanies.filter(c => c.gradings.recommended === '2.SM').length,
      '3.SS': this.filteredCompanies.filter(c => c.gradings.recommended === '3.SS').length,
      '4.DF': this.filteredCompanies.filter(c => c.gradings.recommended === '4.DF').length,
      '5.LOSS': this.filteredCompanies.filter(c => c.gradings.recommended === '5.LOSS').length
    }, ['#29357F', '#27ABE2', '#3CAEA3', '#F6D55C', '#ED553B']);
  }

  createChangesChartFromCompanies() {
    this.changeChart = this.vsm.createDcraPie({
      UP: this.filteredCompanies.filter(c => this.checkGradingChange(c) === 'UP').length,
      DN: this.filteredCompanies.filter(c => this.checkGradingChange(c) === 'DOWN').length,
      SAME: this.filteredCompanies.filter(c => this.checkGradingChange(c) === 'SAME').length
    }, ['#27ABE2', '#F6D55C', '#ED553B']);
  }

  createCharts() {
    this.createRatingsChartFromCompanies();
    this.createChangesChartFromCompanies();
  }

  /*createLists() {
    console.log('creating lists');
    this.lists.forEach(l => l.items = []);
    /!*this.lists = [
      {
        name: 'Pending Review',
        items: this.ds.getCompaniesByStatus('R1', this.companies),
        columns: ['previous', 'recommended']
      },
      {
        name: 'My Reviews',
        // items: this.ds.getCompaniesByStatus('R2', this.companies),
        items: [],
        columns: ['recommended', 'user']
      }
    ];*!/
    this.getPending();
    // this.getMyOverrides();
    this.getAllOverrides();
  }*/

  constructor(
    private userManager: UserService,
    private ds: DataService,
    private vsm: VisualMakerService,
    private apis: ApiService,
    private titleService: Title
  ) {
  }

  ngOnInit(): void {
    let date = new Date();
    // this.fiscalYear = 2020; // date.getFullYear();
    // this.fiscalMonth = 12; // date.getMonth() + 1;
    console.log(this.fiscalYear, this.fiscalMonth);
    this.years = [this.fiscalYear, (this.fiscalYear - 1), (this.fiscalYear - 2), (this.fiscalYear - 3)]
    this.titleService.setTitle('DCRA');
    this.activeUser = this.userManager.getActiveUser();
    console.log('logged in as', this.activeUser);
    this.apis.token = this.activeUser.token;
    this.initFromApi();
    // this.initFromSample();
  }
}
