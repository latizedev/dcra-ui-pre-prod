import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GradingHomeComponent } from './grading-home.component';

describe('HomeComponent', () => {
  let component: GradingHomeComponent;
  let fixture: ComponentFixture<GradingHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GradingHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GradingHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
