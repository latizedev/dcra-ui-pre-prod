import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EarlyWarningDashboadComponent} from './early-warning-dashboad/early-warning-dashboad.component';
import {SharedModule} from '../shared/shared.module';
import {SimplebarAngularModule} from 'simplebar-angular';


@NgModule({
  declarations: [
    EarlyWarningDashboadComponent
  ],
    imports: [
        CommonModule,
        SharedModule,
        SimplebarAngularModule
    ]
})
export class EarlyWarningModule {
}
