import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {faCaretDown, faCaretUp, faFlag, faStarHalfAlt, faUndoAlt} from '@fortawesome/free-solid-svg-icons';
import {CompanyData, CompanyExposure, CompanyExposureApi, CytoElement, DcraUser, GradingBreakdown} from '../../classes';
import {DataService} from '../../services/data.service';
import {Node, Edge} from '@swimlane/ngx-graph';
import {Trend} from '../../classes/trend';
import {VisualMakerService} from '../../services/visual-maker.service';
import {fromEvent, Observable, Subscription} from 'rxjs';
import {ApiService} from "../../services/api.service";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-early-warning-dashboad',
  templateUrl: './early-warning-dashboad.component.html',
  styleUrls: ['./early-warning-dashboad.component.scss']
})
export class EarlyWarningDashboadComponent implements OnInit, OnDestroy {

  currentUser: DcraUser;

  resizeObservable$: Observable<Event>;
  resizeSubscription$: Subscription;

  resizeTimer;

  quarters: string[];
  colors = ['#27ABE2', '#29357F', '#999999', '#7458D9', '#d99958', '#d95894'];
  peers: CompanyData[];
  selectedPeerID: string;
  quarterSelected: string;

  company: CompanyData;
  // companies: CompanyData[];
  companyID: string;
  icons = {
    grading: faStarHalfAlt,
    up: faCaretUp,
    dn: faCaretDown,
    undo: faUndoAlt,
    flag: faFlag
  };

  topPointers: {category: string, growth: number, text: string};
  midPointers: {category: string, growth: number, text: string};

  companyTrendBreakdown: GradingBreakdown[] = [
    {
      text: '',
      name: 'PL_EBITDA',
      label: 'EBITDA',
      movement: 0,
      trend: {}
    },
    {
      text: '',
      name: 'CF_NET_CASHFLOW',
      label: 'Net Cashflow',
      movement: 0,
      trend: {}
    },
    {
      text: "<b>Decrease</b> in <b>Revenue</b> by more than <b>40%</b>",
      name: "PL_REVENUES",
      label: 'Total Revenue',
      movement: 0,
      trend: {}
    },
  ]
  companyTrend;

  industryTrendBreakdown: GradingBreakdown[] = [
    {
      text: '',
      name: 'SPX Index',
      label: 'S&P 500 INDEX US',
      movement: 0,
      trend: {}
    },
    {
      text: '',
      name: 'INDU Index',
      label: 'Dow Jones Industrial Average',
      movement: 0,
      trend: {}
    },
    {
      text: '',
      name: 'TPX Index',
      label: 'TOKYO',
      movement: 0,
      trend: {}
    },
    {
      text: '',
      name: 'SENSEX Index',
      label: 'Mumbai',
      movement: 0,
      trend: {}
    },
    {
      text: '',
      name: 'STI Index',
      label: 'Singapore',
      movement: 0,
      trend: {}
    }
  ];
  industrialTrend;

  gradingData: {[key: string]: any};
  gradingTrend;

  ratingsData: {[key: string]: any};
  ratingTrend;

  benchmarkSample;


  spiderList: { key: string, name: string, ratio?: {a: string, b: string} }[] = [
    {
      name: 'Investing Cashflow',
      key: 'CF_CASH_FROM_INVST_ACVT'
    },
    {
      name: 'Financing Cashflow',
      key: 'CF_CASH_FROM_FINCNG_ACVT'
    },
    {
      name: 'Total Revenue',
      key: 'PL_REVENUES'
    },
    {
      name: 'Total Liabilities',
      key: 'LIAB_TOTAL'
    },
    {
      name: 'Net Cashflow',
      key: 'CF_NET_CASHFLOW'
    },
    {
      name: 'Total Asset',
      key: 'TOTAL_AST'
    },
    {
      name: 'Revenue Growth',
      key: 'PL_REVENUES_GROWTH',
    },
    {
      name: 'Operating Margin',
      key: 'NA',
      ratio: {a: 'PL_EBIT', b: 'PL_REVENUES'}
    },
    {
      name: 'Revenue / Total Asset',
      key: 'NA',
      ratio: {a: 'PL_REVENUES', b: 'TOTAL_AST'}
    },
    {
      name: 'Current Assets / Current Liabilities',
      key: 'NA',
      ratio: {a: 'CURR_AST', b: 'CURR_LIAB'}
    },
  ];

  exposureNetwork: CytoElement;
  financialStandingSpider;

  "latestFinancials" = [
    {
      name: "Revenue",
      key: 'PL_REVENUES',
      value: 0
    },
    {
      name: "Cash Flow",
      key: 'CF_NET_CASHFLOW',
      value: 0
    },
    {
      name: "EBITDA",
      key: 'PL_EBITDA',
      value: 0
    },
    {
      name: "Total Asset",
      key: 'TOTAL_AST',
      value: 0
    }
  ];

  latestYear: number;
  latestMonth: number;

  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  // latestYear: number;
  // latestMonth: number;


  constructor(
    private route: ActivatedRoute,
    private ds: DataService,
    private vsm: VisualMakerService,
    private apis: ApiService,
    private user: UserService
  ) {
  }

  ngOnDestroy(): void {
    // throw new Error('Method not implemented.');
  }

  changePeer(): void {
    this.financialStandingSpider = null;
     let peer = this.peers.filter(p => p.cif === this.selectedPeerID)[0];
    setTimeout(() =>
      this.financialStandingSpider = this.vsm.createFinancialStandingSpiderSinglePeer(this.company, peer), 100);
    // setTimeout(() => this.financialStandingSpider = this.vsm.createFinancialStandingSpider(this.companies, this.company), 100);
  }

  changeQuarter(): void {
    this.exposureNetwork = null;
    setTimeout(() => {
      // this.exposureNetwork = this.vsm.createExposureNetwork(this.quarterSelected, this.company, null)
      this.exposureNetwork = this.vsm.createCytoExposureNetworkOld(this.quarterSelected, this.company)
    }, 100);
  }

  createSpiderList(company: CompanyData, records: any) {
    this.spiderList.forEach(item => {
      if (item.ratio) {
        let val = records[item.ratio.a] / records[item.ratio.b];
        company.financialStanding[item.name + '<br>' + this.ds.nFormatter(val, 2)] = val
      } else {
        let val = records[item.key];
        company.financialStanding[item.name + '<br>' + this.ds.nFormatter(val, 2)] = val
      }
    })
  }

  resetData(): void {
    this.ds.clearLocal();
  }

  ngOnInit(): void {

    this.currentUser = this.user.getActiveUser();
    console.log(this.currentUser);
    this.apis.token = this.currentUser.token;

    /*this.resizeObservable$ = fromEvent(window, 'resize');
    this.resizeSubscription$ = this.resizeObservable$.subscribe(evt => {
      clearTimeout(this.resizeTimer);
      this.resizeTimer = setTimeout(() => this.redrawCharts(), 1000);
    });*/

    this.companyID = decodeURIComponent(this.route.snapshot.paramMap.get('id'));
    this.company = new CompanyData(this.companyID);

    // exposure
    this.apis.getCompanyData('exposures', this.company.cif).subscribe((q: { id: string }) => {
      this.apis.getResultFromQueryID(q.id, (qr: { rows: CompanyExposureApi[] }) => {
        console.log('received exposure data for CIF', qr)
        this.company.currentExposure = new CompanyExposure(this.company.cif, new Date(this.latestYear, this.latestMonth - 1));
        if (qr.rows.length) {
          this.company.currentExposure.updateFromExposureApi(qr.rows);

          setTimeout(() => {

            this.exposureNetwork = this.vsm.createCytoExposureNetwork(this.company);
            console.log(this.exposureNetwork);

          }, 500);
        }


      })
    }, e => {

      if (e.status === 401) {
        alert('session expired');
        this.user.logOut();
      }
    });

    // financials
    this.apis.getCompanyData('financials', this.company.cif).subscribe((q: { id: string }) => {
      this.apis.getResultFromQueryID(q.id, (qr: { rows: any[] }) => {
        if (qr.rows.length) {
          let finRecords = qr.rows.sort((a, b) => {
            return b.Fiscal_Month - a.Fiscal_Month
          }).sort((a, b) => {
            return b.Fiscal_Year - a.Fiscal_Year
          }).slice(0, 100);
          console.log('financial  data for CIF', finRecords);
          // let years = finRecords.map(r => r.Fiscal_Year);
          // console.log('years', years);
          this.latestYear = finRecords[0].Fiscal_Year;
          this.latestMonth = finRecords[0].Fiscal_Month;

          // grades and ratings
          this.apis.getGradesForCif(this.company.cif).subscribe((q2: { id: string }) => {
            this.apis.getResultFromQueryID(q2.id, (qr2: { rows: any[] }) => {
              if (qr2.rows.length) {

                let gradesRecords = qr2.rows.sort((a, b) => {
                  return b.Fiscal_Month - a.Fiscal_Month
                }).sort((a, b) => {
                  return b.Fiscal_Year - a.Fiscal_Year
                }).slice(0, 100);

                console.log('grades data for CIF', gradesRecords);

                this.gradingData = {};
                this.ratingsData = {};
                this.gradingTrend = null;
                this.ratingTrend = null;
                this.companyTrend = null;


                this.months.forEach((month, i) => {
                  let y = this.latestYear;
                  let m = this.latestMonth - i;
                  if (m < 1) {
                    y -= 1;
                    m += 12
                  }
                  console.log('creating records for ' + y + '-' + m);
                  let gradesRecord = gradesRecords.filter(r => r.Fiscal_Year === y && r.Fiscal_Month === m)[0];
                  let finRecord = finRecords.filter(r => r.Fiscal_Year === y && r.Fiscal_Month === m)[0];
                  console.log('grades record for month', gradesRecord);
                  console.log('financial record for month', finRecord);
                  if (gradesRecord) {
                    this.gradingData[this.months[m-1] + ' ' + y] = this.ds.gradingNumberToString(gradesRecord.Calculated_MAS_Grading);
                    this.ratingsData[this.months[m-1] + ' ' + y] = gradesRecord.INTERNAL_RATING;
                  }

                  this.companyTrendBreakdown.forEach(b => {
                    // let record = finRecords.filter(r => r.Fiscal_Year === y && r.Fiscal_Month === this.latestMonth)[0];
                    // console.log(record);
                    if (finRecord) {
                      b.trend[y + '-' + m] = finRecord[b.name];
                    }
                  });
                });
                console.log('grading trend Data',  this.gradingData);
                console.log('ratings trend Data',  this.ratingsData);
                console.log('company trends breakdown',  this.companyTrendBreakdown);

                setTimeout(() => {

                  this.gradingTrend = this.vsm.createGradingTrend(this.gradingData);

                  this.ratingTrend = this.vsm.createRatingTrend(this.ratingsData);

                  this.companyTrend = this.vsm.createTimeSeries(this.companyTrendBreakdown.map(b => ({
                    name: b.label || b.name,
                    unit: 'Y-O-Y',
                    steps: b.trend
                  })), true);

                  console.log('company trend created', this.companyTrend);
                  console.log('grading trend created', this.gradingTrend);
                  console.log('ratings trend created', this.ratingTrend);

                }, 100);
              }
            })
          })



          /*this.apis.getAllBenchmarkTickers().subscribe((q2: { id: string }) => {
            this.apis.getResultFromQueryID(q2.id, (qr2: { rows: any[] }) => {
              console.log('got all tickers for', qr2);
            },0, 500)
          });*/

            let industryTrendsReady = 0;
            this.industryTrendBreakdown.forEach(b => {
              /*this.apis.getBenchmarkForTicker(b.name).subscribe((q2: { id: string }) => {
                this.apis.getResultFromQueryID(q2.id, (qr2: { rows: any[] }) => {
                  console.log('got tickers for ' + b.name, qr2);
                },0, 500)
              });*/
              this.apis.getBenchmarkForTickerDateRange(
                b.name,
                this.latestYear + '-' + this.latestMonth + '-01',
                (this.latestYear - 1) + '-' + this.latestMonth + '-01'
              ).subscribe((q2: { id: string }) => {
                this.apis.getResultFromQueryID(q2.id, (qr2: { rows: any[] }) => {
                  qr2.rows.sort((a, b) => {
                    return b.d - a.d
                  });
                  console.log('got tickers for given dates for ' + b.name, qr2);
                  b.trend = {};
                  qr2.rows.forEach(row => {
                    b.trend[row.d] = (row.v - qr2.rows[qr2.rows.length - 1].v) * 100 / qr2.rows[qr2.rows.length - 1].v;
                  });
                  industryTrendsReady += 1;
                  if (industryTrendsReady === this.industryTrendBreakdown.length) {
                    this.industrialTrend = null;
                    setTimeout(() => {
                      this.industrialTrend = this.vsm.createTimeSeries(this.industryTrendBreakdown.map(b => ({
                        name: b.label || b.name,
                        unit: 'Y-O-Y',
                        steps: b.trend
                      })), true);
                      console.log('Industrial trend created', this.industrialTrend)
                    }, 100);
                  }
                  // this.processMovement(b);

                },0, 500)
              }, e => {
                if (e.status === 401) {
                  alert('session expired');
                  this.user.logOut();
                }
              });
            });
        }

        // spider and exposure

        this.company.latestFinancials = [];
        this.latestFinancials.forEach(fin => {
          // fin.value = qr.rows[0][fin.key];
          this.company.latestFinancials.push({
            name: fin.name,
            value: qr.rows[0][fin.key]
          })
        });
        console.log(this.company.latestFinancials);
        this.createSpiderList(this.company, qr.rows[0]);
        console.log('created spider data', this.company.financialStanding);

        // get peers
        let peersReady = 0;
        this.apis.getAllCompanyData('backgrounds').subscribe((q: { id: string }) => {
          this.apis.getResultFromQueryID(q.id, (qr: { rows: any[] }) => {
            console.log('received background data for all CIFs', qr);
            this.peers = qr.rows.map(c => new CompanyData(c.CIF_NO)); // todo: filter by industry or similar
            console.log('peers', this.peers);
            this.peers.forEach((peer, p, peers) => {
              this.apis.getCompanyData('financials', peer.cif).subscribe((q: { id: string }) => {
                this.apis.getResultFromQueryID(q.id, (qr: { rows: any[] }) => {
                  let currentRows = qr.rows.filter(r => r.Fiscal_Year === this.latestYear && r.Fiscal_Month === this.latestMonth)
                  console.log('matched record for', peer.cif, currentRows);
                  if (currentRows.length) {
                    /*qr.rows.sort((a: any, b: any) => {
                      return b.Fiscal_Year - a.Fiscal_Year
                    });*/
                    console.log('received financial data for peer', peer.cif, currentRows);
                    peer.latestFinancials = [];
                    this.latestFinancials.forEach(fin => {
                      // fin.value = qr.rows[0][fin.key];
                      peer.latestFinancials.push({
                        name: fin.name,
                        value: currentRows[0][fin.key]
                      })
                    });
                    this.createSpiderList(peer, currentRows[0]);
                    console.log(peersReady);
                  } else {
                    // if no records
                    peers.splice(p, 1);
                  }
                  peersReady += 1;
                  if (peersReady === this.peers.length) {
                    setTimeout(() => {
                      console.log('creating spider chart', this.peers, this.company);
                      this.financialStandingSpider = this.vsm.createFinancialStandingSpider(this.peers, this.company);
                      // this.exposureNetwork = this.vsm.createCytoExposureNetwork(this.quarterSelected, this.company, null);
                      // this.showCharts = true;
                    }, 200)
                  }
                  // this.news = qr.rows.map(n => new CompanyNews(n));
                })
              });
            });
          })
        }, e => {

          if (e.status === 401) {
            alert('session expired');
            this.user.logOut();
          }
        });

      })
    }, e => {

      if (e.status === 401) {
        alert('session expired');
        this.user.logOut();
      }
    });
    // nlg
    this.apis.getCompanyData('nlgData', this.company.cif).subscribe((q: { id: string }) => {
      this.apis.getResultFromQueryID(q.id, (qr: { rows: any[] }) => {
        console.log('received nlgData data for CIF', qr)
        if (qr.rows.length) {


          setTimeout(() => {

          }, 500);
        }


      })
    }, e => {

      if (e.status === 401) {
        alert('session expired');
        this.user.logOut();
      }
    });
    // macro tickers

    /*this.companies = this.ds.getCompanies();
    this.company = this.companies.filter(c => c.id === this.companyID)[0];
    this.peers = this.companies.filter(c => c.id !== this.companyID);*/
    // this.quarters = Object.keys(this.company.totalRiskExposure);
    // this.quarterSelected = this.quarters[this.quarters.length - 1];
    // should be last four quarters, or each chart should have its own quarters

    setTimeout(() => {
      // this.redrawCharts();
    }, 200);
  }

}
