import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EarlyWarningDashboadComponent } from './early-warning-dashboad.component';

describe('EarlyWarningDashboadComponent', () => {
  let component: EarlyWarningDashboadComponent;
  let fixture: ComponentFixture<EarlyWarningDashboadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EarlyWarningDashboadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EarlyWarningDashboadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
