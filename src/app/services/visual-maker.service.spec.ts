import { TestBed } from '@angular/core/testing';

import { VisualMakerService } from './visual-maker.service';

describe('VisualMakerService', () => {
  let service: VisualMakerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VisualMakerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
