import {Injectable} from '@angular/core';
import {CompanyData, SimpleNode} from '../classes';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  companies: CompanyData[];

  constructor() {
  }

  /*getCompanyNodesFromCompanies(companies: CompanyData[], latestGradingKey?: 'recommended' | 'user' | 'final'): SimpleNode[] {
    return companies.map(c => {
      return {
        id: c.id,
        label: c.name,
        flagged: c.flagged,
        data: {
          previous: c.gradings.previous,
          latest: latestGradingKey ? c.gradings[latestGradingKey] : null
        }
      };
    });
  }*/
  getCompanies(): CompanyData[] {
    this.companies = this.getCompaniesFromStorage();
    console.log(this.companies);
    if (!this.companies || !this.companies.length) {
      this.companies = [];
      this.saveCompaniesToStorage();
    }
    return this.companies;
  }

  getCompaniesFromStorage(): CompanyData[] {
    return JSON.parse(localStorage.getItem('companies'));
  }

  saveCompaniesToStorage(): void {
    localStorage.setItem('companies', JSON.stringify(this.companies));
  }

  gradingNumberToString(num: number): '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS' {
    if (num) {
      return num === 1 ? '1.PASS' : (num === 2 ? '2.SM' : (num === 3 ? '3.SS' : (num === 4 ? '4.DF' : (num === 5 ? '5.LOSS' : null))));
    } else {
      return null
    }
  }

  /*getCompanyFromStorage(companyID: string): CompanyData {
    this.companies = this.getCompaniesFromStorage();
    return this.companies.filter(c => c.id === companyID)[0];
  }

  updateWithLocalData(): void {
    this.jsonCompanies.map(c => this.getCompany(c.id));
  }*/

  checkIfSaved(companyID: string): boolean {
    return this.companies.map(c => c.cif).indexOf(companyID) > -1;
  }

  getCompanyById(companyId: string): CompanyData {
    return this.companies.filter(c => c.cif === companyId)[0];
  }

  saveCompany(company: CompanyData): void {
    this.companies = this.getCompaniesFromStorage();
    if (this.checkIfSaved(company.cif)) { // company already there
      this.companies.splice(this.companies.map(c => c.cif).indexOf(company.cif), 1, company);
    } else { // new company
      this.companies.push(company);
    }
    this.saveCompaniesToStorage();
  }

  resetCompanyToJson(company): void {
    // localStorage.removeItem(company.id);
  }

  clearLocal(): void {
    localStorage.removeItem('companies');
    this.getCompanies();
    window.location.reload();
  }

  nFormatter(num, digits) {
    const lookup = [
      { value: 1, symbol: "" },
      { value: 1e3, symbol: "k" },
      { value: 1e6, symbol: "M" },
      { value: 1e9, symbol: "G" },
      { value: 1e12, symbol: "T" },
      { value: 1e15, symbol: "P" },
      { value: 1e18, symbol: "E" }
    ];
    const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var item = lookup.slice().reverse().find(function(item) {
      return num >= item.value;
    });
    return item ? (num / item.value).toFixed(digits).replace(rx, "$1") + item.symbol : "0";
  }
  getCurrencyAmountFromString(str: string): {currency: string, amount: number} {
    // FX: USD394k, Currency option: USD40k
    let map = {
      k: 1000,
      m: 1000000,
      mil: 1000000,
      b: 1000000000
    }
    console.log('processing exposure data ', str)
    let actualAmount: number;
    let float = parseFloat(str.match(/-?(?:\d+(?:\.\d*)?|\.\d+)/)[0]);
    let currency = '';
    let multiplier = 1;
    console.log('float', float);
    let array = (str.split(float.toString()));
    console.log('array', array);
    if (array.length > 1) {
      currency = array[0].split(' ').pop().toUpperCase();
      multiplier = map[array[1].toLowerCase().charAt(0)] || 1;
    }
    console.log('multiplier', multiplier);
    let amount = float * multiplier;
    console.log('amount', amount);
    return {currency, amount}
  }

  mapFinancialColumnsToLabels() {
    let map = {
      PL_REVENUES: 'TOTAL REVENUE',
      PL_REVENUES_GROWTH: 'TOTAL REVENUE GROWTH',
      PL_COST_OF_SALES: 'cost of sales',
      PL_GROS_PROFIT: 'gross profit',
      PL_SG_AND_A_EXPNS: 'SG&A expenses',
      PL_RENT: 'rental expense',
      PL_GROS_PROFIT_OTHER: 'other operating income / expense',
      PL_EBIT: 'earnings before interest and tax / operating income',
      PL_INT_INCM: 'interest income',
      PL_INT_EXPNS: 'interest expense',
      PL_NON_OPE_INCM_EXPNS: 'non-operating income / expense',
      PL_EBIT_OTHER: 'FX gain / loss',
      PL_EBT: 'Pretax income',
      PL_TAXES: 'Taxes',
      PL_MINORITY_INT: 'Minority Interest',
      PL_EXTRDNRY_GAIN_OR_LOSS: 'Extraordinary gain / loss',
      PL_EBT_OTHER: 'any other gain / loss',
      PL_NET_INCM: 'net income',
      PL_EBITDA: 'EBITDA',
      PL_DEPRE_AMORTZATION: 'depreciation / amortization',
      /*PL_EXP_REVENUES
      PL_EXP_REVENUES_GROWTH
      PL_EXP_COST_OF_SALES
      PL_EXP_GROS_PROFIT
      PL_EXP_SG_AND_A_EXPNS
      PL_EXP_RENT
      PL_EXP_GROS_PROFIT_OTHER
      PL_EXP_EBIT
      PL_EXP_INT_INCM
      PL_EXP_INT_EXPNS
      PL_EXP_NON_OPE_INCM_EXPNS
      PL_EXP_EBIT_OTHER
      PL_EXP_EBT
      PL_EXP_TAXES
      PL_EXP_MINORITY_INT
      PL_EXP_EXTRDNRY_GAIN_OR_LOSS
      PL_EXP_EBT_OTHER
      PL_EXP_NET_INCM
      PL_EXP_EBITDA
      PL_EXP_DEPRE_AMORTZATION
      PL_MRG_REVENUES
      PL_MRG_REVENUES_GROWTH
      PL_MRG_COST_OF_SALES
      PL_MRG_GROS_PROFIT
      PL_MRG_SG_AND_A_EXPNS
      PL_MRG_RENT
      PL_MRG_GROS_PROFIT_OTHER
      PL_MRG_EBIT
      PL_MRG_INT_INCM
      PL_MRG_INT_EXPNS
      PL_MRG_NON_OPE_INCM_EXPNS
      PL_MRG_EBIT_OTHER
      PL_MRG_EBT
      PL_MRG_TAXES
      PL_MRG_MINORITY_INT
      PL_MRG_EXTRDNRY_GAIN_OR_LOSS
      PL_MRG_EBT_OTHER
      PL_MRG_NET_INCM
      PL_MRG_EBITDA
      PL_MRG_DEPRE_AMORTZATION*/
      CF_NET_INCM: 'net income',
      CF_DEPRE_AMORTZATION: 'depreciation / amortization',
      CF_TAXES: 'Taxes(Deferred etc)',
      CF_GAIN_FROM_PP_AND_E: 'Gain(Loss)from PP&E Sales(-/+)',
      CF_NET_WORKING_CAPITAL: 'Net Working Capital',
      CF_OTHER_OPE_CASHFLOW: 'Other Operating Cashflow',
      CF_CASH_FROM_OPE_ACVT: 'operating cashflow',
      CF_CAPEX: 'capex',
      CF_INVST: 'investments',
      CF_ACQUISITIONS: 'acquisitions',
      CF_PROCEEDS_FROM_PP_AND_E: 'proceeds from sale of PPE',
      CF_OTHER_INVST_CASHFLOW: 'other investment cashflow',
      CF_CASH_FROM_INVST_ACVT: 'investing cashflow',
      CF_SALE_OF_STOCK: 'sale of stock',
      CF_PURCHASE_OF_STOCK: 'purchase of stock',
      CF_CASH_DIVIDNDS: 'cash dividends',
      CF_DEBT_BORROWING: 'debt borrowings',
      CF_DEBT_REPAYMENT: 'debt repayment',
      CF_OTHER_FINCNG_CASHFLOW: 'other financing cashflow',
      CF_CASH_FROM_FINCNG_ACVT: 'financing cashflow',
      CF_OTHER_FLUCTUATION_OF_CASH: 'other fluctuation of cash',
      CF_NET_CASHFLOW: 'net cashflow',
      CF_CASH_AT_BEGIN_OF_FISCAL_Y: 'cash at beginning of year',
      CF_CASH_AT_END_OF_FISCAL_Y: 'cash at end of year',
      CASH_AND_CASH_EQUIVALENTS: 'cash and cash equivalents',
      ACCOUNTS_RECEIVABLES: 'accounts receivables',
      INVENTORIES: 'inventories',
      PREPAID_EXPNS: 'prepaid expenses',
      OTHER_CURR_AST: 'other current assets',
      CURR_AST: 'Total current assets',
      NET_PLANT_PRPTY_AND_EQPMNT: 'Net PPE',
      GROS_PLANT_PRPTY_AND_EQPMNT: 'Gross PPE',
      ACCUMLATED_DEPRE: 'Accumulated depreciation',
      OTHER_TANGIBLE_AST: 'Other tangible assets',
      TANGIBLE_AST: 'Total tangible assets',
      GOODWILL: 'Goodwill',
      OTHER_INTANGIBLE_AST: 'Other intangible assets',
      INTANGIBLE_AST: 'Total intangible assets',
      INVST: 'investments',
      DEFFERED_CHARGES: 'deferred charges',
      OTHER_AST: 'other non current assets',
      NON_CURR_AST_TOTAL: 'Total non-current assets',
      TOTAL_AST: 'Total Assets',
      SHORT_TERM_DEBT: 'short term debt',
      LONG_TERM_DEBT_DUE_IN_ONE_Y: 'long term debt due in one year',
      NOTE_PAYABLE: 'note payable',
      ACCOUNTS_PAYABLE: 'accounts payables',
      ACCURED_EXPNS: 'accrued expenses',
      TAX_PAYABLE: 'tax payable',
      OTHER_CURR_LIAB: 'other current liabilities',
      CURR_LIAB: 'Total current liaibilities',
      LONG_TERM_DEBT: 'Total long term debt',
      LONG_TERM_BORROWING: 'long term borrowings',
      BOND: 'bonds',
      SUBORDINATE_DEBT: 'subordinated debt',
      DEFFERED_TAXES: 'deferred taxes',
      OTHER_LONG_TERM_LIAB: 'other long term liabilities',
      MINORITY_INT: 'Minority Interest',
      LONG_TERM_LIAB: 'Total non-current liabilities',
      LIAB_TOTAL: 'Total Liabilities',
      COMMON_STOCK: 'share capital',
      ADDITIONAL_PAID_IN_CAPITAL: 'additional capial injection',
      OTHER_RSRV: 'other reserves',
      RETAINED_EARNINGS: 'retained earnings',
      OTHERS: 'other equity components',
      SHAREHOLDERS_EQUITY: 'Total shareholders&quot; equity',
      TOTAL_LIAB_AND_EQUITY: 'Sum of total liabilities and total shareholders&quot; equity',
      OFFBALANCE_LIAB: 'off balance sheet liabilities',
      GROS_DEBT: 'Sum of total long term debt, short term debt, long term debt due in one year and note payable',
      NON_PERFORMING_AST: 'non-performing assets',
      RSRV_FOR_NON_PERFORMING_AST: 'reserve for non-performing assets',
      UNRALZD_HOLDING_GAIN_LOSSES: 'provision for unrealised loss',
      REAL_NET_WORTH: 'shareholders&quot; equity in substance',
      ACCOUNTING_STANDARD: 'accounting standard'
    }
  }
}
