import {Injectable} from '@angular/core';
import {Trend} from '../classes/trend';
import {CompanyData, CompanyExposure, CompanyRelation, CytoElement} from '../classes';
import {Edge, Node} from '@swimlane/ngx-graph';
import {EdgeDefinition, NodeDefinition} from 'cytoscape';
import {DataService} from "./data.service";

@Injectable({
  providedIn: 'root'
})
export class VisualMakerService {

  colors = ['#27ABE2', '#29357F', '#999999', '#7458D9', '#d99958', '#d95894'];

  constructor(
    private data: DataService
  ) {
  }

  createTimeSeries(trends: Trend[], horizontal?: boolean) {
    return {
      data: trends.map(t => ({
        x: Object.keys(t.steps),// ['2013-10', '2013-11', '2013-12'],
        y: Object.values(t.steps),// [1, 3, 6],
        name: t.name,
        type: 'scatter'
      }),
      ),
      layout: {
        margin: {
          l: 75,
          r: 50,
          b: horizontal ? 25 : 50,
          t: 20,
          pad: 0
        },
        legend: {
          orientation: horizontal ? 'h' : 'v',
          y: horizontal ? -0.4 : null
        },
        xaxis: {
          ticklen: 10,
          tickcolor: '#fff',
        },
        yaxis: {
          showticklabels: true,
          title: null,
          ticklen: 10,
          tickcolor: '#fff',
          tickmode: 'array'
        },
        width: 400,
        height: 200
      }
    }
  }

  createSimpleChartFromTrends(trends: Trend[]): any {
    return {
      data: trends.map((t, i) => this.createTrendDatum(t, this.colors[i])),
      layout: {
        margin: {
          l: 75,
          r: 50,
          b: 50,
          t: 20,
          pad: 0
        },
        legend: {
          orientation: 'v'
        },
        xaxis: {
          ticklen: 10,
          tickcolor: '#fff',
          type: 'category'
        },
        yaxis: {
          showticklabels: true,
          title: null,
          ticklen: 10,
          tickcolor: '#fff',
          tickmode: 'array'
        },
        width: 400,
        height: 200
      }
    };
  }

  createTrendDatum(trend: Trend, color = '#27ABE2', fill?: 'tozeroy' | 'tonexty'): any {
    return {
      name: trend.name,
      x: Object.keys(trend.steps),
      y: Object.values(trend.steps),
      type: 'scatter',
      mode: 'lines+markers',
      marker: {
        color
      },
      fill
    };
  }

  createIndustrialTrend(company: CompanyData): any {
    return {
      data: company.industryTrend.map((t, i) => this.createTrendDatum(t, this.colors[i])),
      layout: {
        margin: {
          l: 100,
          r: 50,
          b: 20,
          t: 50,
          pad: 0
        },
        legend: {
          orientation: 'h',
          y: -0.4
        },
        xaxis: {
          ticklen: 10,
          tickcolor: '#fff',
          type: 'category'
        },
        yaxis: {
          title: {
            text: 'QoQ % change', standoff: 10
          },
          ticklen: 10,
          tickcolor: '#fff',
          tickmode: 'array'
        },
        width: 600,
        height: 300
      }
    };
  }

  createCompanyTrend(company: CompanyData): any {
    return {
      data: company.financialTrend.map((t, i) => this.createTrendDatum(t, this.colors[i])),
      layout: {
        margin: {
          l: 100,
          r: 50,
          b: 20,
          t: 50,
          pad: 0
        },
        legend: {
          orientation: 'h',
          y: -0.4
        },
        xaxis: {
          ticklen: 10,
          tickcolor: '#fff',
          type: 'category'
        },
        yaxis: {
          title: {
            text: 'QoQ % change', standoff: 10
          },
          ticklen: 10,
          tickcolor: '#fff',
          tickmode: 'array'
        },
        width: 600,
        height: 300
      }
    };
  }

  createGradingTrend(steps: {[key: string]: any}): any {
    return {
      data: [
        {
          x: Object.keys(steps).reverse(),
          y: Object.values(steps).map(t => 6 - parseInt(t.split('.')[0], 10)).reverse(),
          type: 'bar',
          text: Object.values(steps).reverse(),
          textposition: 'auto',
          hoverinfo: 'none',
          marker: {
            color: '#27ABE2'
          }
        }
      ],
      layout: {
        margin: {
          l: 50,
          r: 50,
          b: 50,
          t: 20,
          pad: 0
        },
        xaxis: {
          ticklen: 10,
          tickcolor: '#fff',
          type: 'category'
        },
        yaxis: {
          showticklabels: false
        },
        width: 600,
        height: 300
      }
    };
  }

  createRatingTrend(steps: {[key: string]: any}): any {
    const values = Object.values(steps);
    const valuesToNum = {
      A1: 9,
      A2: 8,
      A3: 7,
      B1: 6,
      B2: 5,
      B3: 4,
      C1: 3,
      C2: 2,
      C3: 1,
    };
    return {
      data: [
        {
          x: Object.keys(steps).reverse(),
          y: values.map(v => valuesToNum[v]).reverse(),
          type: 'bar',
          text: values.reverse(),
          textposition: 'auto',
          hoverinfo: 'none',
          marker: {
            color: '#27ABE2'
          }
        }
      ],
      layout: {
        xaxis: {
          ticklen: 10,
          tickcolor: '#fff',
          type: 'category'
        },
        margin: {
          l: 20,
          r: 20,
          b: 50,
          t: 20,
          pad: 0
        },
        yaxis: {
          showticklabels: false
        },
        width: 600,
        height: 300
      }
    };
  }

  createSpiderDatum(company: CompanyData, color = '#27ABE2'): any {

    const theta = Object.keys(company.financialStanding);

    return {
      name: company.name,
      type: 'scatterpolar',
      marker: {
        color
      },
      r: Object.values(company.financialStanding),
      theta,
      hovertemplate: '%{theta} : %{r:.2f}',
      fill: 'toself'
    };
  }

  createFinancialStandingSpider(companies: CompanyData[], company: CompanyData, peerID?: string): any {
    const data = [this.createSpiderDatum(company)];
    if (peerID) {
      const peer = companies.filter(c => c.cif === peerID)[0];
      data.push(this.createSpiderDatum(peer, '#29357F'));
    }
    return {
      data,
      layout: {
        margin: {
          l: 180,
          r: 180,
          b: 60,
          t: 50,
          pad: 10
        },
        polar: {
          radialaxis: {
            visible: true,
            rangemode: 'normal'
          }
        },
        legend: {
          orientation: 'h'
        }
      }
    };
  }

  createFinancialStandingSpiderSinglePeer(company: CompanyData, peer?: CompanyData): any {
    console.log('creating spider for', company.cif,  peer ? peer.cif : 'no peers' )
    const data = [this.createSpiderDatum(company)];
    if (peer) {
      data.push(this.createSpiderDatum(peer, '#29357F'));
    }
    console.log(data);
    return {
      data,
      layout: {
        margin: {
          l: 180,
          r: 180,
          b: 60,
          t: 50,
          pad: 10
        },
        polar: {
          radialaxis: {
            visible: true,
            rangemode: 'normal'
          }
        },
        legend: {
          orientation: 'h'
        }
      }
    };
  }

  createExposureNetwork(quarter: string, fromCompany: CompanyData, toCompany: CompanyData): {
    nodes: Node[],
    links: Edge[],
  }
  {
    const totalExposure = fromCompany.totalRiskExposure[quarter];
    return {
      nodes: [
        {
          id: 'source',
          label: 'Your Company',
          data: {
            nodeType: 'cluster',
            size: 30
          }
        },
        {
          id: 'Hanneman',
          label: '$' + ((Math.round(totalExposure / 10000)) / 100) + 'M',
          data: {
            relation: 'Hanneman Inc',
            nodeType: 'instance',
            class: 'Transaction',
            size: 20
          }
        },
        {
          id: 'Aviato',
          label: '$' + ((Math.round(totalExposure * 0.6 / 10000)) / 100) + 'M',
          data: {
            relation: 'Aviato Pte Ltd',
            nodeType: 'cluster',
            class: 'Company',
            size: 30
          }
        },
        {
          id: 'Raviga',
          label: '$' + ((Math.round(totalExposure * 0.4 / 10000)) / 100) + 'M',
          data: {
            relation: 'Raviga Pte Ltd as Trustee for ' + fromCompany.name,
            nodeType: 'instance',
            class: 'Transaction',
            size: 20
          }
        },
        {
          id: 'Hooli',
          label: '$0',
          data: {
            relation: 'Hooli Inc',
            nodeType: 'instance',
            class: 'Transaction',
            size: 20
          }
        }
      ].map(n => ({
        ...n, ...{
          dimension: {
            width: 100,
            height: 100
          }
        }
      })),
      links: [
        {id: 'source-Hanneman', source: 'source', target: 'Hanneman'},
        {id: 'Aviato-Hanneman', source: 'Aviato', target: 'Hanneman'},
        {id: 'Aviato-Raviga', source: 'Aviato', target: 'Raviga'},
        {id: 'Aviato-Hooli', source: 'Aviato', target: 'Hooli'},
        {id: 'source-Raviga', source: 'source', target: 'Raviga'},
        {id: 'source-Hooli', source: 'source', target: 'Hooli'}
      ]
    };
  }

  createBarsTimeline(): any {

  }

  createCytoNetwork(companyName: string, cRelations: CompanyRelation[]): CytoElement {
    let elements = {
      nodes: [],
      edges: []
    }
    elements.nodes.push(
      {
        data: {id: 'origin', label: companyName, icon: 'company', origin: 'true'},
        classes: ['instance', 'origin'],
        position: {x: 0, y: 0}
      });

    cRelations.forEach((r, i) => {

      if (r.values.length > 1) { // cluster nodes
        elements.nodes.push({
          data: {
            id: r.relation.id, label: r.relation.class, icon: r.relation.class.toLowerCase(),
            collapsible: true,
            collapsed: true,
            children: {
              nodes: r.values.map((v, i) => ({
                data: {id: (r.relation.id + i), label: v.label || v.id, icon: r.relation.class.toLowerCase()},
                classes: 'instance'
              })),
              edges: r.values.map((v, i) => ({
                data: {id: 'to' + (r.relation.id + i), source: r.relation.id, target: (r.relation.id + i)},
                classes: 'instance'
              }))
            }
          },
          classes: ['cluster', 'collapsed']
        });
        /*if (this.showChildFor.indexOf(r.relation.id) < 0) {

        } else {

          r.values.forEach((v, x) => {
            this.nodes.push({
              id: ((i + 1).toString()) + (x + 1).toString(),
              label: v.label || v.id,
              data: {
                relation: r.relation.id,
                class: r.relation.class,
                size: 20,
                nodeType: 'cluster-instance'
              }
            });
            this.links.push({
              id: 'l' + ((i + 1).toString()) + (x + 1).toString(),
              source: (i + 1).toString(),
              target: ((i + 1).toString()) + (x + 1).toString(),
            });
          });

        }*/

      } else { // single nodes
        elements.nodes.push({
          data: {
            id: r.relation.id, label: r.values[0].label || r.values[0].id, icon: r.relation.class.toLowerCase()
          },
          classes: 'instance'
        });
      }
      elements.edges.push({
        data: {id: 'to' + r.relation.id, source: 'origin', target: r.relation.id, label: r.relation.id}
      });
    });

    return elements;
  }

  createCytoExposureNetwork(company: CompanyData): CytoElement {
    let nodes: {
      data: { id: string, label: string, icon: string, origin?: 'true' | 'false' },
      position: { x: number, y: number },
      classes: string[]
    }[] = [];
    let edges:
      {
        data: { id: string, source: string, target: string, label: string }
      }[] = [];

    let records = company.currentExposure.detailsOfFacilities;

    /*let totalExposure = records.map(r => Number(r.Credit_Limit.replace(/[^0-9]/g, ''))).reduce((s, a) => {
      return s + a;
    }, 0)*/
    nodes.push(
      {
        data: {id: '0', label: company.cif, icon: 'company', origin: 'true'},
        position: {x: 0, y: 100},
        classes: ['cluster', 'origin']
      },
      {
        data: {
          id: 'm',
          label: company.currentExposure.totalExposure,
          icon: 'company',
          origin: 'true'
        },
        position: {x: 600, y: 100},
        classes: ['cluster', 'origin']
      }
    )
    records.forEach((r, i) => {
      if (r.Outstanding_Amount && (r.Fiscal_Year === company.currentExposure.latestYear)) {
        nodes.push(
          {
            data: {id: 'exposure' + i, label: r.Cust_ABBR, icon: 'company'},
            position: {x: 300, y: 100 * i},
            classes: ['instance']
          }
        )
        edges.push(
          {
            data: {id: 'c1e' + i, source: 'm', target: 'exposure' + i, label: (r.Currency + this.data.nFormatter(r.Outstanding_Amount, 2))}
          },
          {
            data: {
              id: 'c2e' + i,
              source: '0',
              target: 'exposure' + i,
              label: ''
            }
          },
        )
      }
    })
    return {nodes, edges};
  }

  createCytoExposureNetworkOld(quarter: string, fromCompany: CompanyData): CytoElement {
    const totalExposure = fromCompany.totalRiskExposure[quarter];
    return {
      nodes: [
        {
          data: {id: 'company1', label: fromCompany.name, icon: 'company', origin: 'true'},
          position: {x: 0, y: 100},
          classes: ['cluster', 'origin']
        },
        {
          data: {
            id: 'company2',
            label: '$' + ((Math.round(totalExposure / 10000)) / 100) + 'M',
            icon: 'company',
            origin: 'true'
          },
          position: {x: 600, y: 100},
          classes: ['cluster', 'origin']
        },
        {
          data: {id: 'exposure1', label: 'Disrupt', icon: 'company'},
          position: {x: 300, y: 0},
          classes: ['instance']
        },
        {
          data: {id: 'exposure2', label: 'Hanneman', icon: 'company'},
          position: {x: 300, y: 100},
          classes: ['instance']
        },
        {
          data: {id: 'exposure3', label: 'CitadelOfRix', icon: 'company'},
          position: {x: 300, y: 200},
          classes: ['instance']
        }
      ],
      edges: [
        {
          data: {id: 'c1e1', source: 'company1', target: 'exposure1', label: '$0M'}
        },
        {
          data: {
            id: 'c2e1',
            source: 'company2',
            target: 'exposure1',
            label: '$' + ((Math.round(totalExposure * 0.5 / 10000)) / 100) + 'M'
          }
        },
        {
          data: {
            id: 'c1e2',
            source: 'exposure2',
            target: 'company1',
            label: '$' + ((Math.round(totalExposure * 0.3 / 10000)) / 100) + 'M'
          }
        },
        {
          data: {id: 'c2e2', source: 'exposure2', target: 'company2', label: '$0M'}
        },
        {
          data: {id: 'c1e3', source: 'company1', target: 'exposure3', label: '$0M'}
        },
        {
          data: {
            id: 'c2e3',
            source: 'company2',
            target: 'exposure3',
            label: '$' + ((Math.round(totalExposure * 0.2 / 10000)) / 100) + 'M'
          }
        }
      ]
    };
  }

  createDcraPie(
    data: {
      [key: string]: number,
    },
    colors?: string[]): any {
    return {
      data: [{
        values: Object.values(data),
        labels: Object.keys(data),
        domain: {column: 0},
        hoverinfo: 'label',
        textinfo: 'value',
        hole: .5,
        type: 'pie',
        sort: false
      }],
      layout: {
        legend: {
          orientation: 'v',
          y: 0.5,
          x: 1.2,
          xanchor: 'right',
          yanchor: 'center',
          bgcolor: 'rgba(255,255,255,0.2)'
        },
        margin: {
          l: 20,
          r: 20,
          b: 20,
          t: 20,
          pad: 10
        }
      }
    };
  }

  createSurface(
    data: {
      [key: string]: number
    },
    colors?: string[]): any {
    return {
      data: [{
        z: [[1, 2, 1, 2], [2, 3, 4, 1], [2, 4, 1, 2]],
        hoverinfo: 'label',
        textinfo: 'value',
        type: 'surface'
      }],
      layout: {
        legend: {},
        margin: {
          l: 20,
          r: 20,
          b: 20,
          t: 20,
          pad: 10
        }
      }
    };
  }


  createAmlPie(
    pie: {
      data: {
        [key: string]: number,
      },
      hoverText?: string[],
      colors?: string[]
    }
  ): any {
    return {
      data: [{
        values: Object.values(pie.data),
        labels: Object.keys(pie.data),
        domain: {column: 0},
        textinfo: 'value',
        hole: .5,
        type: 'pie',
        marker: {
          line: {
            color: '#fff',
            width: new Array(Object.keys(pie.data).length).fill(4)
          }
        },
        sort: false,
        hovertext: pie.hoverText,
        hoverinfo: 'label+text'
      }],
      layout: {
        piecolorway: pie.colors,
        legend: {
          orientation: 'v',
          y: 0.5,
          x: 1.2,
          xanchor: 'right',
          yanchor: 'center',
          bgcolor: 'rgba(255,255,255,0.2)'
        },
        font: {
          family: 'Roboto, sans-serif',
          size: 14
        },
        margin: {
          l: 20,
          r: 20,
          b: 20,
          t: 20,
          pad: 10
        }
      }
    };
  }

  createAmlMaps(): any {
    return {
      data: [{
        type: 'scattergeo',
        mode: 'markers',
        locations: ['SGP', 'CHN', 'THA', 'IND', 'NGA'],
        marker: {
          size: [20, 30, 15, 10, 10],
          color: [10, 20, 40, 50, 80],
          cmin: 0,
          cmax: 100,
          colorscale: 'YlOrRd',
          reversescale: true,
          colorbar: {
            title: 'Risk',
            ticksuffix: '%',
            showticksuffix: 'last'
          },
          line: {
            color: 'black'
          }
        },
        name: 'geo data'
      }],
      layout: {
        /*'geo': {
          'scope': 'world',
          'resolution': 50
        },*/
        geo: {
          projection: {
            type: 'orthographic',
            rotation: {
              lon: 103,
              lat: 40
            }
          },
          showocean: true,
          oceancolor: '#29357F',
          showland: true,
          landcolor: '#27ABE2',
          showcountries: true,
          countrywidth: 1,
          countrycolor: '#29357F',
          center: {}
        },
        mapbox: {
          style: 'dark'
        },
        font: {
          family: 'Roboto, sans-serif',
          size: 14
        },
        margin: {
          l: 5,
          r: 5,
          b: 5,
          t: 5,
          pad: 0
        }
      }
    };
  }

  createAmlTimeline(data: { x: Date[], y: number[], text?: string[] }[]): any {
    console.log(data);
    return {
      data: data.map((d, i) => {
        return {
          type: 'bar',
          x: d.x,
          y: d.y,
          text: d.text,
          hovertemplate: '%{text}%{y}',
          width: 1000 * 3600 * 24,
          opacity: 0.7,
          name: (i === 2 ? 'high' : i === 1 ? 'medium' : 'low') + ' risk',
          marker: {
            color: (i === 2 ? '#ED553B' : i === 1 ? '#ffc107' : '#29357F') // '#29357F'
          }
        }
      }),
      layout: {
        margin: {
          l: 25,
          r: 25,
          b: 0,
          t: 10,
          pad: 0
        },
        legend: {
          orientation: 'h',
          y: -0.4
        },
        xaxis: {
          ticklen: 10,
          type: 'date',
          tickangle: 0,
          tickformat: '%b-%y'
        },
        yaxis: {
          showticklabels: false,
          title: null,
          ticklen: 10,
          tickcolor: '#fff',
          tickmode: 'array', hoverformat: '.2f'
        },
        width: 400,
        height: 200
      }
    };
  }

  createPlaceholderAmlTimeline(): any {
    return {
      data: [
        {
          type: 'bar',
          x: new Array(60).fill(0).map(i => Math.round(Math.random() * 168) / 7),
          y: new Array(60).fill(0).map(i => Math.round(Math.random() * 40)),
          width: 0.05,
          name: 'safe',
          marker: {
            color: '#29357F'
          }
        },
        {
          type: 'bar',
          x: new Array(20).fill(0).map(i => Math.round(Math.random() * 168) / 7),
          y: new Array(20).fill(0).map(i => Math.round(40 + Math.random() * 40)),
          width: 0.05,
          name: 'cleared',
          marker: {
            color: '#27ABE2'
          }
        },
        {
          type: 'bar',
          x: new Array(10).fill(0).map(i => Math.round(Math.random() * 168) / 7),
          y: new Array(10).fill(0).map(i => Math.round(80 + Math.random() * 20)),
          width: 0.05,
          name: 'blocked',
          marker: {
            color: '#ED553B'
          }
        }
      ],
      layout: {
        margin: {
          l: 25,
          r: 25,
          b: 0,
          t: 10,
          pad: 0
        },
        legend: {
          orientation: 'h',
          y: -0.5
        },
        xaxis: {
          ticklen: 10,
          tickcolor: '#fff',
          type: 'linear',
          range: [7, 0],
          dtick: 1,
          ticksuffix: 'd'
        },
        yaxis: {
          showticklabels: false,
          title: null,
          range: [0, 100],
          ticklen: 10,
          tickcolor: '#fff',
          tickmode: 'array'
        },
        width: 400,
        height: 200
      }
    };
  }

  createBarChart(data: { [key: string]: number }): any {
    return {
      data: [
        {
          x: Object.keys(data),
          y: Object.values(data),
          type: 'bar',
          text: Object.values(data),
          textposition: 'auto',
          hoverinfo: 'none',
          marker: {
            color: '#27ABE2'
          }
        }
      ],
      layout: {
        margin: {
          l: 50,
          r: 50,
          b: 50,
          t: 20,
          pad: 0
        },
        xaxis: {
          ticklen: 10,
          tickcolor: '#fff',
          type: 'category'
        },
        yaxis: {
          showticklabels: false
        },
        width: 600,
        height: 300
      }
    };
  }

  createHistogram(
    normal: number[],
    xaxis: {
      ticksuffix: string,
      dtick?: number
    },
    xbins: {
      end: number,
      size: number,
      start: number
    },
    selected?: number[]
  ): any {
    let data = [
      {
        x: normal,
        name: 'unselected',
        type: 'histogram',
        hovertemplate: '%{x} : <b>%{y}</b>',
        xbins,
        marker: {
          color: '#27ABE2'
        }
      }
    ];
    if (selected) data.push({
      x: selected,
      type: 'histogram',
      name: 'selected',
      hovertemplate: '%{x} : <b>%{y}</b>',
      xbins,
      marker: {
        color: '#29357F'
      }
    });
    return {
      data,
      layout: {
        barmode: 'stack',
        margin: {
          l: 50,
          r: 50,
          b: 50,
          t: 20,
          pad: 0
        },
        bargap: 0.1,
        bargroupgap: 0.1,
        yaxis: {
          showticklabels: false
        },
        xaxis,
        width: 600,
        height: 300
      }
    };
  }

  createAmlCompanyPie(
    values: string[],
    selected?: string[]
  ) {

    let obj = {};
    values.forEach(x => {
      if (!obj[x]) obj[x] = 1
      else obj[x] += 1
    })
    let keys = Object.keys(obj);
    keys.sort((a, b) => obj[a] - obj[b]);
    return {
      data: [{
        labels: values,
        hovertext: values,
        domain: {column: 0},
        text: values.map(v => {
          let arr = v.split(' ');
          if (arr) return arr[0]
        }),
        textinfo: 'text',
        hole: .5,
        type: 'pie',
        marker: {
          line: {
            color: '#fff',
            width: 4
          },
          colors: values.map(k => (selected.indexOf(k) > -1) ? '#29357F' : 'rgba(39, 171, 226, ' + ((keys.indexOf(k) + 1) / keys.length) + ')')
        },
        sort: false,
        hoverinfo: 'text+value',
        hoverlabel: {
          bgcolor: '#343a40',
        },
        insidetextorientation: 'horizontal'
      }],
      layout: {
        showlegend: false,
        font: {
          family: 'Roboto, sans-serif',
          size: 10
        },
        margin: {
          l: 30,
          r: 30,
          b: 30,
          t: 30,
          pad: 10
        }
      }
    };

  }

  createPie(
    data: {
      [key: string]: number,
    }
  ): any {
    return {
      data: [{
        values: Object.values(data),
        labels: Object.keys(data),
        domain: {column: 0},
        textinfo: 'value',
        hole: .5,
        type: 'pie',
        marker: {
          line: {
            color: '#fff',
            width: new Array(Object.keys(data).length).fill(4)
          }
        },
        sort: false,
        hoverinfo: 'label+text',
        colorscale: 'RdBu'
      }],
      layout: {
        legend: {
          orientation: 'v',
          y: 0.5,
          x: 1.2,
          xanchor: 'right',
          yanchor: 'center',
          bgcolor: 'rgba(255,255,255,0.2)'
        },
        font: {
          family: 'Roboto, sans-serif',
          size: 14
        },
        margin: {
          l: 20,
          r: 20,
          b: 20,
          t: 20,
          pad: 10
        }
      }
    };
  }

  createDailyTimeHistogram(data: number[]): any {

    return {
      data: [
        {
          x: data,
          type: 'histogram',
          text: data,
          xbins: {
            end: 2400,
            size: 300,
            start: 0
          },
          textposition: 'auto',
          hoverinfo: 'none',
          marker: {
            color: '#27ABE2'
          }
        }
      ],
      layout: {
        margin: {
          l: 50,
          r: 50,
          b: 50,
          t: 20,
          pad: 0
        },
        bargap: 0.05,
        bargroupgap: 0.2,
        yaxis: {
          showticklabels: false
        },
        xaxis: {
          ticksuffix: 'hrs',
          dtick: 600
        },
        width: 600,
        height: 300
      }
    };
  }

  createScatterPlot(
    normal: { x: number[], y: number[], c: string[] },
    titles: { x: string, y: string },
    suffix: { x: string, y: string },
    // outliers?: { x: number[], y: number[] }
  ) {
    let data = [
      {
        x: normal.x,
        y: normal.y,
        mode: 'markers',
        type: 'scatter',
        name: '',
        // text: ['A-1', 'A-2', 'A-3', 'A-4', 'A-5'],
        // textposition: 'top center',
        marker: {size: 12, color: normal.c},
        hovertemplate: titles.x + ': %{x}, ' + titles.y + ': %{y}',
        hoverlabel: {
          bgcolor: '#343a40',
        }
      }
    ];
    /*if (outliers) {
      data.push(
        {
          x: outliers.x,
          y: outliers.y,
          mode: 'markers',
          type: 'scatter',
          name: '',
          // text: ['A-1', 'A-2', 'A-3', 'A-4', 'A-5'],
          // textposition: 'top center',
          marker: {size: 20, color: '#29357F'},
          hovertemplate: titles.x + ': %{x}, ' + titles.y + ': %{y}',
          hoverlabel: {
            bgcolor: '#343a40',
          }
        }
      )
    }*/
    return {
      data,
      layout: {
        margin: {
          l: 50,
          r: 50,
          b: 60,
          t: 20,
          pad: 0
        },
        xaxis: {
          // range: [0, 2400],
          // ticksuffix: suffix.x,
          zeroline: false,
          title: {
            text: titles.x
          },
          linecolor: '#999',
          linewidth: 1,
          mirror: true
        },
        yaxis: {
          // ticksuffix: suffix.y,
          zeroline: false,
          title: {
            text: titles.y
          },
          ticklabelposition: 'inside top',
          linecolor: '#999',
          linewidth: 1,
          mirror: true
        },
        width: 1200,
        height: 600
      }
    }
  }
}
