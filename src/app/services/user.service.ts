import {Injectable} from '@angular/core';
import {DcraUser} from '../classes';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree, CanActivate} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService implements CanActivate {

  users: DcraUser[] = [  ];
  activeUser: DcraUser;
  targetRoute: string;

  constructor(
    private router: Router
  ) {
  }

  logInAs(username: string): void {
    this.activeUser = this.users.filter(u => u.email === username)[0];
    if (this.activeUser) {
      localStorage.setItem('user', JSON.stringify(this.activeUser));
      this.router.navigate([this.targetRoute || '']).then(r => {
      });
    } else {
      alert('user not found');
    }
  }
  loginFromServer(user: DcraUser): void {
    this.activeUser = user;
    localStorage.setItem('user', JSON.stringify(this.activeUser));
    this.router.navigate([this.targetRoute || '']).then(r => {
    });
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // this.activeRole = JSON.parse(localStorage.getItem('user'));
    const targetRoute = state.url;
    this.activeUser = this.getActiveUser();
    if (targetRoute !== 'unauthorised') {
      if (this.activeUser) { // currently logged in
        return true;
      } else {
        // check that the intended destination is not login page itself
        this.targetRoute = state.url;
        this.router.navigate(['login']).then(r => {
        });
      }
    } else {
      return true;
    }
  }

  getActiveUser(): DcraUser {
    return JSON.parse(localStorage.getItem('user'));
  }

  logOut(): void {
    localStorage.removeItem('user');
    this.activeUser = null;
    this.router.navigate(['login']).then(r => {
    });
  }
}
