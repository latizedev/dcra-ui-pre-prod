import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, timer, Subscription, Subject} from 'rxjs';
import {ApiGradeDistribution, QueryStatus} from '../classes';
import {retry, share, switchMap, takeUntil} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // url1 = 'https://dcra-server-yub-53.loca.lt';
  // url2 = 'https://dcra-reportgen-yub-54.loca.lt'; // 220.24.52.14
  url1 = 'http://20.212.82.171:10001';
  url2 = 'http://20.212.82.171:8500';
  /*url1 = 'http://220.24.52.14:10001';
  url2 = 'http://220.24.52.14:8000';*/
  token;

  constructor(private http: HttpClient) { }

  gradingNumberToString(num: number): '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS' {
      return num === 1 ? '1.PASS' : (num === 2 ? '2.SM' : (num === 3 ? '3.SS' : (num === 4 ? '4.DF' : (num === 5 ? '5.LOSS' : null))));
  }

  getResultFromQueryID(id: string, then: (queryResult) => any, offset= 0, limit = 500) {

    const timer = setInterval(() => {
      this.getQueryStatus(id).subscribe((qs: QueryStatus) => {
        // console.log('query status', qs);
        if(qs.jobState === 'COMPLETED') {
          clearInterval(timer);
          this.getQueryResult(id, offset, limit).subscribe((r: ApiGradeDistribution) => {
            // console.log('query result', r);
            then(r);
          });
        }
      }, qErr => {
        // console.log('query error', qErr);
      });
    }, 1000)
  }


  getAllResultsFromQueryID(id: string, then: (queryResult) => any, offset= 0, limit = 500) {

    const timer = setInterval(() => {
      this.getQueryStatus(id).subscribe((qs: QueryStatus) => {
        // console.log('query status', qs);
        if(qs.jobState === 'COMPLETED') {
          clearInterval(timer);
          let setsReceived = 0;
          let result: { rows: any[] } = { rows: [] };
          let count = Math.ceil(qs.rowCount / 500);

          for (let i = 0; i < count; i++) {

            this.getQueryResult(id, i*500, 500).subscribe((resultBlock: { rows: any[] }) => {
              // console.log('query result', r);x
              setsReceived += 1;
              result.rows.push(...resultBlock.rows);
              if(setsReceived === count) {
                then(result);
              }
            });
          }
        }
      }, qErr => {
        // console.log('query error', qErr);
      });
    }, 1000)
  }

  login(username: string, password: string): any {
    /*
    "username": "guest1@zflexsoftware.com","password": "guest1password"
    returns {
      jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Imd1ZXN0MUB6ZmxleHNvZnR3YXJlLmNvbSIsInN0ZXBzIjpbMCwxLDJdLCJpYXQiOjE2MjQ0Mjk2NTIsImV4cCI6MTYyNDQzMzI1Mn0.7C5zs1aKN9gyfHHxToYrtDtcf7723og6NT6Ya-jb6Jg"
      user: {email: "guest1@zflexsoftware.com", displayName: "Guest1 NumberOne", givenName: "Guest"}
    }
    */
    if(username === 'PeteRai') {
      username = 'analyst1@mizuho.com';
      password = '1234';
    }
    return this.http.post(this.url1 + '/login', {
      username, password
    })
  }

  getQueryStatus(id: string) {
    return this.http.get(this.url1 + '/api/query/' + id, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
  getQueryResult(id: string, offset: number, limit: number) {
    return this.http.get(this.url1 + '/api/query/' + id + '/results?offset=' + offset + '&limit=' + limit, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }

  getGradesDistribution(type: 'calculated' | 'finalized') {
    return this.http.get(this.url1 + '/api/grades/distribution?type=' + type, {
      headers: {
        Authorization: 'Bearer ' +  this.token
      }
    })
  }

  getAllOverrides(offset: number, limit: number) {
    //http://{{host}}:10001/api/grades/allOverrides?offset=0&limit=10
    return this.http.get(this.url1 + '/api/grades/allOverrides?offset=' + offset + '&limit=' + limit, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }

  getAllOverridesForCIF(cif: string) {
    //http://{{host}}:10001/api/grades/allOverrides?offset=0&limit=10
    return this.http.get(this.url1 + '/api/grades/allOverrides?cifNo=' + cif + '&limit=500', {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }

  getMyOverrides() {
    //http://{{host}}:10001/api/grades/allOverrides?offset=0&limit=10
    return this.http.get(this.url1 + '/api/grades/myOverrides', {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }

  getMyPending(step: number) {
    //http://{{host}}:10001/api/grades/allOverrides?offset=0&limit=10
    return this.http.get(this.url1 + '/api/grades/overridesByStatus/Awaiting?currentStep=' + step +'&offset=0&limit=20', {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }

  getOverrideStatus(id: number) {
    return this.http.get(this.url1 + '/api/grades/overrides/' + id, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });

  }
  getAllCalculatedGrades() {
    return this.http.get(this.url1 + '/api/grades/calculated', {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
  getAllCalculatedGradesYearMonth(year: number, month: number) {
    return this.http.get(this.url1 + '/api/grades/calculated?Fiscal_Year=' + year + '&window=' + month, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }

  getGradesForCif(cif) {
    return this.http.get(this.url1 + '/api/grades/calculated?CIF_NO=' + cif, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }

  getGradesForYearMonth(year: number, month: number) {
    return this.http.get(this.url1 + '/api/grades/calculated?Fiscal_Year=' + year + '&window=' + month, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
  getFinalizedGrades() {
    return this.http.get(this.url1 + '/api/grades/finalized', {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
  getFinalizedGradeForCIF(cif: string) {
    return this.http.get(this.url1 + '/api/grades/finalized?CIF_NO=' + cif, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }

  gradeOverride(cif: number, year: number, month: number, calculated: number, assigned: number, comment: string) {
    // http://{{host}}:10001/api/grades/override
    let body = {
      "jsonrpc": "2.0",
      "id": null,
      "method": null,
      "params": {
        "CIF_NO": cif,
        "Fiscal_Year": year,
        "Fiscal_Month": month,
        "Calculated_MAS_Grading": calculated,
        "Finalized_MAS_Grading": assigned,
        "justification": comment
      }
    }

    return this.http.post(this.url1 + '/api/grades/override', body, {
      headers: {
        Authorization: 'Bearer ' + this.token
      },
    });
  }

  gradeOverrideApprove(id: number, step: number, comment: string, assigned?: number) {
    // http://{{host}}:10001/api/grades/override
    let body: any = {
      "jsonrpc": "2.0",
      "id":null,
      "method": null,
      "params": {
        "id": id,
        "currentStep": step,
        "justification": comment
      }
    }

    if (assigned) {
      body.params.Finalized_MAS_Grading = assigned
    }

    return this.http.post(this.url1 + '/api/grades/approve', body, {
      headers: {
        Authorization: 'Bearer ' + this.token
      },
    });
  }

  gradeOverrideReject(id: number, step: number, comment: string) {
    // http://{{host}}:10001/api/grades/override
    let body = {
      "jsonrpc": "2.0",
      "id":null,
      "method": null,
      "params": {
        "id": id,
        "currentStep": step,
        "justification": comment
      }
    }

    return this.http.post(this.url1 + '/api/grades/reject', body, {
      headers: {
        Authorization: 'Bearer ' + this.token
      },
    });
  }
  gradeOverrideCancel(id: number, comment: string) {
    // http://{{host}}:10001/api/grades/override
    let body = {"jsonrpc": "2.0",
      "id":null,
      "method": null,
      "params": {
        "id": id,
        "justification": comment
      }
    }

    return this.http.post(this.url1 + '/api/grades/cancel', body, {
      headers: {
        Authorization: 'Bearer ' + this.token
      },
    });
  }
  getGradingReasons(cif: number, year: number, month: number){
    let body = {
      "CIF_NO": cif,
      "Fiscal_Year": year,
      "Fiscal_Month": month
    }
    return this.http.post(this.url2 + '/ReasonsApi', body, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    })
  }
  getGradingNarrative(cif: number, year: number, month: number){
    let body = {
      "CIF_NO": cif,
      "Fiscal_Year": year,
      "Fiscal_Month": month
    }
    return this.http.post(this.url2 + '/NarrativeApi', body, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    })
  }

  // company info apis
  getAllCompanyData(type: string) {
    // GET http://{{host}}:10001/api/backgrounds/latest?CIF_NO=1000
    // type must be 'backgrounds' | 'exposures' | 'financials' | 'news'
    // console.log('GET ' + this.url1 + '/api/' + type + '/latest?CIF_NO=' + cif)
    return this.http.get(this.url1 + '/api/' + type + '/latest', {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
  getCompanyData(type: 'financials' | 'backgrounds' | 'exposures' | 'news' | 'nlgData', cif?: string) {
    // GET http://{{host}}:10001/api/backgrounds/latest?CIF_NO=1000
    // type must be 'backgrounds' | 'exposures' | 'financials' | 'news'
    // console.log('GET ' + this.url1 + '/api/' + type + '/latest?CIF_NO=' + cif)
    return this.http.get(this.url1 + '/api/' + type + '/latest?CIF_NO=' + cif, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
  // http://{{host}}:10001/api/macroEconomics/latest?minDate=2020-03-01&maxDate=2021-07-30
  getBenchmarkTickersForDateRange(startYearMonth, finalYearMonth){
    return this.http.get(this.url1 + '/api/macroEconomics/latest?minDate=' + startYearMonth + '-01&maxDate=' + finalYearMonth + '-30', {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
  getBenchmarkTickersForDate(date: string){
    // date 2021-09-30
    return this.http.get(this.url1 + '/api/macroEconomics/latest?d=' + date, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
  getAllBenchmarkTickers(){
    // date 2021-09-30
    return this.http.get(this.url1 + '/api/macroEconomics/latest', {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
  getBenchmarkTickerList(){
    // date 2021-09-30
    return this.http.get(this.url1 + '/api/macroEconomics/list/Ticker', {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
  getBenchmarkForTicker(ticker: string){
    return this.http.get(this.url1 + '/api/macroEconomics/latest?Ticker=' + ticker, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
  getBenchmarkForTickerDateRange(ticker: string, max: string, min: string){
    return this.http.get(this.url1 + '/api/macroEconomics/latest?Ticker=' + ticker + '&minDate=' + min +' &maxDate=' + max, {
      headers: {
        Authorization: 'Bearer ' + this.token
      }
    });
  }
}
