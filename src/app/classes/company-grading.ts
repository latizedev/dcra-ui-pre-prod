export class CompanyGradingObsolete {
  value: '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS'; // Pass 2. SM 3. SS 4. DF 5. Loss
  rejected: boolean;
  constructor(value: '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS') {
    this.value = value;
    this.rejected = false;
  }
}
