export interface CompanyNewsApi {
  News_Title: string;
  Source_Publication_Name: string;
  Article_Published_Date: string;
  Excerpt: string;
  URL: string
}

export class CompanyNews {
  date: string;
  source: string;
  sourceUrl: string;
  title: string;
  preview: string;
  tags: string[];
  flags: string[];

  // News_Title	Source_Publication_Name	Article_Published_Date	Excerpt	URL
  constructor(
    apiOutput: CompanyNewsApi
  ) {
    this.date = apiOutput.Article_Published_Date;
    this.source = apiOutput.Source_Publication_Name;
    this.sourceUrl = apiOutput.URL;
    this.preview = apiOutput.Excerpt;
    this.title = apiOutput.News_Title;
    this.flags = [];
    this.tags = [];

  }
}
