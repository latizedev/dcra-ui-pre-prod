export class Trend {
  name: string;
  unit?: string;
  steps: {
    [key: string]: any
  };
}
