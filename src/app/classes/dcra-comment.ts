export class DcraComment {
  ratingFrom: string;
  ratingTo?: string;
  userName: string;
  text: string;
  date: Date;
}
