export class GradingBreakdown {
  name?: string;
  label?: string;
  movement?: number;
  text: string;
  trend?: {
    [key: string]: number,
  };
  negativeEffect?: boolean;
  chart?: {
    data: any,
    layout: any
  };
}

/*
PL_REVENUES	TOTAL REVENUE
PL_REVENUES_GROWTH	TOTAL REVENUE GROWTH
PL_COST_OF_SALES	cost of sales
PL_GROS_PROFIT	gross profit
PL_SG_AND_A_EXPNS	SG&A expenses
PL_RENT	rental expense
PL_GROS_PROFIT_OTHER	other operating income / expense
PL_EBIT	earnings before interest and tax / operating income
PL_INT_INCM	interest income
PL_INT_EXPNS	interest expense
PL_NON_OPE_INCM_EXPNS	non-operating income / expense
PL_EBIT_OTHER	FX gain / loss
PL_EBT	Pretax income
PL_TAXES	Taxes
PL_MINORITY_INT	Minority Interest
PL_EXTRDNRY_GAIN_OR_LOSS	Extraordinary gain / loss
PL_EBT_OTHER	any other gain / loss
PL_NET_INCM	net income
PL_EBITDA	EBITDA
PL_DEPRE_AMORTZATION	depreciation / amortization
PL_EXP_REVENUES
PL_EXP_REVENUES_GROWTH
PL_EXP_COST_OF_SALES
PL_EXP_GROS_PROFIT
PL_EXP_SG_AND_A_EXPNS
PL_EXP_RENT
PL_EXP_GROS_PROFIT_OTHER
PL_EXP_EBIT
PL_EXP_INT_INCM
PL_EXP_INT_EXPNS
PL_EXP_NON_OPE_INCM_EXPNS
PL_EXP_EBIT_OTHER
PL_EXP_EBT
PL_EXP_TAXES
PL_EXP_MINORITY_INT
PL_EXP_EXTRDNRY_GAIN_OR_LOSS
PL_EXP_EBT_OTHER
PL_EXP_NET_INCM
PL_EXP_EBITDA
PL_EXP_DEPRE_AMORTZATION
PL_MRG_REVENUES
PL_MRG_REVENUES_GROWTH
PL_MRG_COST_OF_SALES
PL_MRG_GROS_PROFIT
PL_MRG_SG_AND_A_EXPNS
PL_MRG_RENT
PL_MRG_GROS_PROFIT_OTHER
PL_MRG_EBIT
PL_MRG_INT_INCM
PL_MRG_INT_EXPNS
PL_MRG_NON_OPE_INCM_EXPNS
PL_MRG_EBIT_OTHER
PL_MRG_EBT
PL_MRG_TAXES
PL_MRG_MINORITY_INT
PL_MRG_EXTRDNRY_GAIN_OR_LOSS
PL_MRG_EBT_OTHER
PL_MRG_NET_INCM
PL_MRG_EBITDA
PL_MRG_DEPRE_AMORTZATION
CF_NET_INCM	net income
CF_DEPRE_AMORTZATION	depreciation / amortization
CF_TAXES	Taxes(Deferred etc)
CF_GAIN_FROM_PP_AND_E	Gain(Loss)from PP&E Sales(-/+)
CF_NET_WORKING_CAPITAL	Net Working Capital
CF_OTHER_OPE_CASHFLOW	Other Operating Cashflow
CF_CASH_FROM_OPE_ACVT	operating cashflow
CF_CAPEX	capex
CF_INVST	investments
CF_ACQUISITIONS	acquisitions
CF_PROCEEDS_FROM_PP_AND_E	proceeds from sale of PPE
CF_OTHER_INVST_CASHFLOW	other investment cashflow
CF_CASH_FROM_INVST_ACVT	investing cashflow
CF_SALE_OF_STOCK	sale of stock
CF_PURCHASE_OF_STOCK	purchase of stock
CF_CASH_DIVIDNDS	cash dividends
CF_DEBT_BORROWING	debt borrowings
CF_DEBT_REPAYMENT	debt repayment
CF_OTHER_FINCNG_CASHFLOW	other financing cashflow
CF_CASH_FROM_FINCNG_ACVT	financing cashflow
CF_OTHER_FLUCTUATION_OF_CASH	other fluctuation of cash
CF_NET_CASHFLOW	net cashflow
CF_CASH_AT_BEGIN_OF_FISCAL_Y	cash at beginning of year
CF_CASH_AT_END_OF_FISCAL_Y	cash at end of year
CASH_AND_CASH_EQUIVALENTS	cash and cash equivalents
ACCOUNTS_RECEIVABLES	accounts receivables
INVENTORIES	inventories
PREPAID_EXPNS	prepaid expenses
OTHER_CURR_AST	other current assets
CURR_AST	Total current assets
NET_PLANT_PRPTY_AND_EQPMNT	Net PPE
GROS_PLANT_PRPTY_AND_EQPMNT	Gross PPE
ACCUMLATED_DEPRE	Accumulated depreciation
OTHER_TANGIBLE_AST	Other tangible assets
TANGIBLE_AST	Total tangible assets
GOODWILL	Goodwill
OTHER_INTANGIBLE_AST	Other intangible assets
INTANGIBLE_AST	Total intangible assets
INVST	investments
DEFFERED_CHARGES	deferred charges
OTHER_AST	other non current assets
NON_CURR_AST_TOTAL	Total non-current assets
TOTAL_AST	Total Assets
SHORT_TERM_DEBT	short term debt
LONG_TERM_DEBT_DUE_IN_ONE_Y	long term debt due in one year
NOTE_PAYABLE	note payable
ACCOUNTS_PAYABLE	accounts payables
ACCURED_EXPNS	accrued expenses
TAX_PAYABLE	tax payable
OTHER_CURR_LIAB	other current liabilities
CURR_LIAB	Total current liaibilities
LONG_TERM_DEBT	Total long term debt
LONG_TERM_BORROWING	long term borrowings
BOND	bonds
SUBORDINATE_DEBT	subordinated debt
DEFFERED_TAXES	deferred taxes
OTHER_LONG_TERM_LIAB	other long term liabilities
MINORITY_INT	Minority Interest
LONG_TERM_LIAB	Total non-current liabilities
LIAB_TOTAL	Total Liabilities
COMMON_STOCK	share capital
ADDITIONAL_PAID_IN_CAPITAL	additional capial injection
OTHER_RSRV	other reserves
RETAINED_EARNINGS	retained earnings
OTHERS	other equity components
SHAREHOLDERS_EQUITY	Total shareholders' equity
TOTAL_LIAB_AND_EQUITY	Sum of total liabilities and total shareholders' equity
OFFBALANCE_LIAB	off balance sheet liabilities
GROS_DEBT	Sum of total long term debt, short term debt, long term debt due in one year and note payable
NON_PERFORMING_AST	non-performing assets
RSRV_FOR_NON_PERFORMING_AST	reserve for non-performing assets
UNRALZD_HOLDING_GAIN_LOSSES	provision for unrealised loss
REAL_NET_WORTH	shareholders' equity in substance (i.e. shareholders' equity less provision for unrealised loss)
ACCOUNTING_STANDARD	accounting standard

 */
