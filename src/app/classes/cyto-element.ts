export class CytoElement {
  nodes: CytoNode[];
  edges: CytoEdge[]
}
export class CytoNode {
  data: { id: string, parent?: string, label?: string, [key: string]: any };
  position?: { x: number, y: number };
  renderedPosition?: { x: number, y: number };
  selected?: boolean; // whether the element is selected (default false)
  selectable?: boolean; // whether the selection state is mutable (default true)
  locked?: boolean; // when locked a node's position is immutable (default false)
  grabbable?: boolean; // whether the node can be grabbed and moved by the user
  pannable?: boolean; // whether dragging the node causes panning instead of grabbing
  classes?: any; // an array (or a space separated string) of class names that the element has
  // DO NOT USE THE `style` FIELD UNLESS ABSOLUTELY NECESSARY
  // USE THE STYLESHEET INSTEAD
  style?: any // style property overrides
}
export class CytoEdge {
  data: { id: string, source: string, target: string, label?: string };
  pannable?: boolean; // whether dragging the edge causes panning instead of grabbing
}
