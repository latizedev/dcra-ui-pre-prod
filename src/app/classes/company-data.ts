import {GradingBreakdown} from './grading-breakdown';
import {DcraComment} from './dcra-comment';
import {GradingCategory} from './grading-category';
import {CompanyExposure} from './company-exposure';
import {CompanyNews} from './company-news';
import {CompanyRelation} from './company-relation';
import {Trend} from './trend';
import {ApiGrade, CompanyRow} from './api-responses';

export class CompanyData {
  cif: string;
  fiscalYear: number;
  fiscalMonth: number;
  flagged: boolean;
  comments: DcraComment[];
  name: string;
  gradings: {
    previous: '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS',
    recommended: '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS',
    review: '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS',
    final: '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS'
  };
  gradingReview: GradingCategory[];
  currentExposure: CompanyExposure;
  news: CompanyNews[];
  relations: CompanyRelation[];
  totalRiskExposure: {
    [key: string]: number
  };
  gradingTrend: Trend;
  ratingTrend: Trend;
  financialTrend: Trend[];
  industryTrend: Trend[];
  latestFinancials: {
    name: string,
    value: number,
  }[];
  financialStanding: any; /*{
    'Cash from Investing<br>Activities ($&#39;000)': number,
    'Total Revenue ($&#39;000)': number,
    'Total Liabilities ($&#39;000)': number,
    'Net Cashflow ($&#39;000)': number,
    'Operating Margin': number,
    'Total Asset ($&#39;000)': number,
    'Revenue / Total Asset': number,
    'Revenue Growth (YoY)': number,
    'Current Assets /<br>Current Liabilities': number,
    'Cash from Financing<br>Activities ($&#39;000)': number,
  };*/

  constructor(cif: any, year?: number, month?: number, name?: string) {
    this.cif = cif ? ((cif / 10000000).toFixed(7).toString().split('.')[1]) : '0000000';
    this.name = name || cif;
    this.fiscalYear = year;
    this.fiscalMonth = month;
    this.flagged = false;
    this.comments = [];
    this.gradings = {
      previous: null,
      recommended: null,
      review: null,
      final: null
    };
    this.gradingReview = [];
    this.currentExposure = null;
    this.relations = [];
    this.totalRiskExposure = null;
    this.gradingTrend = null;
    this.ratingTrend = null;
    this.financialTrend = null;
    this.industryTrend = null;
    this.latestFinancials = [];
    this.financialStanding = {
    };
  }
  assignGrading?(
    key: 'previous' | 'recommended' | 'review' | 'final',
    value: '1.PASS' | '2.SM' | '3.SS' | '4.DF' | '5.LOSS') {
    this.gradings[key] = value;
  }
  updateFromGradingRecords?(records: CompanyRow[]) {

  }
}
