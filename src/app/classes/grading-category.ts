import {GradingBreakdown} from './grading-breakdown';
import {DcraComment} from './dcra-comment';

export class GradingCategory {
  name: string; // revenue, ebitda
  type: string; // profitability, cash-flow ratio etc
  change: number;
  scoresTrend: {key: string, value: number}[];
  breakdown: GradingBreakdown[];
  chart?: {
    data: any,
    layout: any
  };
  comments: DcraComment[];
  isCollapsed?: boolean;
  benchmark?: boolean;
  percentage?: boolean;
  originAdjusted?: boolean;
}
