export class ApiOverrideResponse {
  result: {
    total: number,
    data: ApiOverrideData[]
  };
}
export class ApiOverrideData {
  auditLog: ApiOverrideLog[];
  calculatedMasGrading: number;
  cifNo: any;
  createdAt: string; // "2021-06-22T10:32:36.292207+00:00"
  createdBy: string; // "guest3@zflexsoftware.com"
  finalized: ApiGrade[];
  finalizedMasGrading: number;
  fiscalMonth: number;
  fiscalYear: number;
  id: number;
  justification: string;
  override: {
    calculatedMasGrading: number;
    cifNo: number;
    createdAt: string;
    createdBy: string;
    finalizedMasGrading: number;
    fiscalMonth: number;
    fiscalYear: number;
    id: number;
    justification: string
  }
}
export class ApiOverrideLog {
  createdAt: string; // "2021-06-22T10:10:21.04413+00:00"
  currentStep: number;
  finalizedMasGrading: number;
  id: number;
  justification: string; // "seems ok with me"
  modifiedBy: string; // "guest1@zflexsoftware.com"
  status: string; // "Approved"
  updatedAt: string; // "2021-06-22T10:10:35.579+00:00
}
export class ApiGrade {
  calculatedMasGrading: number;
  cifNo: number;
  finalizedMasGrading: number;
  fiscalMonth: number;
  fiscalYear: number;
  id: number;
}
export class ApiGradeDistribution {
  rowCount: number;
  rows: {Calculated_MAS_Grading: number, nCount: number}[]
}
export class QueryStatus {
  cancellationReason: ""
  endedAt: string; // "2021-06-24T07:51:51.267Z"
  errorMessage: string;
  jobState: string; // "COMPLETED"
  queryType: "REST"
  queueId: string; // "SMALL"
  queueName: string; // "SMALL"
  resourceSchedulingEndedAt: string; // "2021-06-24T07:51:51.236Z"
  resourceSchedulingStartedAt: string; // "2021-06-24T07:51:51.229Z"
  rowCount: number; // 5
  startedAt: string; // "2021-06-24T07:51:51.148Z"
}
export class CompanyRow {
  "Calculated_MAS_Grading": number; //number; //4,
  Finalized_MAS_Grading: number;
  "riskFlag": number; //0,
  "upgradeFlag": number; //0,
  "downgradePoint": number; //4,
  "upgradePoint": number; //0,
  "d1": number; //1,
  "d2": number; //0,
  "d3": number; //1,
  "d4": number; //0,
  "d5": number; //0,
  "d6": number; //0,
  "d7": number; //1,
  "d8": number; //0,
  "d9": number; //0,
  "d10": number; //1,
  "d11": number; //0,
  "u1": number; //0,
  "u2": number; //0,
  "u3": number; //0,
  "u4": number; //0,
  "u5": number; //0,
  "u6": number; //0,
  "u7": number; //0,
  "u8": number; //0,
  "u9": number; //0,
  "u10": number; //0,
  "u11": number; //0,
  "u12": number; //0,
  "u13": number; //0,
  "u14": number; //0,
  "u15": number; //0,
  "u16": number; //0,
  "u17": number; //0,
  "u18": number; //0,
  "u19": number; //0,
  "u20": number; //0,
  "S": number; //0,
  "perc_chg_PL_REVENUES": number; //-36.86106168581114,
  "perc_chg_PL_COST_OF_SALES": number; //-29.28964452507998,
  "perc_chg_PL_GROS_PROFIT": number; //-43.619242144912924,
  "perc_chg_PL_EBIT": number; //-198.73311023324862,
  "perc_chg_PL_EBT": number; //-1060.5717596203124,
  "perc_chg_PL_EXTRDNRY_GAIN_OR_LOSS": number; //null,
  "perc_chg_PL_EBITDA": number; //-108.47297426694301,
  "perc_chg_CF_DEPRE_AMORTZATION": number; //-21.39159459869519,
  "perc_chg_CF_NET_WORKING_CAPITAL": number; //-256.2479771162644,
  "perc_chg_CF_CAPEX": number; //37.75940999612391,
  "perc_chg_CF_CASH_FROM_INVST_ACVT": number; //-62.70472033457754,
  "perc_chg_CF_SALE_OF_STOCK": number; //null,
  "perc_chg_CF_DEBT_BORROWING": number; //-100,
  "perc_chg_CF_DEBT_REPAYMENT": number; //-30.128274694506544,
  "perc_chg_CF_CASH_FROM_FINCNG_ACVT": number; //-125.99668634151392,
  "perc_chg_CF_NET_CASHFLOW": number; //-191.02956035130444,
  "perc_chg_CF_NET_INCM": number; //-2998.4661284661283,
  "perc_chg_ACCOUNTS_RECEIVABLES": number; //-12.54631251763067,
  "perc_chg_INVENTORIES": number; //2.1800935217749595,
  "perc_chg_NET_PLANT_PRPTY_AND_EQPMNT": number; //-9.092827434148983,
  "perc_chg_TOTAL_AST": number; //-1.9928865739502928,
  "perc_chg_LIAB_TOTAL": number; //6.031458354734407,
  "perc_chg_SHAREHOLDERS_EQUITY": number; //-31.794050790147175,
  "perc_chg_UNRALZD_HOLDING_GAIN_LOSSES": number; //-16.48152608914301,
  "perc_chg_REAL_NET_WORTH": number; //-49.57944338094537,
  "perc_chg_Total_Borrowing": number; //-54.73091876335895,
  "perc_chg_Interest_Coverage_ratio": number; //-217.87527556159912,
  "perc_chg_Profit_After_Tax_plus_Depriciation": number; //-52.16547020970936,
  "perc_chg_Operating_Cash_flow": number; //-19.89393072636235,
  "perc_chg_A_R_Turnover": number; //-19.89393072636235,
  "chg_PL_REVENUES": number; //-1051983,
  "chg_PL_COST_OF_SALES": number; //-394231,
  "chg_PL_GROS_PROFIT": number; //-657752,
  "chg_PL_EBIT": number; //-315930,
  "chg_PL_EBT": number; //-951948,
  "chg_PL_EXTRDNRY_GAIN_OR_LOSS": number; //0,
  "chg_PL_EBITDA": number; //-351178,
  "chg_CF_DEPRE_AMORTZATION": number; //-35248,
  "chg_CF_NET_WORKING_CAPITAL": number; //2050545,
  "chg_CF_CAPEX": number; //-36044,
  "chg_CF_CASH_FROM_INVST_ACVT": number; //133889,
  "chg_CF_SALE_OF_STOCK": number; //0,
  "chg_CF_DEBT_BORROWING": number; //-106425,
  "chg_CF_DEBT_REPAYMENT": number; //27762,
  "chg_CF_CASH_FROM_FINCNG_ACVT": number; //-389360,
  "chg_CF_NET_CASHFLOW": number; //589884,
  "chg_CF_NET_INCM": number; //-854263,
  "chg_ACCOUNTS_RECEIVABLES": number; //-238837,
  "chg_INVENTORIES": number; //91482,
  "chg_NET_PLANT_PRPTY_AND_EQPMNT": number; //-138320,
  "chg_TOTAL_AST": number; //-206841,
  "chg_LIAB_TOTAL": number; //493202,
  "chg_SHAREHOLDERS_EQUITY": number; //-700043,
  "chg_UNRALZD_HOLDING_GAIN_LOSSES": number; //195002,
  "chg_REAL_NET_WORTH": number; //-505041,
  "chg_Total_Borrowing": number; //-1579629,
  "chg_Interest_Coverage_ratio": number; //-2.2829241294097296,
  "chg_Profit_After_Tax_plus_Depriciation": number; //-981871,
  "chg_Operating_Cash_flow": number; //-413486,
  "chg_A_R_Turnover": number; //-413486,
  "Total_Borrowing": number; //1306544,
  "Interest_Coverage_ratio": number; //-1.2351117406358199,
  "Profit_After_Tax_plus_Depriciation": number; //900353,
  "Operating_Cash_flow": number; //1664967,
  "A_R_Turnover": number; //1664967,
  "rowNumber": number; //1,
  "CIF_NO": number; //1000862,
  "Fiscal_Year": number; //2018,
  "Fiscal_Month": number; //6,
  "PL_REVENUES": number; //1801931,
  "PL_COST_OF_SALES": number; //951743,
  "PL_GROS_PROFIT": number; //850188,
  "PL_EBIT": number; //-156958,
  "PL_EBT": number; //-862190,
  "PL_EXTRDNRY_GAIN_OR_LOSS": number; //0,
  "PL_EBITDA": number; //-27431,
  "PL_INT_EXPNS": number; //127080,
  "CF_NET_INCM": number; //-825773,
  "CF_DEPRE_AMORTZATION": number; //129527,
  "CF_NET_WORKING_CAPITAL": number; //1250326,
  "CF_CAPEX": number; //-131501,
  "CF_CASH_FROM_INVST_ACVT": number; //-79634,
  "CF_SALE_OF_STOCK": number; //0,
  "CF_DEBT_BORROWING": number; //0,
  "CF_DEBT_REPAYMENT": number; //-64384,
  "CF_CASH_FROM_FINCNG_ACVT": number; //-80336,
  "CF_NET_CASHFLOW": number; //281092,
  "CF_CASH_AT_BEGIN_OF_FISCAL_Y": number; //784359,
  "CF_CASH_AT_END_OF_FISCAL_Y": number; //1065451,
  "CF_TAXES": number; //-95799,
  "ACCOUNTS_RECEIVABLES": number; //1664806,
  "ACCUMLATED_DEPRE": number; //1726126,
  "INVENTORIES": number; //4287724,
  "CURR_AST": number; //7466087,
  "NET_PLANT_PRPTY_AND_EQPMNT": number; //1382879,
  "TOTAL_AST": number; //10172124,
  "ACCOUNTS_PAYABLE": number; //345228,
  "CURR_LIAB": number; //7222094,
  "LONG_TERM_DEBT": number; //1306544,
  "LONG_TERM_BORROWING": number; //1306544,
  "LONG_TERM_LIAB": number; //1448268,
  "LIAB_TOTAL": number; //8670362,
  "SHAREHOLDERS_EQUITY": number; //1501762,
  "TOTAL_LIAB_AND_EQUITY": number; //10172124,
  "UNRALZD_HOLDING_GAIN_LOSSES": number; //-988153,
  "REAL_NET_WORTH": number; //513609,
  "MAS_Grading": number; //3,
  "threshold": number; //42
}
