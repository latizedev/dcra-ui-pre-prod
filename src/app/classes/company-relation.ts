export class CompanyRelation {
  relation: {
    id: string,
    label?: string,
    class: string,
    showChildren?: boolean
  };
  values: {
    flagged?: boolean,
    id: string,
    label?: string,
  }[];
}
