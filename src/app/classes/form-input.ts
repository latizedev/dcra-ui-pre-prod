import {OperatorFunction} from 'rxjs';

export class UlValidators {
  min?: number;
  int?: boolean;
  positive?: boolean;

}

export class FormInput {
  key: string;
  label: string;
  value?: any;
  type: 'select' | 'text' | 'number' | 'autocomplete' | 'array' | 'date' | 'divider';
  colW?: number;
  required?: boolean;

  valid?: boolean;
  validate(value): void {
    console.log('validating', this.key, ': ', value);
    let errors = [];
    if (this.required) {
      if (value == undefined || value.trim() === '') errors.push('required')
    }
    if (this.validators) {
      if (this.required) {
        console.log('this is required');
      }
      if (this.validators.min !== undefined && value != undefined) {
        console.log(Number(value));
        if (Number(value) <= this.validators.min) errors.push('must be more than ' + this.validators.min)
      }
      if (this.validators.int) {
        if (!Number.isInteger(Number(value))) errors.push('must be integer')
      }
      if (this.validators.positive) {
        if (Number(value) < 0) errors.push('must be positive')
      }
    }
    if (errors.length) this.error = errors.join(', ');
    else this.error = null;
  };
  validator?: (x: any) => {};
  error?: string;

  selectOptions?: any[];
  numberOptions?: {
    min?: number,
    max?: number,
    step?: number,
  }
  typeAheadOptions?: {
    typeAhead?: OperatorFunction<string, readonly any[]> | null | undefined
  }

  validators?: UlValidators;


  constructor(
    input: {
      key: string,
      label?: string,
      type?: 'select' | 'text' | 'number' | 'autocomplete' | 'array' | 'date' | 'divider',
      colW?: number,
      required?: boolean,

      validator?: (x) => {},
      validators?: UlValidators,

      selectOptions?: any[],
      numberOptions?: {
        min?: number,
        max?: number,
        step?: number,
      },
      typeAheadOptions?: {
        typeAhead?: OperatorFunction<string, readonly any[]> | null | undefined
      }
    }
  ) {
    this.key = input.key;
    this.label = input.label || this.key.split('_').join(' ');
    this.value = null;
    this.type = input.type || 'text';
    this.colW = input.colW;
    this.required = input.required;

    this.validator = input.validator;
    this.validators = input.validators;


    this.selectOptions = input.selectOptions;
    this.numberOptions = input.numberOptions;
    this.typeAheadOptions = input.typeAheadOptions;
  }
}
