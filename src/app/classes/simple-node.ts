export class SimpleNode {
  id: string;
  label: string;
  flagged?: boolean;
  icon?: string;
  type?: string;
  data?: {
    [key: string]: any
  };
}
