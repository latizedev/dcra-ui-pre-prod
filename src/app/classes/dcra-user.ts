import jwt_decode from 'jwt-decode';

export class DcraUser {
  givenName: string;
  email: string;
  userRole: number;
  image?: string;
  token?: string;
  dremioToekn?: string;
  constructor(user: { displayName: string, email: string, givenName: string }, jwt: string) {
    this.givenName = user.givenName;
    this.email = user.email;
    if (
      this.email.includes('wendy') ||
      this.email.includes('pohann') ||
      this.email.includes('jiexin') ||
      this.email.includes('shunxiong') ||
      this.email.includes('charmaine') ||
      this.email.includes('analyst')
    ) {
      this.userRole = 0;
    } else if (
      this.email.includes('ling') ||
      this.email.includes('approver1')
    ) {
      this.userRole = 1;
    } else if (
      this.email.includes('wakasugi') ||
      this.email.includes('approver2')) {
      this.userRole = 2;
    }
    this.token = jwt;
  }
}
