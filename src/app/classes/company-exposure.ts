export class CompanyBackgroundApi {
  Industry: string;
  Company_Background: string;
  Business_Activities: string;
  Products_and_Services: string;
}

export class CompanyExposureApi {
  Cust_ABBR: string;
  Fiscal_Year: number;
  Fiscal_Month: number;
  Credit_Facility: string;
  Credit_Limit: string;
  Outstanding: string;
  Purpose: string;
  Maturity: string;
  Security: string;
  Covenants: string;
}

export class CompanyExposure {

  // api outputs
  // exposure table - CCIF	Cust_ABBR	Date	Credit facility	Credit limit	Outstanding	Purpose	 Maturity	Security	Covenants
  // background - CCIF	Industry	Company Background	Business Activities	Products and Services
  customerName: string;
  cifNumber: string;
  // industry: string;
  rating: string;
  existingGrading: string;
  date: Date;
  nextReviewDate: Date;
  totalExposure: string;
  latestYear: number;
  detailsOfFacilities: {
    /*CIF_NO: 3000862
Covenants: "Nil\r"
Credit_Facility: "SP11903ABCD"
Credit_Limit: "50 mil"
Cust_ABBR: "XYZ PRIME-SF"
Fiscal_Month: 5
Fiscal_Year: 2018
Maturity: "06-05-2021"
Outstanding: "Nil"
Purpose: "Hedging of FX risk"
Security: "Nil"*/
    Cust_ABBR: string,
    Fiscal_Year: number,
    Fiscal_Month: number,
    Credit_Facility: string,
    Credit_Limit: string,
    Outstanding: string,
    Purpose: string,
    Maturity: string,
    Security: string,
    Covenants: string,
    Currency: string,
    Outstanding_Amount: number
  }[];
  companyBackground: {
    url: string,
    text: string,
    industry: string;
    businessActivities: string;
    productsAndServices: string;
    management?: {
      [key: string]: string
    }
  };

  constructor(cif: string, date: Date) {
    this.customerName = cif;
    this.cifNumber = cif;
    this.rating = '';
    this.existingGrading = '';
    this.date = date;
    this.nextReviewDate = null;
    this.detailsOfFacilities = [];
    this.companyBackground = null;
  }

  updateFromBackgroundApi(bg: CompanyBackgroundApi) {
    if (bg) {
      this.companyBackground = {
        url: null,
        text: bg.Company_Background,
        businessActivities: bg.Business_Activities,
        industry: bg.Industry,
        productsAndServices: bg.Products_and_Services,
      }
    } else {
      this.companyBackground = {
        url: null,
        text: null,
        businessActivities: null,
        industry: null,
        productsAndServices: null,
      }

    }
  }

  updateFromExposureApi(exposure: CompanyExposureApi[]) {
    this.detailsOfFacilities = exposure.map(r => {
      if (r.Outstanding && (!r.Outstanding.toLowerCase().includes('nil')) && (!r.Outstanding.toLowerCase().includes('n/a'))) {

        let map = {
          k: 1000,
          m: 1000000,
          mil: 1000000,
          b: 1000000000
        }
        console.log('processing exposure data ', r.Outstanding)
        let actualAmount: number;
        let float = parseFloat(r.Outstanding.match(/-?(?:\d+(?:\.\d*)?|\.\d+)/)[0]);
        let currency = '';
        let multiplier = 1;
        console.log('float', float);
        let array = (r.Outstanding.split(float.toString()));
        console.log('array', array);
        if (array.length > 1) {
          currency = array[0].split(' ').pop().toUpperCase();
          multiplier = map[array[1].toLowerCase().charAt(0)] || 1;
        }
        console.log('multiplier', multiplier);
        let amount = float * multiplier;
        console.log('amount', amount);

        return {...r, ...{Outstanding_Amount: amount, Currency: currency}}
      } else {
        return {...r, ...{Outstanding_Amount: 0, Currency: null}}
      }
    })
    let currencies: string[] = [];
    let sums: number[] = [];
    let years = this.detailsOfFacilities.map(df => df.Fiscal_Year);
    this.latestYear = years.reduce(function(a, b) {
      return Math.max(a, b);
    }, 0);
    let latestRecords = this.detailsOfFacilities.filter(df => df.Fiscal_Year === this.latestYear);
    if (latestRecords.length) {
      latestRecords.forEach(df => {
        if (df.Outstanding_Amount) {
          let i = currencies.indexOf(df.Currency);
          if (i > -1) {
            sums[i] += df.Outstanding_Amount;
          } else {
            currencies.push(df.Currency);
            sums.push(df.Outstanding_Amount);
          }
        }
      });
      this.totalExposure = currencies.map((c,i) => c + sums[i].toLocaleString()).join(' + ');
    } else {
      this.totalExposure = null;
    }
  }
}
