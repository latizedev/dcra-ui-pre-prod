import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {GradingModule} from './grading/grading.module';
import {EarlyWarningModule} from './early-warning/early-warning.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DemoModule} from './demo/demo.module';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {AmlModule} from './aml/aml.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    GradingModule,
    EarlyWarningModule,
    DemoModule,
    AmlModule
  ],
  providers: [
    HttpClient
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
