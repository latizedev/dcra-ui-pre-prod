import { Injectable } from '@angular/core';
import {AmlTransaction} from '../classes/aml-classes';

@Injectable({
  providedIn: 'root'
})
export class AmlService {

  constructor() { }

  getFromLocal() {
    const localTransactions: AmlTransaction[] = JSON.parse(localStorage.getItem('amlTransactions'));

    let transactions: AmlTransaction[] = [];

    if (localTransactions) {
      localTransactions.forEach(t => {
        const lt = new AmlTransaction();
        lt.getFromLocal(t);
        transactions.push(lt);
      });
      return transactions;
    } else {
      return []
    }
  }
  getDateTimeStringFromDate(date: Date) {
    let isoString = date.toISOString();
    let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC',]
    return (date.getDate() < 9 ? '0' : '') + (date.getDate() + 1) + '-' + months[date.getMonth()] + '-' + date.getFullYear() + ' ' + date.toISOString().split('T')[1].slice(0, -5)
  }
}
