import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DescriptionFromHsCodeService {

  codes: {
    label: string,
    description: string
  }[] = [
    {
      label: "27011100",
      description: "ANTHRACITE NOT AGGLOMERATED (TNE)"
    },
    {
      label: "27011210",
      description: "COKING COAL NOT AGGLOMERATED (TNE)"
    },
    {
      label: "27011290",
      description: "OTHER BITUMINOUS COAL NOT AGGLOMERATED (TNE)"
    },
    {
      label: "27011900",
      description: "OTHER COAL NOT AGGLOMERATED (TNE)"
    },
    {
      label: "27012000",
      description: "BRIQUETTES OVOIDS & SIMILAR SOLID FUELS MANUFACTURED FROM COAL (TNE)"
    },
    {
      label: "27021000",
      description: "LIGNITE NOT AGGLOMERATED (TNE)"
    },
    {
      label: "27022000",
      description: "AGGLOMERATED LIGNITE (TNE)"
    },
    {
      label: "27030010",
      description: "PEAT NOT AGGLOMERATED (TNE)"
    },
    {
      label: "27030020",
      description: "AGGLOMERATED PEAT (TNE)"
    },
    {
      label: "27040010",
      description: "COKE & SEMI-COKE OF COAL (TNE)"
    },
    {
      label: "27040020",
      description: "COKE & SEMI-COKE OF LIGNITE OR OF PEAT (TNE)"
    },
    {
      label: "27040030",
      description: "RETORT CARBON (TNE)"
    },
    {
      label: "27050000",
      description: "COAL GAS WATER GAS PRODUCER GAS & SIMILAR GASES EXCL PETROLEUM GASES & OTHER GASEOUS HYDROCARBONS (TNE)"
    },
    {
      label: "27060000",
      description: "TAR DISTILLED FROM COAL LIGNITE PEAT & OTHER MINERAL TARS INCL RECONSTITUTED TARS (TNE)"
    },
    {
      label: "27071000",
      description: "BENZOL (BENZENE) (TNE)"
    },
    {
      label: "27072000",
      description: "TOLUOL (TOLUENE) (TNE)"
    },
    {
      label: "27073000",
      description: "XYLOL (XYLENES) (TNE)"
    },
    {
      label: "27074000",
      description: "NAPHTHALENE (TNE)"
    },
    {
      label: "27075000",
      description: "OTHER AROMATIC HYDROCARBON MIXTURES OF WHICH 65% OR MORE BY VOLUME DISTILS AT 250 DEGREE CELSIUS BY THE ISO 3405 METHOD (EQUIVALENT TO ASTM D 86 METHOD) (TNE)"
    },
    {
      label: "27079100",
      description: "CREOSOTE OILS (TNE)"
    },
    {
      label: "27079910",
      description: "CARBON BLACK FEEDSTOCK OF WHICH THE WEIGHT OF AROMATIC CONSTITUENTS EXCEEDS THAT OF NON AROMATIC CONSTITUENTS (TNE)"
    },
    {
      label: "27079990",
      description: "OTHER OILS & PRODUCTS OF DISTILLATION OF HIGH TEMPERATURE COAL TAR OR SIMILAR PRODUCTS OF WHICH THE WEIGHT OF AROMATIC CONSTITUENTS EXCEEDS THAT OF NON AROMATIC CONSTITUENTS (TNE)"
    },
    {
      label: "27081000",
      description: "PITCH OBTAINED FROM COAL TAR OR OTHER MINERAL TARS (TNE)"
    },
    {
      label: "27082000",
      description: "PITCH COKE OBTAINED FROM COAL TAR OR OTHER MINERAL TARS (TNE)"
    },
    {
      label: "27090010",
      description: "CRUDE PETROLEUM OILS (TNE)"
    },
    {
      label: "27090020",
      description: "CONDENSATES (TNE)"
    },
    {
      label: "27090090",
      description: "OTHER CRUDE OILS OBTAINED FROM BITUMINOUS MINERALS (TNE)"
    },
    {
      label: "27101211",
      description: "MOTOR SPIRIT OF RON 97 & ABOVE LEADED (TNE)"
    },
    {
      label: "27101212",
      description: "MOTOR SPIRIT OF RON 90 & ABOVE BUT BELOW 97 LEADED (TNE)"
    },
    {
      label: "27101213",
      description: "MOTOR SPIRIT OF RON UNDER 90 LEADED (TNE)"
    },
    {
      label: "27101221",
      description: "MOTOR SPIRIT OF RON 97 & ABOVE UNLEADED & UNBLENDED (TNE)"
    },
    {
      label: "27101222",
      description: "MOTOR SPIRIT OF RON 97 & ABOVE UNLEADED & BLENDED WITH ETHANOL (TNE)"
    },
    {
      label: "27101223",
      description: "OTHER MOTOR SPIRIT OF RON 97 & ABOVE UNLEADED & BLENDED EXCL BLENDED WITH ETHANOL (TNE)"
    },
    {
      label: "27101224",
      description: "MOTOR SPIRIT OF RON 90 & ABOVE BUT BELOW RON 97 UNLEADED & UNBLENDED (TNE)"
    },
    {
      label: "27101225",
      description: "MOTOR SPIRIT OF RON 90 & ABOVE BUT BELOW RON 97 UNLEADED & BLENDED WITH ETHANOL (TNE)"
    },
    {
      label: "27101226",
      description: "OTHER MOTOR SPIRIT OF RON 90 & ABOVE BUT BELOW RON 97 UNLEADED & BLENDED EXCL BLENDED WITH ETHANOL (TNE)"
    },
    {
      label: "27101227",
      description: "MOTOR SPIRIT OF RON BELOW 90 UNLEADED & UNBLENDED (TNE)"
    },
    {
      label: "27101228",
      description: "MOTOR SPIRIT OF RON BELOW 90 UNLEADED & BLENDED WITH ETHANOL (TNE)"
    },
    {
      label: "27101229",
      description: "OTHER MOTOR SPIRIT OF RON BELOW 90 UNLEADED & BLENDED EXCL BLENDED WITH ETHANOL (TNE)"
    },
    {
      label: "27101231",
      description: "AVIATION SPIRIT OF A KIND USED IN AVIATION PISTON ENGINES 100 OCTANE & ABOVE (TNE)"
    },
    {
      label: "27101239",
      description: "AVIATION SPIRIT OF A KIND USED IN AVIATION PISTON ENGINES BELOW 100 OCTANE (TNE)"
    },
    {
      label: "27101240",
      description: "TETRAPROPYLENE (TNE)"
    },
    {
      label: "27101250",
      description: "WHITE SPIRIT (TNE)"
    },
    {
      label: "27101260",
      description: "LOW AROMATIC SOLVENTS CONTAINING BY WEIGHT LESS THAN 1% AROMATIC CONTENT (TNE)"
    },
    {
      label: "27101270",
      description: "OTHER SOLVENT SPIRITS (TNE)"
    },
    {
      label: "27101280",
      description: "NAPHTHA REFORMATES & OTHER PREPARATIONS OF A KIND USED FOR BLENDING INTO MOTOR SPIRITS (TNE)"
    },
    {
      label: "27101291",
      description: "ALPHA OLEFINS (TNE)"
    },
    {
      label: "27101292",
      description: "OTHER PETROLEUM SPIRIT HAVING A FLASHPOINT OF LESS THAN 23 DEGREE CELSIUS (TNE)"
    },
    {
      label: "27101299",
      description: "OTHER LIGHT OILS & PREPARATIONS CONTAINING BY WEIGHT 70 % OR MORE OF PETROLEUM OILS OR OILS FROM BITUMINOUS MINERALS NOT CONTAINING BIODIESEL EXCL WASTE OILS (TNE)"
    },
    {
      label: "27101920",
      description: "TOPPED CRUDES (TNE)"
    },
    {
      label: "27101930",
      description: "CARBON BLACK FEEDSTOCK OF WHICH WEIGHT OF NON-AROMATIC CONSTITUENTS EXCEEDS THAT OF AROMATIC CONSTITUENTS (TNE)"
    },
    {
      label: "27101941",
      description: "LUBRICATING OIL FEEDSTOCK (TNE)"
    },
    {
      label: "27101942",
      description: "LUBRICATING OILS FOR AIRCRAFT ENGINES (TNE)"
    },
    {
      label: "27101943",
      description: "OTHER LUBRICATING OILS (TNE)"
    },
    {
      label: "27101944",
      description: "LUBRICATING GREASES (TNE)"
    },
    {
      label: "27101950",
      description: "HYDRAULIC BRAKE FLUID (TNE)"
    },
    {
      label: "27101960",
      description: "TRANSFORMER & CIRCUIT BREAKERS OILS (TNE)"
    },
    {
      label: "27101971",
      description: "AUTOMOTIVE DIESEL FUEL (TNE)"
    },
    {
      label: "27101972",
      description: "OTHER DIESEL FUELS EXCL AUTOMOTIVE DIESEL FUEL (TNE)"
    },
    {
      label: "27101979",
      description: "FUEL OILS (TNE)"
    },
    {
      label: "27101981",
      description: "AVIATION TURBINE FUEL HAVING A FLASH POINT OF 23 DEGREE CELSIUS OR MORE (TNE)"
    },
    {
      label: "27101982",
      description: "AVIATION TURBINE FUEL HAVING A FLASH POINT BELOW 23 DEGREE CELSIUS (TNE)"
    },
    {
      label: "27101983",
      description: "OTHER KEROSENE (TNE)"
    },
    {
      label: "27101989",
      description: "OTHER MEDIUM PETROLEUM OILS & OILS OBTAINED FROM BITUMINOUS MINERALS EXCL CRUDE OILS & MEDIUM PREPARATIONS CONTAINING BY WEIGHT 70% OR MORE OF PETROLEUM OILS OR OILS OBTAINED FROM BITUMINOUS MINERALS NOT CONTAINING BIODIESEL EXCL WASTE OILS (TNE)"
    },
    {
      label: "27101990",
      description: "OTHER PETROLEUM OILS & OILS OBTAINED FROM BITUMINOUS MINERALS (EXCL CRUDE OILS) & OTHER PREPARATIONS CONTAINING BY WEIGHT 70% OR MORE OF PETROLEUM OILS OR OILS OBTAINED FROM BITUMINOUS MINERALS BUT NOT CONTAINING BIODIESEL EXCL WASTE OILS (TNE)"
    },
    {
      label: "27102000",
      description: "PETROLEUM OILS & OILS OBTAINED FROM BITUMINOUS MINERALS EXCL CRUDE OILS & OTHER PREPARATIONS CONTAINING BY WEIGHT 70% OR MORE OF PETROLEUM OILS OR OILS OBTAINED FROM BITUMINOUS MINERAL CONTAINING BIODIESEL EXCL WASTE OILS (TNE)"
    },
    {
      label: "27109100",
      description: "WASTE OILS CONTAINING POLYCHLORINATED BIPHENYLS POLYCHLORINATED TERPHENYLS OR POLYBROMINATED BIPHENYLS (TNE)"
    },
    {
      label: "27109900",
      description: "OTHER WASTE OILS (TNE)"
    },
    {
      label: "27111100",
      description: "LIQUEFIED NATURAL GAS (TNE)"
    },
    {
      label: "27111200",
      description: "LIQUEFIED PROPANE (TNE)"
    },
    {
      label: "27111300",
      description: "LIQUEFIED BUTANE (TNE)"
    },
    {
      label: "27111410",
      description: "LIQUEFIED ETHYLENE (TNE)"
    },
    {
      label: "27111490",
      description: "LIQUEFIED PROPYLENE BUTYLENE & BUTADIENE (TNE)"
    },
    {
      label: "27111900",
      description: "OTHER LIQUEFIED PETROLEUM GASES & GASEOUS HYDROCARBONS (TNE)"
    },
    {
      label: "27112110",
      description: "NATURAL GAS OF A KIND USED AS MOTOR FUEL (TNE)"
    },
    {
      label: "27112190",
      description: "OTHER NATURAL GAS (TNE)"
    },
    {
      label: "27112900",
      description: "OTHER PETROLEUM GASES & GASEOUS HYDROCARBONS (TNE)"
    },
    {
      label: "27121000",
      description: "PETROLEUM JELLY (TNE)"
    },
    {
      label: "27122000",
      description: "PARAFFIN WAX CONTAINING BY WEIGHT LESS THAN 0.75% OF OIL (TNE)"
    },
    {
      label: "27129010",
      description: "PARAFFIN WAX CONTAINING BY WEIGHT 0.75% OR MORE OF OIL (TNE)"
    },
    {
      label: "27129090",
      description: "MICROCRYSTALLINE PETROLEUM WAX SLACK WAX OZOKERITE LIGNITE WAX PEAT WAX & OTHER MINERAL WAXES & SIMILAR PRODUCTS OBTAINED BY SYNTHESIS OR BY OTHER PROCESSES EXCL PETROLEUM JELLY & PARAFFIN WAX (TNE)"
    },
    {
      label: "27131100",
      description: "PETROLEUM COKE NOT CALCINED (TNE)"
    },
    {
      label: "27131200",
      description: "PETROLEUM COKE CALCINED (TNE)"
    },
    {
      label: "27132000",
      description: "PETROLEUM BITUMEN (TNE)"
    },
    {
      label: "27139000",
      description: "OTHER RESIDUES OF PETROLEUM OILS OR OF OILS OBTAINED FROM BITUMINOUS MINERALS (TNE)"
    },
    {
      label: "27141000",
      description: "BITUMINOUS OR OIL SHALE & TAR SANDS (TNE)"
    },
    {
      label: "27149000",
      description: "NATURAL BITUMEN & NATURAL ASPHALT & ASPHALTITES & ASPHALTIC ROCKS (TNE)"
    },
    {
      label: "27150010",
      description: "POLYURETHANE TAR COATINGS (TNE)"
    },
    {
      label: "27150090",
      description: "OTHER BITUMINOUS MIXTURES BASED ON NATURAL ASPHALT OR NATURAL BITUMEN OR PETROLEUM BITUMEN OR MINERAL TAR OR MINERAL TAR PITCH EXCL POLYURETHANE TAR COATINGS (TNE)"
    },
    {
      label: "29011000",
      description: "ACYCLIC HYDROCARBONS SATURATED (KGM)"
    },
    {
      label: "29012100",
      description: "ETHYLENE UNSATURATED (KGM)"
    },
    {
      label: "29012200",
      description: "PROPENE UNSATURATED (KGM)"
    },
    {
      label: "29012300",
      description: "BUTENE (BUTYLENE) & ISOMERS THEREOF UNSATURATED (KGM)"
    },
    {
      label: "29012400",
      description: "BUTA-1,3-DIENE & ISOPRENE UNSATURATED (KGM)"
    },
    {
      label: "29012910",
      description: "ACETYLENE (KGM)"
    },
    {
      label: "29012920",
      description: "HEXENE & ISOMERS THEREOF (KGM)"
    },
    {
      label: "29012990",
      description: "OTHER ACYCLIC HYDROCARBONS UNSATURATED (KGM)"
    },
    {
      label: "29021100",
      description: "CYCLOHEXANE (TNE)"
    },
    {
      label: "29021900",
      description: "CYCLANES CYCLENES & CYCLOTERPENES EXCL CYCLOHEXANE (TNE)"
    },
    {
      label: "29022000",
      description: "BENZENE (TNE)"
    },
    {
      label: "29023000",
      description: "TOLUENE (TNE)"
    },
    {
      label: "29024100",
      description: "O-XYLENE (TNE)"
    },
    {
      label: "29024200",
      description: "M-XYLENE (TNE)"
    },
    {
      label: "29024300",
      description: "P-XYLENE (TNE)"
    },
    {
      label: "29024400",
      description: "MIXED XYLENE ISOMERS (TNE)"
    },
    {
      label: "29025000",
      description: "STYRENE (TNE)"
    },
    {
      label: "29026000",
      description: "ETHYLBENZENE (TNE)"
    },
    {
      label: "29027000",
      description: "CUMENE (TNE)"
    },
    {
      label: "29029010",
      description: "DODECYLBENZENE (TNE)"
    },
    {
      label: "29029020",
      description: "OTHER ALKYLBENZENES (TNE)"
    },
    {
      label: "29029090",
      description: "OTHER CYCLIC HYDROCARBONS (TNE)"
    },
    {
      label: "29031110",
      description: "CHLOROMETHANE (METHYL CHLORIDE) SATURATED (KGM)"
    },
    {
      label: "29031190",
      description: "CHLOROETHANE (KGM)"
    },
    {
      label: "29031200",
      description: "DICHLOROMETHANE (METHYLENE CHLORIDE) (KGM)"
    },
    {
      label: "29031300",
      description: "CHLOROFORM (TRICHLOROMETHANE) (KGM)"
    },
    {
      label: "29031400",
      description: "CARBON TETRACHLORIDE (KGM)"
    },
    {
      label: "29031500",
      description: "ETHYLENE DICHLORIDE (ISO)(1,2-DICHLOROETHANE)(KGM)"
    },
    {
      label: "29031910",
      description: "1,2 - DICHLOROPROPANE (PROPYLENE DICHLORIDE) & DICHLOROBUTANE (KGM)"
    },
    {
      label: "29031920",
      description: "1,1,1 - TRICHLOROETHANE (METHYL CHLOROFORM) (KGM)"
    },
    {
      label: "29031990",
      description: "OTHER SATURATED CHLORINATED DERIVATIVES OF ACYCLIC HYDROCARBONS (KGM)"
    },
    {
      label: "29032100",
      description: "VINYL CHLORIDE (CHLOROETHYLENE) (KGM)"
    },
    {
      label: "29032200",
      description: "TRICHLOROETHYLENE (KGM)"
    },
    {
      label: "29032300",
      description: "TETRACHLOROETHYLENE (PERCHLOROETHYLENE) (KGM)"
    },
    {
      label: "29032900",
      description: "OTHER UNSATURATED CHLORINATED DERIVATIVES OF ACYCLIC HYDROCARBONS (KGM)"
    },
    {
      label: "29033100",
      description: "ETHYLENE DIBROMIDE (ISO)(1,2-DIBROMOETHANE) (KGM)"
    },
    {
      label: "29033910",
      description: "BROMOMETHANE (METHYL BROMIDE) (KGM)"
    },
    {
      label: "29033990",
      description: "OTHER FLUORINATED BROMINATED OR IODINATED DERIVATIVES OF ACYCLIC HYDROCARBONS (KGM)"
    },
    {
      label: "29037100",
      description: "CHLORODIFLUOROMETHANE (KGM)"
    },
    {
      label: "29037200",
      description: "DICHLOROTRIFLUOROETHANES (KGM)"
    },
    {
      label: "29037300",
      description: "DICHLOROFLUOROETHANES (KGM)"
    },
    {
      label: "29037400",
      description: "CHLORODIFLUOROETHANES (KGM)"
    },
    {
      label: "29037500",
      description: "DICHLOROPENTAFLUOROPROPANES (KGM)"
    },
    {
      label: "29037600",
      description: "BROMOCHLORODIFLUOROMETHANE BROMOTRIFLUOROMETHANE & DIBROMOTETRAFLUOROETHANES (KGM)"
    },
    {
      label: "29037700",
      description: "DERIVATIVES OF ACYCLIC HYDROCARBONS PERHALOGENATED ONLY WITH FLUORINE & CHLORINE (KGM)"
    },
    {
      label: "29037800",
      description: "OTHER PERHALOGENATED DERIVATIVES OF ACYCLIC HYDROCARBONS CONTAINING TWO OR MORE HALOGENS (KGM)"
    },
    {
      label: "29037900",
      description: "OTHER HALOGENATED DERIVATIVES OF ACYCLIC HYDROCARBONS CONTAINING TWO OR MORE HALOGENS (KGM)"
    },
    {
      label: "29038100",
      description: "1,2,3,4,5,6-HEXACHLOROCYCLOHEXANE (HCH(ISO)) INCL LINDANE (ISO, INN) (KGM)"
    },
    {
      label: "29038200",
      description: "ALDRIN (ISO) CHLORDANE (ISO) & HEPTACHLOR (ISO)(KGM)"
    },
    {
      label: "29038300",
      description: "MIREX (ISO) (KGM)"
    },
    {
      label: "29038900",
      description: "OTHER HALOGENATED DERIVATIVES OF CYCLANIC CYCLENIC OR CYCLOTERPENIC HYDROCARBONS (KGM)"
    },
    {
      label: "29039100",
      description: "CHLOROBENZENE O-DICHLOROBENZENE & P-DICHLOROBENZENE (KGM)"
    },
    {
      label: "29039200",
      description: "HEXACHLOROBENZENE (ISO) & DDT (ISO) (KGM)"
    },
    {
      label: "29039300",
      description: "PENTACHLOROBENZENE (ISO) (KGM)"
    },
    {
      label: "29039400",
      description: "HEXBROMOBIPHENYLS (KGM)"
    },
    {
      label: "29039900",
      description: "OTHER HALOGENATED DERIVATIVES OF AROMATIC HYDROCARBONS (KGM)"
    },
    {
      label: "29041000",
      description: "DERIVATIVES OF HYDROCARBONS CONTAINING ONLY SULPHO GROUPS THEIR SALTS & ETHYL ESTERS (KGM)"
    },
    {
      label: "29042010",
      description: "TRINITROTOLUENE (KGM)"
    },
    {
      label: "29042090",
      description: "DERIVATIVES OF HYDROCARBONS CONTAINING ONLY NITRO OR NITROSO GROUPS EXCL TRINITROTOLUENE (KGM)"
    },
    {
      label: "29043100",
      description: "PERFLUOROOCTANE SULPHONIC ACID (KGM)"
    },
    {
      label: "29043200",
      description: "AMMONIUM PERFLUOROOCTANE SULPHONATE (KGM)"
    },
    {
      label: "29043300",
      description: "LITHIUM PERFLUOROOCTANE SULPHONATE (KGM)"
    },
    {
      label: "29043400",
      description: "POTASSIUM PERFLUOROOCTANE SULPHONATE (KGM)"
    },
    {
      label: "29043500",
      description: "OTHER SALTS OF PERFLUOROOCTANE SULPHONIC ACID (KGM)"
    },
    {
      label: "29043600",
      description: "PERFLUOROOCTANE SULPHONYL FLUORIDE (KGM)"
    },
    {
      label: "29049100",
      description: "TRICHLORONITROMETHANE (CHLOROPICRIN) (KGM)"
    },
    {
      label: "29049900",
      description: "OTHER SULPHONATED NITRATED OR NITROSATED DERIVATIVES OF HYDROCARBONS (KGM)"
    },
    {
      label: "38112110",
      description: "ADDITIVES FOR LUBRICATING OIL CONTAINING PETROLEUM OR OILS FROM BITUMINOUS MINERALS FOR RETAIL (KGM)"
    },
    {
      label: "38112190",
      description: "ADDITIVES FOR LUBRICATING OIL CONTAINING PETROLEUM OR OILS FROM BITUMINOUS MINERALS NOT FOR RETAIL (KGM)"
    },
    {
      label: "38119090",
      description: "OTHER PREPARED ADDITIVES FOR OTHER MINERAL OILS OR FOR OTHER LIQUIDS OF THE SAME PURPOSE (KGM)"
    },
    {
      label: "39011012",
      description: "LINEAR LOW DENSITY POLYETHYLENE IN LIQUID OR PASTE FORM (TNE)"
    },
    {
      label: "39011019",
      description: "OTHER POLYETHYLENE OF SPECIFIC GRAVITY BELOW 0.94 IN LIQUID OR PASTE FORM (TNE)"
    },
    {
      label: "39011092",
      description: "LINEAR LOW DENSITY POLYETHYLENE IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39011099",
      description: "OTHER POLYETHYLENE OF SPECIFIC GRAVITY BELOW 0.94 IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39012000",
      description: "POLYETHYLENE OF SPECIFIC GRAVITY 0.94 OR MORE (TNE)"
    },
    {
      label: "39013000",
      description: "ETHYLENE-VINYL ACETATE COPOLYMERS (TNE)"
    },
    {
      label: "39014000",
      description: "ETHYLENE-ALPHA-OLEFIN COPOLYMERS HAVING A SPECIFIC GRAVITY OF LESS THAN 0.94 (TNE)"
    },
    {
      label: "39019040",
      description: "OTHER POLYMERS OF ETHYLENE IN DISPERSION (TNE)"
    },
    {
      label: "39019090",
      description: "OTHER POLYMERS OF ETHYLENE IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39021030",
      description: "POLYPROPYLENE IN DISPERSION (TNE)"
    },
    {
      label: "39021040",
      description: "POLYPROPYLENE GRANULES OR PELLETS OR BEADS OR FLAKES OR CHIPS AND SIMILAR FORM (TNE)"
    },
    {
      label: "39021090",
      description: "POLYPROPYLENE IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39022000",
      description: "POLYISOBUTYLENE (TNE)"
    },
    {
      label: "39023030",
      description: "PROPYLENE COPOLYMERS IN LIQUID OR PASTE FORM (TNE)"
    },
    {
      label: "39023090",
      description: "PROPYLENE COPOLYMERS IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39029010",
      description: "CHLORINATED POLYPROPYLENE USED FOR PRINTING INK FORMULATION (TNE)"
    },
    {
      label: "39029090",
      description: "OTHER POLYMERS OF PROPYLENE OR OF OTHER OLEFINS IN PRIMARY FORMS (TNE)"
    },
    {
      label: "39031110",
      description: "POLYSTYRENE EXPANSIBLE IN GRANULES (TNE)"
    },
    {
      label: "39031190",
      description: "POLYSTYRENE EXPANSIBLE IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39031910",
      description: "POLYSTYRENE NOT EXPANSIBLE IN DISPERSION (TNE)"
    },
    {
      label: "39031920",
      description: "POLYSTYRENE NOT EXPANSIBLE IN GRANULES PELLETS OR BEADS OR FLAKES OR CHIPS AND SIMILAR FORMS (TNE)"
    },
    {
      label: "39031990",
      description: "OTHER POLYSTYRENE NOT EXPANSIBLE IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39032040",
      description: "STYRENE-ACRYLONITRILE COPOLYMERS IN AQUEOUS DISPERSION (TNE)"
    },
    {
      label: "39032050",
      description: "STYRENE-ACRYLONITRILE COPOLYMERS IN NON-AQUEOUS DISPERSION (TNE)"
    },
    {
      label: "39032090",
      description: "STYRENE-ACRYLONITRILE COPOLYMERS IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39033040",
      description: "ACRYLONITRILE-BUTADIENE-STYRENE COPOLYMERS IN AQUEOUS DISPERSION (TNE)"
    },
    {
      label: "39033050",
      description: "ACRYLONITRILE-BUTADIENE-STYRENE COPOLYMERS IN NON-AQUEOUS DISPERSION (TNE)"
    },
    {
      label: "39033060",
      description: "ACRYLONITRILE-BUTADIENE-STYRENE COPOLYMERS IN GRANULES (TNE)"
    },
    {
      label: "39033090",
      description: "ACRYLONITRILE-BUTADIENE-STYRENE COPOLYMERS IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39039030",
      description: "OTHER POLYMERS OF STYRENE IN DISPERSION (TNE)"
    },
    {
      label: "39039091",
      description: "IMPACT POLYSTYRENE OF NOTCHED IZOD IMPACT AT 23oC LESS THAN 80J/M (TNE)"
    },
    {
      label: "39039099",
      description: "OTHER POLYMERS OF STYRENE IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39041010",
      description: "POLYVINYL CHLORIDE HOMOPOLYMERS SUSPENSION TYPE (TNE)"
    },
    {
      label: "39041091",
      description: "POLYVINYL CHLORIDE NOT MIXED WITH ANY OTHER SUBSTANCES IN GRANULES (TNE)"
    },
    {
      label: "39041092",
      description: "POLYVINYL CHLORIDE NOT MIXED WITH ANY OTHER SUBSTANCES IN POWDER FORM (TNE)"
    },
    {
      label: "39041099",
      description: "POLYVINYL CHLORIDE NOT MIXED WITH ANY OTHER SUBSTANCES IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39042110",
      description: "OTHER POLYVINYL CHLORIDE NON-PLASTICISED IN GRANULES (TNE)"
    },
    {
      label: "39042120",
      description: "OTHER POLYVINYL CHLORIDE NON-PLASTICISED IN POWDER FORM (TNE)"
    },
    {
      label: "39042190",
      description: "OTHER POLYVINYL CHLORIDE NON-PLASTICISED IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39042210",
      description: "OTHER POLYVINYL CHLORIDE PLASTICISED IN DISPERSION (TNE)"
    },
    {
      label: "39042220",
      description: "OTHER POLYVINYL CHLORIDE PLASTICISED IN GRANULES (TNE)"
    },
    {
      label: "39042230",
      description: "OTHER POLYVINYL CHLORIDE PLASTICISED IN POWDER FORM (TNE)"
    },
    {
      label: "39042290",
      description: "OTHER POLYVINYL CHLORIDE PLASTICISED IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39043010",
      description: "VINYL CHLORIDE-VINYL ACETATE COPOLYMERS IN GRANULES (TNE)"
    },
    {
      label: "39043020",
      description: "VINYL CHLORIDE-VINYL ACETATE COPOLYMERS IN POWDER FORM (TNE)"
    },
    {
      label: "39043090",
      description: "VINYL CHLORIDE-VINYL ACETATE COPOLYMERS IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39044010",
      description: "OTHER VINYL CHLORIDE COPOLYMERS IN GRANULES (TNE)"
    },
    {
      label: "39044020",
      description: "OTHER VINYL CHLORIDE COPOLYMERS IN POWDER FORM (TNE)"
    },
    {
      label: "39044090",
      description: "OTHER VINYL CHLORIDE COPOLYMERS IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39045040",
      description: "VINYLIDENE CHLORIDE POLYMERS IN DISPERSION (TNE)"
    },
    {
      label: "39045050",
      description: "VINYLIDENE CHLORIDE POLYMERS IN GRANULES (TNE)"
    },
    {
      label: "39045060",
      description: "VINYLIDENE CHLORIDE POLYMERS IN POWDER FORM (TNE)"
    },
    {
      label: "39045090",
      description: "VINYLIDENE CHLORIDE POLYMERS IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39046110",
      description: "POLYTETRAFLUOROETHYLENE IN GRANULES (TNE)"
    },
    {
      label: "39046120",
      description: "POLYTETRAFLUOROETHYLENE IN POWDER FORM (TNE)"
    },
    {
      label: "39046190",
      description: "POLYTETRAFLUOROETHYLENE IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39046930",
      description: "OTHER FLUORO-POLYMERS IN DISPERSION (TNE)"
    },
    {
      label: "39046940",
      description: "OTHER FLUORO-POLYMERS IN GRANULES (TNE)"
    },
    {
      label: "39046950",
      description: "OTHER FLUORO-POLYMERS IN POWDER FORM (TNE)"
    },
    {
      label: "39046990",
      description: "OTHER FLUORO-POLYMERS IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39049030",
      description: "OTHER POLYMERS OF VINYL CHLORIDE OR OF OTHER HALOGENATED OLEFINS IN DISPERSION (TNE)"
    },
    {
      label: "39049040",
      description: "OTHER POLYMERS OF VINYL CHLORIDE OR OF OTHER HALOGENATED OLEFINS IN GRANULES (TNE)"
    },
    {
      label: "39049050",
      description: "OTHER POLYMERS OF VINYL CHLORIDE OR OF OTHER HALOGENATED OLEFINS IN POWDER FORM (TNE)"
    },
    {
      label: "39049090",
      description: "OTHER POLYMERS OF VINYL CHLORIDE OR OF OTHER HALOGENATED OLEFINS IN OTHER PRIMARY FORMS (TNE)"
    },
    {
      label: "39111000",
      description: "PETROLEUM RESINS COUMARONE INDENE OR COUMARONE-INDENE RESINS & POLYTERPENES (TNE)"
    },
    {
      label: "68071000",
      description: "ARTICLES OF ASPHALT OR SIMILAR MATERIAL IN ROLLS (TNE)"
    },
    {
      label: "68079010",
      description: "TILES OF ASPHALT OR SIMILAR MATERIAL (TNE)"
    },
    {
      label: "68079090",
      description: "OTHER ARTICLES OF ASPHALT OR SIMILAR MATERIAL (TNE)"
    }
  ]

  getDescription(label: string) {
    // console.log(hsCode);
    // console.log(this.codes.filter(c => c.hsCode === hsCode));
    return this.codes.filter(c => c.label === label)[0].description;
  }

  constructor() {
  }
}
