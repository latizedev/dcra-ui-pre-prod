import { TestBed } from '@angular/core/testing';

import { AmlApiService } from './aml-api.service';

describe('AmlApiService', () => {
  let service: AmlApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AmlApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
