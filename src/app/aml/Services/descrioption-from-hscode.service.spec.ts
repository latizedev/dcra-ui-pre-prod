import { TestBed } from '@angular/core/testing';

import { DescriptionFromHsCodeService } from './description-from-hs-code.service';

describe('DescrioptionFromHSCodeService', () => {
  let service: DescriptionFromHsCodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DescriptionFromHsCodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
