import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, timer, Subscription, Subject, from} from 'rxjs';
import {AmlTransactionApiInput} from '../classes/aml-classes';

@Injectable({
  providedIn: 'root'
})
export class AmlApiService {

  url = 'http://20.212.82.171:'; // http://13.250.185.182:8000/
  token;

  constructor(private http: HttpClient) { }
  checkForDEVH(input: AmlTransactionApiInput): any {
    return this.http.post(this.url + '8000/RiskScoring ', input)
    // return this.http.request('POST', this.url + '8000/RiskScoring ',  {body: input})
    /*return from( // wrap the fetch in a from if you need an rxjs Observable
       fetch(
        this.url + '8000/RiskScoring ',
        {
          body: JSON.stringify(input),
          headers: {
            'Content-Type': 'application/json',
          },
          method: 'POST',
          mode: 'no-cors'
        }
      )
    )*/
  }
  predictLabel(inputText: string[], predictionRate?: number, threshold?: number) {
    return this.http.post(this.url + '8000/predict_label ', {INPUTTEXT: inputText})
  }
  isHsCodeValid(inputText: string[], predictionRate?: number, threshold?: number) {
    return this.http.post(this.url + '8000/ishscodevalid ', {INPUTTEXT: inputText})
  }
  hsCodeRecency(
    body: {
        MODULE: string,
        CCIF_NO: number,
        REGISTERED_DATE: string,
        HSCODE: number
    }) {
    return this.http.post(this.url + '8000/HscodeRisc ', body)
  }
}
