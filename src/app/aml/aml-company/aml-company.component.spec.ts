import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmlCompanyComponent } from './aml-company.component';

describe('AmlCompanyComponent', () => {
  let component: AmlCompanyComponent;
  let fixture: ComponentFixture<AmlCompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmlCompanyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmlCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
