import {Component, Input, OnInit} from '@angular/core';
import {AmlCompany, AmlGoods, AmlTransaction, AmlTransactionFormValue} from '../classes/aml-classes';
import {VisualMakerService} from '../../services/visual-maker.service';
import {
  faBuilding, faCaretDown, faCheck,
  faExchangeAlt, faFileUpload,
  faFlag,
  faHandPaper,
  faTasks,
  faThumbsUp,
  faThumbtack
} from '@fortawesome/free-solid-svg-icons';
import {CytoElement} from '../../classes';
import {ActivatedRoute} from '@angular/router';
import {AmlService} from '../Services/aml.service';
import {colors} from '@angular/cli/utilities/color';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-aml-company',
  templateUrl: './aml-company.component.html',
  styleUrls: ['./aml-company.component.scss']
})
export class AmlCompanyComponent implements OnInit {

  date = new Date();
  companyID: string;
  transactionMin: string;
  company: AmlCompany;
  relations = [
    {
      "relation": {
        "id": "hasIndustry",
        "class": "Industry"
      },
      "values": [
        {
          "id": "Fuel Production"
        }
      ]
    },
    {
      "relation": {
        "id": "hasC_CIF",
        "class": "literal"
      },
      "values": [
        {
          "id": "ABC1234"
        }
      ]
    },
    {
      "relation": {
        "id": "hasProducts",
        "class": "Product"
      },
      "values": [
        {
          "id": "Building Materials"
        },
        {
          "id": "Construction Services"
        },
        {
          "id": "Construction Services"
        },
        {
          "id": "Construction Services"
        }
      ]
    },
    {
      "relation": {
        "id": "hasTransactions",
        "class": "Transaction"
      },
      "values": [
        {
          "id": "998356"
        },
        {
          "id": "778941"
        },
        {
          "id": "236541"
        }
      ]
    },
    {
      "relation": {
        "id": "hasAffiliations",
        "class": "Company"
      },
      "values": [
        {
          "id": "Giggle",
          "label": "Giggle Ltd"
        }
      ]
    }
  ];

  // selectedTransaction: AmlTransaction;

  isCollapsed: boolean[] = [];

  activeTab = 'dashboard';
  activeDisplay: 'list' | 'table' = 'list';

  icons = {
    pin: faThumbtack,
    stop: faHandPaper,
    allow: faThumbsUp,
    company: faBuilding,
    flag: faFlag,
    checklist: faTasks,
    transaction: faExchangeAlt,
    upload: faFileUpload,
    check: faCheck,
    toggle: faCaretDown
  }

  /*charts = {
    flags: null,
    geo: null,
    locations: null,
    timeline: null,
    timing: null,
    volume: null,
    amount: null,
    goods: null,
    industries: null,
    scatter: null,
  }*/

  collapsed: boolean[] = [];

  topMetrics: {
    short: string,
    long?: string,
    count: number,
    icon: string
  }[] =
    [ // 'Total Transactions', 'Cleared Transactions', 'Significant Deviation from Historical Trade', 'Goods Inconsistent with Industry'
      {
        short: 'Total',
        count: 0,
        icon: 'transaction'
      }, {
      short: 'Cleared',
      count: 0,
      icon: 'allow'
    }, {
      short: 'DEVH',
      long: 'Significant Deviation from Historical Trade',
      count: 0,
      icon: 'flag'
    }, {
      short: 'GINC',
      long: 'Goods Inconsistent with Industry',
      count: 0,
      icon: 'flag'
    }
    ];

  charts: {
    type: string,
    x: string,
    y: string,
    data: string,
    chart: any,
    w: number,
    h?: 'large' | 'small'
  }[] = [
    {
      type: 'bar',
      x: 'AMOUNT',
      y: 'COUNT',
      data: null,
      chart: null,
      w: 6,
      h: 'small'
    },
    {
      type: 'bar',
      x: 'QUANTITY',
      y: 'COUNT',
      data: null,
      chart: null,
      w: 6,
      h: 'small'
    },
    {
      type: 'pie',
      x: 'PORT_OF_LOADING',
      y: 'COUNT',
      data: null,
      chart: null,
      w: 6,
      h: 'small'
    },
    {
      type: 'pie',
      x: 'GOODS_DESCRIPTION',
      y: 'COUNT',
      data: null,
      chart: null,
      w: 6,
      h: 'small'
    },
    {
      type: 'scatter',
      x: 'AMOUNT',
      y: 'QUANTITY',
      data: null,
      chart: null,
      w: 12,
      h: 'large'
    }
  ]
  timeline: null;

  locations = ['SGP', 'CHN', 'THA', 'IND'];
  goods = ['Bitumen', 'Kerosene', 'Butane', 'Petroleum']
  payees = ['Brendel Manufacturing', 'Charminar Motors', 'Avionix', 'Raindere', 'Stevens Ltd', 'Dorfmatics', 'B Pumps', 'Revelin', 'Rookia', 'Russo Oil'];
  industries = ['Motor Vehicles', 'Airlines', 'Manufacturing', 'Freight', 'Oil & Gas']


  numProps: { key: string, label: string }[] = [
    {key: 'AMOUNT', label: 'AMOUNT'},
    // {key: 'REGISTERED_DATE', label: 'REGISTERED DATE'},
    {key: 'QUANTITY', label: 'QUANTITY'},
    {key: 'UNITPRICE', label: 'UNIT PRICE'}
  ];
  stringProps: { key: string, label: string }[] = [
    {key: 'CURRENCY', label: 'CURRENCY'},
    {key: 'PORT_OF_LOADING', label: 'PORT OF LOADING'},
    {key: 'PORT_OF_DISCHARGE', label: 'PORT OF DISCHARGE'},
    {key: 'GOODS_DESCRIPTION', label: 'GOODS'}
  ];

  locationsData = {
    SGP: 20, CHN: 30, THA: 15, IND: 10, NGA: 1
  }
  cytoElements: CytoElement;

  columns = ['risk', 'minNo', 'registeredDate', 'amount', 'goods', 'flags'];

  // showDataImporter = false;
  // transactionFormValue = new AmlTransactionFormValue();

  constructor(
    private vsm: VisualMakerService,
    private route: ActivatedRoute,
    private amls: AmlService,
    private titleService: Title,
  ) {
  }

  changeDisplay(mode: 'list' | 'table') {
    this.activeDisplay = mode;
    this.timeline = null;
    this.charts.forEach(ch => ch.chart = null);
    setTimeout(() => {
      this.createTimeline();
      this.charts.forEach(ch => this.createChart(ch));
    }, 400)
  }

  getCompany() {
    // run api to get company from id
    this.company = new AmlCompany(this.companyID, 'CCIF ' + this.companyID);
    // this.company.transactions = new Array(50 + Math.round(Math.random() * 100)).fill(new AmlTransaction(null)).map((i) => this.createTransaction());
    this.company.transactions = this.amls.getFromLocal().filter(t => t.company.id === this.companyID);
    this.company.transactions.sort((a, b) => {
      return (a.registeredDate || new Date()).getTime() - (b.registeredDate || new Date()).getTime()
    });
    this.cytoElements = this.vsm.createCytoNetwork(this.company.name, this.relations);

    this.topMetrics[0].count = this.company.transactions.length;
    this.topMetrics[1].count = this.company.transactions.filter(t => t.status == 'cleared').length;
    this.topMetrics[2].count = this.company.transactions.filter(t => t.flags.map(f => f.key).includes('02.DEVH')).length;
    this.topMetrics[3].count = this.company.transactions.filter(t => t.flags.map(f => f.key).includes('01.GINC')).length;
    console.log(this.company);
  }

  randomInt(max: number) {
    return Math.floor(Math.random() * max);
  }

  propertyMap(t: AmlTransaction, prop: string): any {
    if (prop === 'GOODS_DESCRIPTION') {
      return t.goods[0].description ? t.goods[0].description.toUpperCase() : '--'
    }
    if (prop === 'PORT_OF_LOADING') {
      return t.goods[0].portOfLoading ? t.goods[0].portOfLoading.toUpperCase() : '--'
    }
    if (prop === 'PORT_OF_DISCHARGE') {
      return t.goods[0].portOfDischarge ? t.goods[0].portOfDischarge.toUpperCase() : '--'
    }
    if (prop === 'CURRENCY') {
      return t.currency ? t.currency.toUpperCase() : '--'
    }
    if (prop === 'AMOUNT') {
      return t.amount
    }
    if (prop === 'QUANTITY') {
      return t.goods[0].quantity || 0
    }
  }

  createTimeline() {
    this.timeline = this.vsm.createAmlTimeline(
      [
        {
          y: this.company.transactions.filter(t => t.risk <= 25).map(t => t.amount),
          x: this.company.transactions.filter(t => t.risk <= 25).map(t => t.registeredDate),
          text: this.company.transactions.filter(t => t.risk <= 25).map(t => Math.round(t.risk) + '% ' + t.currency)
        },
        {
          y: this.company.transactions.filter(t => t.risk > 25 && t.risk <= 60).map(t => t.amount),
          x: this.company.transactions.filter(t => t.risk > 25 && t.risk <= 60).map(t => t.registeredDate),
          text: this.company.transactions.filter(t => t.risk > 25 && t.risk <= 60).map(t => Math.round(t.risk) + '% ' + t.currency)
        },
        {
          y: this.company.transactions.filter(t => t.risk > 60).map(t => t.amount),
          x: this.company.transactions.filter(t => t.risk > 60).map(t => t.registeredDate),
          text: this.company.transactions.filter(t => t.risk > 60).map(t => Math.round(t.risk) + '% ' + t.currency)
        }
      ]
    );
  }

  selectAll(bool: boolean) {
    this.company.transactions.forEach(t => t.selected = bool);
    this.charts.forEach(ch => this.createChart(ch));
  }

  createChart(chart: { type: string, x: string, y: string, data: string, chart: any, w: number }) {
    chart.chart = null;
    let selected: AmlTransaction[] = this.company.transactions.filter(t => t.selected);
    let unselected: AmlTransaction[] = this.company.transactions.filter(t => !t.selected);
    setTimeout(() => {

      if (chart.type === 'bar') {
        chart.chart = this.vsm.createHistogram(
          // this.company.transactions.map(t => Math.round(this.propertyMap(t, chart.x) / 100) / 10),
          unselected.map(t => this.propertyMap(t, chart.x)),
          {
            ticksuffix: ''
          },
          null,
          selected.map(t => this.propertyMap(t, chart.x))
        );
      }
      if (chart.type === 'pie') {
        chart.chart = this.vsm.createAmlCompanyPie(
          this.company.transactions.map(t => this.propertyMap(t, chart.x)),
          [...new Set(selected.map(t => this.propertyMap(t, chart.x)))]
        );
      }
      if (chart.type === 'scatter') {
        chart.chart = this.vsm.createScatterPlot(
          {
            x: this.company.transactions.map(t => this.propertyMap(t, chart.x)),
            y: this.company.transactions.map(t => this.propertyMap(t, chart.y)),
            c: this.company.transactions.map(t => t.selected ? '#29357F' : '#27ABE2')
          },
          {
            x: this.numProps.filter(p => p.key === chart.x)[0].label.toLowerCase(),
            y: this.numProps.filter(p => p.key === chart.y)[0].label.toLowerCase()
          },
          {
            x: chart.x === 'AMOUNT' ? 'M' : (chart.x === 'QUANTITY' ? 'K' : ''),
            y: chart.y === 'AMOUNT' ? 'M' : (chart.y === 'QUANTITY' ? 'K' : '')
          },
        );
        console.log(chart.chart);
      }
    }, 50)
  }

  chartClicked(chart: any, nodes: { i: number, label: string, points: number[] }[]) {
    console.log(nodes);
    if (chart.type === 'scatter') {
      // this.company.transactions[node.i].selected = !this.company.transactions[node.i].selected;
      nodes.forEach(n => {
        this.company.transactions[n.i].selected = !this.company.transactions[n.i].selected
      })
      this.charts.forEach(ch => this.createChart(ch));
    }
    if (chart.type === 'pie') {
      // this.company.transactions[node.i].selected = !this.company.transactions[node.i].selected;
      nodes.forEach(n => {
        n.points.forEach((p) => {
          this.company.transactions[p].selected = !this.company.transactions[p].selected;
        })
      })
      this.charts.forEach(ch => this.createChart(ch));
    }
    if (chart.type === 'bar') {
      // this.company.transactions[node.i].selected = !this.company.transactions[node.i].selected;
      nodes.forEach(n => {
        n.points.forEach((p) => {
          this.company.transactions[p].selected = !this.company.transactions[p].selected;
        })
      })
      this.charts.forEach(ch => this.createChart(ch));
    }
  }

  transactionSelected(t: AmlTransaction) {
    t.selected = !t.selected;
    this.charts.forEach(ch => this.createChart(ch));

  }

  selectionChange(transactions: AmlTransaction[]) {
    console.log('transactions selected', this.company.transactions.filter(t => t.selected));
    // this.selectedTransaction = t;
    this.charts.forEach(ch => this.createChart(ch));
  }

  ngOnInit(): void {
    this.titleService.setTitle('ATRiM');

    this.timeline = null;
    this.companyID = decodeURIComponent(this.route.snapshot.paramMap.get('id'));
    this.transactionMin = decodeURIComponent(this.route.snapshot.paramMap.get('min'));
    // this.transactionFormValue.CCIF_NO = this.companyID;
    this.getCompany();

    let data = {
      '01.GINC': 6,
      '02.DEVG': 12,
      '04.HGEO': 7,
      '07.OPCG': 8,
      '10.LGLM': 3,
      '12.TDIS': 6,
      '15.DUAG': 3,
      '16.MLIN': 4
    };

    setTimeout(() => {
      this.charts.forEach(ch => this.createChart(ch));
    }, 100)

    setTimeout(() => {
      this.createTimeline();
    }, 1000)

    /*this.charts.timing = this.vsm.createHistogram(
      this.company.transactions.map(t => t.registeredDate.getHours() * 60 + t.registeredDate.getMinutes()),
      {
        ticksuffix: 'hrs',
        dtick: 600
      },
      {
        end: 2400,
        size: 300,
        start: 0
      });
    this.charts.amount = this.vsm.createHistogram(
      this.company.transactions.map(t => t.amount),
      {
        ticksuffix: 'k',
      },
      null);

    let locationsData = {};
    this.locations.forEach(l => {
      locationsData[l] = this.company.transactions.filter(t => t.goods[0].portOfLoading === l).length
    });
    this.charts.locations = this.vsm.createPie(locationsData);
    this.charts.geo = this.vsm.createAmlMaps();

    let goodsData = {};
    this.goods.forEach(g => {
      goodsData[g] = this.company.transactions.filter(t => t.goods[0].description === g).length
    });
    this.charts.goods = this.vsm.createPie(goodsData);

    let industryData = {};
    this.industries.forEach(industry => {
      industryData[industry] = this.company.transactions.filter(t => t.company.industry === industry).length
    })
    this.charts.industries = this.vsm.createPie(industryData);

    this.charts.scatter = this.vsm.createScatterPlot(
      {
        x: this.company.transactions.map(t => t.registeredDate.getHours() * 60 + t.registeredDate.getMinutes()),
        y: this.company.transactions.map(t => t.amount)
      }
    );
    console.log(this.charts.scatter);
  */
  }
}
