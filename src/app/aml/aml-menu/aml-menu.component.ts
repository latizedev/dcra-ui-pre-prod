import { Component, OnInit } from '@angular/core';
import {faExchangeAlt, faFlag, faHome, faList} from '@fortawesome/free-solid-svg-icons';
import {AmlCompany, AmlTransaction} from '../classes/aml-classes';
import {AmlService} from '../Services/aml.service';

@Component({
  selector: 'app-aml-menu',
  templateUrl: './aml-menu.component.html',
  styleUrls: ['./aml-menu.component.scss']
})
export class AmlMenuComponent implements OnInit {

  transactions: AmlTransaction[];
  companies: AmlCompany[];

  icons = {
    home: faHome,
    list: faList,
    arrows: faExchangeAlt,
    flag: faFlag
  }

  menuItems = ['list'];
  activeItem;

  constructor(
    private amls: AmlService
  ) { }

  openList(item: string) {
    this.activeItem = item
  }

  ngOnInit(): void {
    this.transactions = this.amls.getFromLocal();
    this.companies =  [];
    this.transactions.forEach(t => {
      let filtered = this.companies.filter(c => c.id === t.company.id);
      if (filtered.length) {
        let company = filtered[0];
        company.transactions.push(t);
        company.flags.push(...t.flags);
      }
      else {
        let company = new AmlCompany(t.company.id);
        company.transactions.push(t);
        company.flags.push(...t.flags);
        this.companies.push(company);
      }
    })

  }

}
