import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmlMenuComponent } from './aml-menu.component';

describe('AmlMenuComponent', () => {
  let component: AmlMenuComponent;
  let fixture: ComponentFixture<AmlMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmlMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmlMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
