import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {FormInput} from '../../../classes/form-input';
import {
  AmlTransactionApiInput,
  AmlTransactionFormValue,
  AmlApiOutputDEVH
} from '../../classes/aml-classes';
import {AmlApiService} from '../../Services/aml-api.service';
import {faMagic} from '@fortawesome/free-solid-svg-icons';
import {Observable, OperatorFunction} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, map, switchMap} from 'rxjs/operators';
import {DescriptionFromHsCodeService} from '../../Services/description-from-hs-code.service';

@Component({
  selector: 'app-transaction-import',
  templateUrl: './transaction-import.component.html',
  styleUrls: ['./transaction-import.component.scss']
})
export class TransactionImportComponent implements OnInit {

  @Output() transactionImported = new EventEmitter<AmlTransactionFormValue[]>();
  @Input() transactionFormValue: AmlTransactionFormValue;

  icons = {
    auto: faMagic
  }

  activeTab = 'single';
  // compulsory module amount ccy
  transactionInputs: FormInput[] = [
    new FormInput({
      key: 'REGISTERED_DATE', type: 'date', colW: 3
    }),
    new FormInput({key: 'DIVIDER', type: 'divider', colW: 12}),
    new FormInput({
      key: 'MIN_NO', colW: 4
    }),
    new FormInput({
      key: 'CCIF_NO', colW: 4
    }),
    new FormInput({
      key: 'LC_NUMBER', colW: 4
    }),
    /*new FormInput({
      key: 'GBASE_REFNO', colW: 3
    }),*/
    new FormInput({
      key: 'MODULE', colW: 4, type: 'select', selectOptions: ['SHIP', 'SAFE', 'SAIL', 'IBILL'], required: true
    }),
    new FormInput({
      key: 'CCY', colW: 4, type: 'select', selectOptions: ['USD', 'EUR'], required: true
    }),
    new FormInput({
      key: 'AMOUNT', type: 'number', colW: 4, required: true, numberOptions: {min: 0},
      validators: {positive: true}
    }),
    new FormInput({key: 'DIVIDER', colW: 12, type: 'divider'}),
    new FormInput({
      key: 'PORT_OF_LOADING', type: 'autocomplete', colW: 6
    }),
    new FormInput({
      key: 'PORT_OF_DISCHARGE', type: 'autocomplete', colW: 6
    }),
    new FormInput({
      key: 'GOODS_DESCRIPTION', type: 'autocomplete', colW: 6
    }),
    /*new FormInput({
      key: 'TYPE', colW: 4
    }),*/
    new FormInput({
      key: 'QUANTITY', type: 'number', colW: 2, numberOptions: {min: 0},
      validators: {positive: true}
    }),
    new FormInput({
      key: 'UNIT', colW: 2, type: 'select', selectOptions: ['MT', 'KG', 'LT', 'BBL']
    }),
    new FormInput({
      key: 'UNITPRICE', type: 'number', colW: 2, numberOptions: {min: 0, step: 0.01},
      validators: {positive: true}
    }),
    /*new FormInput({
      key: 'UNIT_OF_UNITPRICE', colW: 2, type: 'select', options: ['MT', 'KG', 'LT', 'BBL']
    })*/
  ]
  required = [];
  isFormValid = false;
  transactionApiInput: AmlTransactionApiInput;
  formReady = false;
  goodsDescriptions = [];
  noGoodsDescription = false;

  // transactionProperties = ['MODULE', 'MIN_NO', 'CCIF_NO', 'LC_NUMBER', 'CCY', 'AMOUNT', 'REGISTERED_DATE', 'PORT_OF_LOADING', 'PORT_OF_DISCHARGE', 'GOODS_DESCRIPTION', 'QUANTITY', 'TYPE', 'UNIT', 'UNITPRICE', 'UNIT_OF_UNITPRICE'];
  columns: { name: string, required: boolean, type: 'string' | 'number' | 'date' }[] = [
    {name: 'MODULE', required: true, type: 'string'},
    {name: 'MIN_NO', required: true, type: 'string'},
    {name: 'CCIF_NO', required: true, type: 'string'},
    {name: 'LC_NUMBER', required: true, type: 'string'},
    {name: 'GBASE_REFNO', required: true, type: 'string'},
    {name: 'CCY', required: true, type: 'string'},
    {name: 'AMOUNT', required: true, type: 'number'},
    {name: 'REGISTERED_DATE', required: true, type: 'string'},
    {name: 'PORT_OF_LOADING', required: true, type: 'string'},
    {name: 'PORT_OF_DISCHARGE', required: true, type: 'string'},
    {name: 'GOODS_DESCRIPTION', required: true, type: 'string'},
    {name: 'QUANTITY', required: true, type: 'number'},
    {name: 'TYPE', required: false, type: 'string'},
    {name: 'UNIT', required: true, type: 'string'},
    {name: 'UNITPRICE', required: true, type: 'number'},
    {name: 'UNIT_OF_UNITPRICE', required: true, type: 'string'},
  ]

  constructor(
    private amlApis: AmlApiService,
    private hss: DescriptionFromHsCodeService
  ) {
  }

  inputChange(input: FormInput) {
    // console.log(input);
    input.validate(this.transactionFormValue[input.key]);
    // console.log(input);
    // console.log(this.transactionInputs.filter(ti => ti.error));
    this.isFormValid = !this.transactionInputs.filter(ti => ti.required).filter(ti => !this.transactionFormValue[ti.key]).length && !this.transactionInputs.filter(ti => ti.error).length;
    // console.log(input.key, this.transactionFormValue[input.key], input.valid);
    // this.validateForm();
  }
  submit() {
    this.validateForm();
    if (this.isFormValid) {
      // console.log('form submitted', this.transactionFormValue);
      this.transactionImported.emit([this.transactionFormValue]);
    }
  }
  validateForm() {
    this.transactionInputs.forEach(ti => ti.validate(this.transactionFormValue[ti.key]));
    // console.log(this.transactionInputs.filter(ti => ti.valid === false));
    // console.log(this.transactionInputs.filter(ti => ti.error));
    this.isFormValid = !this.transactionInputs.filter(ti => ti.error).length; // no errors
  }
  /*stillRequired() {
    return this.transactionInputs.filter(ti => ti.required).filter(ti => !this.transactionFormValue[ti.key]).map(ti => ti.label).join(', ');
  }*/
  autoFill() {
    this.transactionFormValue = {
      ...this.transactionFormValue, ...{
        "MODULE": "SHIP",
        "MIN_NO": '20311' + Math.round(Math.random() * 1000),
        "CCIF_NO": '87115' + Math.round(Math.random() * 10),
        "GBASE_REFNO": "ILC749029305",
        "LC_NUMBER": "ILC749029305",
        "CCY": "USD",
        "AMOUNT": Math.round(Math.random() * 10000) + '000',
        "REGISTERED_DATE": '2020-08-' + Math.round(Math.random() * 30),
        "PORT_OF_LOADING": "HONG KONG",
        "PORT_OF_DISCHARGE": "SINGAPORE",
        "GOODS_DESCRIPTION": this.hss.codes[Math.round(Math.random() * 200)].description, // {label: '', description: "ESPO BLEND CRUDE OIL"},
        "QUANTITY":  Math.round(Math.random() * 1000) + '00',
        "UNIT":"MT",
      }
    }
    // this.transactionInputs.forEach(input => input.validate(this.transactionFormValue[input.key]));
    this.validateForm();
  }
  /*searchGoodsDescriptions = (text: Observable<string>) =>
    // [{"inputText":"gasoline","labels":[{"label":"__label__27101227","probability":"0.9924327731132507","isvalidhscode":true}]}]
    text.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap( (searchText) =>  this.amlApis.isHsCodeValid([searchText])),
      map(obj => {
        let array = obj[0].labels;
        array.forEach(item => {
          item.description = this.getDescription(item.label)
        });
        return array;
      }),
    )*/
  searchGoodsDescriptionsLocally = (text: Observable<string>) =>
    // [{"inputText":"gasoline","labels":[{"label":"__label__27101227","probability":"0.9924327731132507","isvalidhscode":true}]}]
    text.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        // : this.hss.codes.filter(v => v.description.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 5))
        : this.hss.codes.map(v => v.description).filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 5))
    )
  getHSCode(term: string) {
    return this.hss.codes.filter(v => v.description.toLowerCase().indexOf(term.toLowerCase()) > -1)[0].label
  }

  probability(x: any) {
    return Math.round(x.probability * 100) + '%'
  }
  inputFormatter(value: any)   {
    return value.description;
  }
  normalStringEntered(val) {
    console.log(val)
  }

  multipleImport(multi: AmlTransactionFormValue[]) {
    // console.log(multi);
    this.transactionImported.emit(multi);
  }
  ngOnInit(): void {
    console.log(this.transactionFormValue);
    if (!this.transactionFormValue) this.transactionFormValue = new AmlTransactionFormValue();
    this.formReady = true;
  }

}
