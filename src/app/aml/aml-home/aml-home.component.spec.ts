import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmlHomeComponent } from './aml-home.component';

describe('AmlHomeComponent', () => {
  let component: AmlHomeComponent;
  let fixture: ComponentFixture<AmlHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmlHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmlHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
