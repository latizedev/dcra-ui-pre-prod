import {Component, OnInit} from '@angular/core';
import {
  faAngleDoubleDown,
  faAngleDoubleUp, faAngleDown, faAngleUp,
  faBookmark, faBuilding, faCheck, faCog,
  faExchangeAlt, faFileUpload, faFilter,
  faFlag, faHandPaper, faStop, faSync, faThumbsDown, faThumbsUp,
  faThumbtack, faTimes
} from '@fortawesome/free-solid-svg-icons';
import {VisualMakerService} from '../../services/visual-maker.service';
import {Title} from '@angular/platform-browser';
import {
  AmlApiOutputDEVH,
  AmlCompany,
  AmlFlag,
  AmlTransaction,
  AmlTransactionApiInput,
  AmlTransactionFormValue
} from '../classes/aml-classes';
import {AmlApiService} from '../Services/aml-api.service';
import {AmlService} from '../Services/aml.service';
import transactionsJson from '../../../assets/transactions/transactions.json';
import transactionTableJson from '../../../assets/transactions/transactions_old.json';

@Component({
  selector: 'app-aml-home',
  templateUrl: './aml-home.component.html',
  styleUrls: ['./aml-home.component.scss']
})
export class AmlHomeComponent implements OnInit {

  chartTimer;

  date = new Date();

  icons = {
    flagged: faFlag,
    monitored: faThumbtack,
    new: faExchangeAlt,
    company: faBuilding,
    saved: faBookmark,
    dblUp: faAngleDoubleUp,
    up: faAngleUp,
    dblDn: faAngleDoubleDown,
    dn: faAngleDown,
    thumbsUp: faThumbsUp,
    cleared: faThumbsUp,
    thumbsDn: faThumbsDown,
    blocked: faHandPaper,
    block: faHandPaper,
    close: faTimes,
    check: faCheck,
    import: faFileUpload,
    load: faSync,
    cog: faCog,
    filter: faFilter
  };
  rotate = 0;
  topMetrics = ['new', 'flagged', 'cleared', 'blocked'];
  topMetricVal = {new: 0, flagged: 0, cleared: 0, saved: 0};

  flagsChart;
  flagsDist = {
    // 'None': 0,
    '01.GINC': 0,
    '02.DEVH': 0,
    '04.HGEO': 0,
    '07.OPCG': 0,
    '10.LGLM': 0,
    '12.TDIS': 0,
    '15.DUAG': 0,
    '16.MLIN': 0
  };
  timelineChart;
  geoChart;

  // allTransactionsFromApi: amlApiOutputDevh[] = [];
  allColumns = ['CCIF_NO', 'MIN_NO', 'MODULE', 'CCY', 'unit_clean', 'category_clean', 'HSCODE1', 'AMOUNT',
    'QUANTITY', 'unit_price', 'median_amt_per_mth', 'median_qty_per_mth', 'median_unit_price_per_mth',
    'percChgAmt_per_mth', 'percChgQty_per_mth',
    'percChgUnit_price_per_mth', 'Risk_Score', 'Flag', 'Reason'];

  columns = ['risk', 'minNo', 'company', 'registeredDate', 'amount',   'module',  'goods', 'flags'];
  columnToLabel = {
    minNo: 'min No',
    registeredDate: 'Registered Date',
    id: 'CCIF'
  }
  transactions: AmlTransaction[] = [
  ]
  showProcessedCount = false;
  // transactionsFromJson: AmlTransactionFormValue[] = transactionsJson;
  filteredTransactions: AmlTransaction[] = [];
  tableFilterStatus = 'ALL';
  page = 1;
  pageSize = 10;

  map = {
    lat: 1.3439166, lng: 103.7540054
  }
  showDataImporter = false;
  comment: string;

  transactionsString;

  showFilter = false;
  filter = {
    status: {
      collapsed: false,
      flagged: true,
      cleared: true,
      unflagged: true
    },
    risk: {
      collapsed: false,
      aggregated: {
        from: 0,
        to: 100,
        floor: 0,
        ceil: 100
      },
      GINC: {
        from: 0,
        to: 100,
        floor: 0,
        ceil: 100
      },
      DEVH: {
        from: 0,
        to: 100,
        floor: 0,
        ceil: 100
      },
    },
    flags: {
      GINC: true,
      DEVH: true,
    },
    flagProps: {
      collapsed: false,
      GINC: {
        hsCode: false,
        recency: false,
      },
      DEVH: {
        'port of loading': false,
        'port of discharge': false,
        amount: false,
        unitprice: false,
        quantity: false,
      }
    },
    amount: {
      collapsed: true,
      from: 0,
      to: 1000,
      floor: 0,
      ceil: 100000
    },
    quantity: {
      collapsed: true,
      from: 0,
      to: 1000,
      floor: 0,
      ceil: 1000
    },
    unitPrice: {
      collapsed: true,
      from: 0,
      to: 1000,
      floor: 0,
      ceil: 1000
    },
  };
  filterReset: string;

  showConfig = true;
  config = {
    weightage: 50,
    ginc: 0,
    devh: 0
  }

  transactionToEdit: AmlTransaction;
  transactionToEditAsForm: AmlTransactionFormValue;

  constructor(
    private vsm: VisualMakerService,
    private titleService: Title,
    private amlApis: AmlApiService,
    private amls: AmlService
  ) {
  }

  updateSummaries() {
    this.topMetrics.forEach(m => {
      this.topMetricVal[m] = this.filteredTransactions.filter(t => t.status === m).length;
    })
    this.topMetricVal.new = this.filteredTransactions.length;
    this.updateFlagsDistribution();
  }

  updateFlagsDistribution() {
    this.flagsDist = {
      // 'None': 0,
      '01.GINC': 0,
      '02.DEVH': 0,
      '04.HGEO': 0,
      '07.OPCG': 0,
      '10.LGLM': 0,
      '12.TDIS': 0,
      '15.DUAG': 0,
      '16.MLIN': 0
    };
    this.filteredTransactions.forEach(t => {
      if (t.flags && t.flags.length) {
        t.flags.forEach(f => {
          this.flagsDist[f.key] += 1
        })
      } else {
        // console.log('no flag');
        this.flagsDist['None'] += 1
      }
    });
    console.log(this.flagsDist);
    this.flagsChart = null;
    this.timelineChart = null;

    this.flagsChart = this.vsm.createAmlPie({
      data: this.flagsDist,
      hoverText: [
        // 'No flags',
        'Goods inconsistency with business strategy',
        'Deviation from historical trades',
        'Higher risk geographical jurisdiction',
        'Overpricing of goods',
        '--',
        '--',
        'Dual use goods',
        'Multiple invoicing'
      ],
      colors: [
        // '#dee2e6',
        '#29357F', '#27ABE2', '#3CAEA3', '#F6D55C',
        '#ED553B', '#753cae', '#c233c0', '#ae3c64']
    });

    clearTimeout(this.chartTimer);
    this.chartTimer = setTimeout(() => {

      this.timelineChart = this.vsm.createAmlTimeline(
        [
          {
            y: this.filteredTransactions.filter(t => t.risk <= 25).map(t => t.amount),
            x: this.filteredTransactions.filter(t => t.risk <= 25).map(t => t.registeredDate),
            text: this.filteredTransactions.filter(t => t.risk <= 25).map(t => Math.round(t.risk) + '% ' + t.currency)
          },
          {
            y: this.filteredTransactions.filter(t => t.risk > 25 && t.risk <= 60).map(t => t.amount),
            x: this.filteredTransactions.filter(t => t.risk > 25 && t.risk <= 60).map(t => t.registeredDate),
            text: this.filteredTransactions.filter(t => t.risk > 25 && t.risk <= 60).map(t => Math.round(t.risk) + '% ' + t.currency)
          },
          {
            y: this.filteredTransactions.filter(t => t.risk > 60).map(t => t.amount),
            x: this.filteredTransactions.filter(t => t.risk > 60).map(t => t.registeredDate),
            text: this.filteredTransactions.filter(t => t.risk > 60).map(t => Math.round(t.risk) + '% ' + t.currency)
          }
        ]
      );

    }, 1)
  }

  reConfig() {
    this.reRunTestFlags();
  }

  filterAll() {
    this.filteredTransactions = this.transactions.filter(t => {
      let risk: any = {};
      let flags: any = {};
      t.flags.forEach(f => {
        risk[f.key] = f.risk
        flags[f.key] = f.breakdown.map(b => b.key);
      });
      let dfArr = Object.keys(this.filter.flagProps.DEVH).filter(k => this.filter.flagProps.DEVH[k]);
      // console.log(dfArr.sort().join().toUpperCase());
      // console.log(flags['02.DEVH'] ? flags['02.DEVH'].sort().join() : '');
      // let flags = t.flags.map(f => f.key);
      return ((t.risk === -1 ? 0 : t.risk) >= this.filter.risk.aggregated.from && t.risk <= this.filter.risk.aggregated.to) &&
        ((t.GINC === -1 ? 0 : t.GINC) >= this.filter.risk.GINC.from && t.GINC <= this.filter.risk.GINC.to) &&
        ((t.DEVH === -1 ? 0 : t.GINC) >= this.filter.risk.DEVH.from && t.DEVH <= this.filter.risk.DEVH.to) &&
        (t.status === 'flagged' ? this.filter.status.flagged : t.status === 'cleared' ? this.filter.status.cleared : this.filter.status.unflagged) &&
        (!this.filter.flags.GINC ? !risk['01.GINC'] : true) &&
        (!this.filter.flags.DEVH ? !risk['02.DEVH'] : true) &&
        (this.filter.flagProps.GINC.hsCode ? risk['01.GINC'] > 31 : true) &&
        (this.filter.flagProps.GINC.recency ? risk['01.GINC'] < 100 : true) &&
        // (Object.keys(this.filter.flagProps.DEVH).filter(k => (k !== 'any') && this.filter.flagProps.DEVH[k]).filter(k =>  (flags['02.DEVH'] ? (flags['02.DEVH'].indexOf(k.toUpperCase()) === -1) : this.filter.flagProps.DEVH.any)).length === 0)
        (dfArr.length ? (flags['02.DEVH'] ? (flags['02.DEVH'].sort().join() === dfArr.sort().join().toUpperCase()) : false) : true)
    });
    this.updateSummaries()
  }
  resetFilter() {
    this.filter = JSON.parse(this.filterReset);
    this.filterAll();
  }
  getProcessed() {
    return this.filteredTransactions.filter(t => t.risk >= 0).length
  }

  updateTotalRiskScoreForTransaction(t: AmlTransaction) {
    /*if (t.flags.length) {
      const riskScores = t.flags.map(f => f.risk);
      t.risk = Math.round((riskScores.reduce((a, b) => a + b) / 2)*100)/100;
    } else {
      t.risk = 0;
    }*/
    t.risk = (t.GINC + t.DEVH) / 2;
    if (t.risk > 100) {
      console.log('this transaction is screwed up');
      console.log(t);
      console.log(JSON.parse(JSON.stringify(t)));

    }
    this.filterAll();
    this.saveToLocal();
    this.transactionsString = JSON.stringify(this.transactions);
  }

  transactionImported(transactions: AmlTransactionFormValue[]) {
    this.showDataImporter = false;

    if (this.transactionToEdit) {
      let i = this.transactions.indexOf(this.transactionToEdit);
      this.transactionToEdit = new AmlTransaction(transactions[0]);
      this.transactions.splice(i, 1, this.transactionToEdit);
      let apiInput = new AmlTransactionApiInput(transactions[0]);
      this.assignFlagsToTransaction(this.transactionToEdit, apiInput);
    } else {
      transactions.forEach(t => {
        const newTransaction = new AmlTransaction(t);
        this.transactions.unshift(newTransaction);
        let apiInput = new AmlTransactionApiInput(t);
        this.assignFlagsToTransaction(newTransaction, apiInput);
      })
      this.filterAll();
    }
    this.transactionToEdit = null;
    this.transactionToEditAsForm = null;
    // this.allTransactionsFromApi.push(...transactions);
  }
  editTransaction(t: AmlTransaction) {
    this.transactionToEdit = t;
    this.transactionToEditAsForm = this.transactionToEdit.getFormValue();
    this.showDataImporter = true;
  }
  assignFlagsToTransaction(t: AmlTransaction, input: AmlTransactionApiInput) {
    t.error = '';
    t.status = null;
    setTimeout(() => {
      this.amlApis.isHsCodeValid(t.goods.map(g => g.description)).subscribe((results: { OUTPUT: { HSCODE: string, PROBABILITY: number, 'VALID HSCODE': boolean }[] }[]) => {
        input.HSCODE = results.length ? ((results[0].OUTPUT && results[0].OUTPUT.length) ? (results[0].OUTPUT[0]['VALID HSCODE'] ? Number(results[0].OUTPUT[0].HSCODE) : null) : null) : null;

        if (input.HSCODE) {
          this.amlApis.hsCodeRecency({
            MODULE: t.module,
            CCIF_NO: Number(t.company.id),
            REGISTERED_DATE: this.amls.getDateTimeStringFromDate(t.registeredDate),
            HSCODE: input.HSCODE
          }).subscribe((recency: {risk_score_hscode_recency: number}) => {
            // risk_score_hscode_recency
            console.log('recency', recency);
            t.assignFlagGINC(results, (recency.risk_score_hscode_recency - this.config.ginc));
          }, error => {
            t.status = 'error';
            t.error += 'Failed to determine recency. ';
            t.assignFlagGINC(results);
            this.updateTotalRiskScoreForTransaction(t);
          })
        } else {
          t.assignFlagGINC(results);
          this.updateTotalRiskScoreForTransaction(t);
        }
        if (!input.HSCODE) input.HSCODE = 0;
        this.amlApis.checkForDEVH(input).subscribe((r: AmlApiOutputDEVH[]) => {
          t.assignFlagDEVH(r[1], this.config.devh);
          this.updateTotalRiskScoreForTransaction(t);
        }, err => {
          t.status = 'error';
          t.error += 'Failed to determine deviation from historical patterns. ';
        });
      }, err => {
        t.status = 'error';
        t.error += 'Failed to match HSCODE. ';
      });
    }, 1000)
  }

  clearLocal() {
    localStorage.removeItem('amlTransactions');
    this.geoChart = null;
    this.timelineChart = null;
    window.location.reload(true);
  }
  saveToLocal() {
    localStorage.setItem('amlTransactions', JSON.stringify(this.transactions));
  }

  reRunTestFlags() {
    this.transactions = [];
    this.filterAll();
    setTimeout(() => {
      transactionTableJson.forEach((t, i) => {
        setTimeout(() => {
          let tInput = new AmlTransactionFormValue();
          tInput.MODULE = t.MODULE;
          tInput.MIN_NO = t.MIN_NO;
          tInput.CCIF_NO = t.CCIF_NO;
          tInput.GBASE_REFNO = t.GBASE_REFNO;
          tInput.LC_NUMBER = t.LC_NUMBER;
          tInput.CCY = t.CCY;
          tInput.AMOUNT = t.AMOUNT;
          tInput.REGISTERED_DATE = t.REGISTERED_DATE;
          tInput.PORT_OF_LOADING = t.PORT_OF_LOADING;
          tInput.PORT_OF_DISCHARGE = t.PORT_OF_DISCHARGE;
          tInput.GOODS_DESCRIPTION = t.GOODS_DESCRIPTION_PDF_Documents || t.GOODS_DESCRIPTION;
          tInput.QUANTITY = t.QUANTITY;
          tInput.UNIT = t.UNIT;
          tInput.UNITPRICE = t.UNITPRICE;
          tInput.UNIT_OF_UNITPRICE = t.UNIT_OF_UNITPRICE;
          this.topMetricVal.new = this.transactions.length;
          this.transactionImported([tInput]);
        }, i*300)
      });
    }, 300)
  }
  loadPresetFlags() {
    transactionsJson.forEach(t => {
      let transaction = new AmlTransaction();
      transaction.getFromLocal(t);
      this.transactions.push(transaction);
    })
    this.filterAll();
    this.saveToLocal();
  }


  ngOnInit(): void {

    this.filterReset = JSON.stringify(this.filter);

    this.transactions = [];
    this.transactions.unshift(...this.amls.getFromLocal());
    this.transactions.forEach(t => {
      if (t.risk === -1) {
        this.assignFlagsToTransaction(t, new AmlTransactionApiInput(t.getFormValue()))
      }
    })

    this.geoChart = null;
    this.timelineChart = null;

    this.titleService.setTitle('ATRiM');

    /**/
    // this.reRunTestFlags();

    /*if (this.transactions.length < 100) {
      this.loadPresetFlags();
    }*/

    // this.saveToLocal();

    // this.transactionImported(transactionsJson as AmlTransactionFormValue[]);

    setTimeout(() => {
      this.filterAll();
      this.geoChart = this.vsm.createAmlMaps();
      // this.timelineChart = this.vsm.createPlaceholderAmlTimeline();
    }, 500)
  }

}


