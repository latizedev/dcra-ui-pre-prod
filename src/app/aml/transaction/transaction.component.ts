import {Component, Input, OnInit} from '@angular/core';
import {AmlTransaction} from '../classes/aml-classes';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

  @Input() transaction: AmlTransaction;

  props = ['module', 'amount', 'risk'];
  goodsProps = ['unitPrice', 'quantity', 'portOfLoading', 'portOfDischarge'];

  active = 1;

  constructor() { }

  ngOnInit(): void {
  }

}
