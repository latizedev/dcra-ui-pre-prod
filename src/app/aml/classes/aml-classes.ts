export class AmlCompany {
  name: string;
  id: string; //UEN
  industry: string;
  ssic1: string;
  ssic2: string;
  products: string[];
  country: string;
  afn: string;
  transactions: AmlTransaction[];
  flags: AmlFlag[];

  constructor(id: string, name?: string) {
    this.id = id;
    this.name = name || id;
    this.industry = null;
    this.ssic1 = null;
    this.ssic2 = null;
    this.products = null;
    this.country = null;
    this.afn = null;
    this.transactions = [];
    this.flags = [];
  }
}

export class AmlGoods {
  type: string;
  description: string;
  portOfLoading: string;
  portOfDischarge: string;
  quantity: number;
  unit: string;
  unitPrice: number;
  currency: string

  /*constructor(g: {
    type: string,
    desciption: string,
    portOfLoading: string,
    portOfDischarge: string,
    quantity: number,
    unit: string,
    unitPrice: number,
    currency: string
  }) {
    this.type = g.type;
    this.desciption = g.desciption;
    this.risk = g.risk;
    this.breakdown = g.breakdown;
  }*/
  constructor(g: AmlTransactionFormValue) {
    this.type = g.TYPE ? g.TYPE.toUpperCase() : undefined;
    this.description = g.GOODS_DESCRIPTION ? g.GOODS_DESCRIPTION.toUpperCase() : undefined; //.description;
    this.portOfLoading = g.PORT_OF_LOADING ? g.PORT_OF_LOADING.toUpperCase() : undefined;
    this.portOfDischarge = g.PORT_OF_DISCHARGE ? g.PORT_OF_DISCHARGE.toUpperCase() : undefined;
    this.quantity = Number(g.QUANTITY);
    this.unit = g.UNIT;
    this.unitPrice = Number(g.UNITPRICE);
    this.currency = g.UNIT_OF_UNITPRICE;
  }
}

export class AmlFlag {
  key: string;
  label: string;
  risk: number;
  breakdown?: AmlFlag[];
  cleared?: boolean;
  comments?: string[];
  files?: string[];

  constructor(f: { key: string, label: string, risk: number, breakdown?: AmlFlag[] }) {
    this.key = f.key;
    this.label = f.label;
    this.risk = f.risk;
    this.breakdown = f.breakdown;
    this.cleared = false;
    this.comments = [];
    this.files = [];
  }
}

export class AmlTransaction {
  minNo: string;
  module: string;
  registeredDate: Date;
  company: AmlCompany;
  amount: number;
  currency: string;
  goods: AmlGoods[];
  risk: number;
  flags: AmlFlag[];
  collapsed?: boolean;
  reporting?: 'block' | 'clear';
  status?: 'blocked' | 'cleared' | 'flagged' | 'error';
  selected?: boolean;
  error?: string;
  GINC: number;
  DEVH: number;

  constructor(input?: AmlTransactionFormValue) {
    if (!input) {
      this.minNo = null;
      this.module = null;
      this.registeredDate = new Date();
      this.company = null;
      this.amount = null;
      this.currency = null;
      this.goods = [];
    } else {
      this.minNo = input.MIN_NO;
      this.module = input.MODULE;
      this.company = new AmlCompany(input.CCIF_NO);
      this.registeredDate = input.REGISTERED_DATE ? new Date(input.REGISTERED_DATE) : undefined;
      this.amount = Number(input.AMOUNT);
      this.currency = input.CCY;
      this.goods = [new AmlGoods(input)];
    }
    this.risk = -1;
    this.flags = [];
    this.collapsed = true;
    this.reporting = null;
    this.status = null;
    this.selected = false;
    this.error = '';
    this.GINC = -1;
    this.DEVH = -1;
  }

  getFromLocal?(t: any) {
    this.minNo = t.minNo;
    this.module = t.module;
    this.registeredDate = new Date(t.registeredDate);
    this.company = t.company;
    this.amount = t.amount;
    this.currency = t.currency;
    this.goods = t.goods;
    this.risk = t.risk;
    this.flags = t.flags;
    this.collapsed = t.collapsed;
    this.reporting = t.reporting;
    this.status = t.status;
    this.error = t.error;
    this.GINC = t.GINC;
    this.DEVH = t.DEVH;
  }

  assignFlagGINC?(apiResults: { OUTPUT: { HSCODE: string, PROBABILITY: number, 'VALID HSCODE': boolean }[] }[], recency?: number, threshold?: number) {
    // console.log('checking for ginc flag', apiResults, recency);
    let childFlags = [];
    this.goods.forEach((g, i) => {
      if (!apiResults[i].OUTPUT.filter(out => out['VALID HSCODE']).length) {
        childFlags.push(new AmlFlag(
          {
            key: g.type || g.description,
            label: 'does not match any valid HS Code description for Customer’s industry',
            risk: 100
          }
        ));
      } else {

      }
      if (recency && recency >= (threshold || 10)) {
        childFlags.push(new AmlFlag(
          {
            key: g.type || g.description,
            label: 'not transacted in more than ' + Math.round(recency / 10) + ' years',
            risk: recency
          }
        ));
      }
    })
    if (childFlags.length) {
      this.flags.push(new AmlFlag(
        {
          key: '01.GINC',
          label: 'Likelihood of goods being traded not matching customer’s business strategy',
          risk: childFlags.map(cf => cf.risk).reduce((a, b) => Math.max(a, b)),
          breakdown: childFlags
        }
      ));
      this.GINC = childFlags.map(cf => cf.risk).reduce((a, b) => Math.max(a, b));
      if (this.status !== 'error') {
        this.status = 'flagged';
      }
    } else {
      this.GINC = 0;
    }
  }

  assignFlagDEVH?(DEVH: AmlApiOutputDEVH, threshold?: number) {

    let childFlags: AmlFlag[] = [];

    DEVH.flags.forEach(f => {
      // console.log('checking for flag_' + f.flag_type);
      childFlags.push(new AmlFlag({
        key: f.flag_type.toUpperCase().split('_').join(' '),
        label: '<b>' + (isNaN(f.flag_value) ? f.flag_value : ((Math.round(f.flag_value * 100)) / 100).toLocaleString('en-US')) +
          '</b> is <b>' + (f.percentage_change ? ((Math.round(f.percentage_change * 100) / 100).toLocaleString() + '% ') : 'significantly ') + '</b>' +
          (f.percentage_change ? ((f.flag_value > f.benchmark) ? 'higher than' : 'lower than') : 'different from') + ' <b>' + (isNaN(f.benchmark) ? f.benchmark : (f.benchmark == 0 ? 'usual values' : ((Math.round(f.benchmark * 100)) / 100).toLocaleString('en-US'))) + '</b>', /* +
            (DEVH['median_' + f.key + '_per_mth'] || 'typical values').toLocaleString('en-US') + '</b>', */
        risk: f.risk
      }))
    });
    if (childFlags.length) {
      let risk = Math.round(DEVH.overall_risk_score * 100) / 100;
      if (risk > (threshold || 0)) {
        this.flags.push(new AmlFlag(
          {
            key: '02.DEVH',
            label: 'Significant deviation from historical trade',
            risk: Math.round(DEVH.overall_risk_score * 100) / 100, // childFlags.map(cf => cf.risk).reduce((a, b) => Math.max(a, b)),
            breakdown: childFlags
          }
        ))
        if (this.status !== 'error') {
          this.status = 'flagged';
        }
      }
    }
    this.DEVH = Number(DEVH.overall_risk_score);
  }

  getFormValue(): AmlTransactionFormValue {
    let value = new AmlTransactionFormValue();
    value.AMOUNT = this.amount.toString();
    value.CCIF_NO = this.company.id;
    value.CCY = this.currency;
    value.GBASE_REFNO = '';
    value.GOODS_DESCRIPTION = this.goods.length ? this.goods[0].description : null;
    value.LC_NUMBER = '';
    value.MIN_NO = this.minNo;
    value.MODULE = this.module;
    value.PORT_OF_DISCHARGE = this.goods.length ? this.goods[0].portOfDischarge : null;
    value.PORT_OF_LOADING = this.goods.length ? this.goods[0].portOfLoading : null;
    value.QUANTITY = (this.goods.length && this.goods[0].quantity) ? this.goods[0].quantity.toString() : null;
    value.REGISTERED_DATE = this.registeredDate ? this.registeredDate.toISOString().split('T')[0] : null;
    value.UNIT = this.goods.length ? this.goods[0].unit : null;
    value.UNITPRICE = (this.goods.length && this.goods[0].unitPrice) ? this.goods[0].unitPrice.toString() : null;
    value.UNIT_OF_UNITPRICE = '';
    if (this.error) {
      value.ERROR = this.error;
    }
    return value;
  }

  clear?() {
    this.status = 'cleared';
  }

  block?() {
    this.status = 'blocked';
  }
}

export class AmlTransactionFormValue {
  MODULE: string;
  MIN_NO: string;
  CCIF_NO: string;
  GBASE_REFNO: string;
  LC_NUMBER: string;
  CCY: string;
  AMOUNT: string;
  REGISTERED_DATE: string;
  PORT_OF_LOADING: string;
  PORT_OF_DISCHARGE: string;
  GOODS_DESCRIPTION: string; // { probability?: number, label: string, description: string };
  TYPE?: string;
  QUANTITY: string;
  UNIT: string;
  UNITPRICE: string;
  UNIT_OF_UNITPRICE: string;
  ERROR?: string;

  constructor() {
    this.MODULE = null;
    this.MIN_NO = null;
    this.CCIF_NO = null;
    this.GBASE_REFNO = null;
    this.LC_NUMBER = null;
    this.CCY = null;
    this.AMOUNT = null;
    this.REGISTERED_DATE = null;
    this.PORT_OF_LOADING = null;
    this.PORT_OF_DISCHARGE = null;
    this.GOODS_DESCRIPTION = null;
    this.TYPE = null;
    this.QUANTITY = null;
    this.UNIT = null;
    this.UNITPRICE = null;
    this.UNIT_OF_UNITPRICE = null;
  }
}

export class AmlTransactionApiInput {
  //'MODULE', 'MIN_NO', 'CCIF_NO', 'LC_NUMBER', 'CCY', 'AMOUNT', 'REGISTERED_DATE', 'PORT_OF_LOADING', 'PORT_OF_DISCHARGE', 'GOODS_DESCRIPTION', 'QUANTITY', 'TYPE', 'UNIT', 'UNITPRICE', 'UNIT_OF_UNITPRICE'
  MODULE: string;
  MIN_NO: number;
  CCIF_NO: number;
  GBASE_REFNO: string;
  LC_NUMBER: string;
  CCY: string;
  AMOUNT: number;
  REGISTERED_DATE: any;
  PORT_OF_LOADING: string;
  PORT_OF_DISCHARGE: string;
  GOODS_DESCRIPTION: string;
  HSCODE: number;
  QUANTITY: string;
  TYPE: string;
  UNIT: string;
  UNITPRICE: string;
  UNIT_OF_UNITPRICE: string;

  constructor(form: AmlTransactionFormValue) {
    console.log(form);
    this.MODULE = form.MODULE ? form.MODULE.toUpperCase() : undefined;
    this.MIN_NO = Number(form.MIN_NO);
    this.CCIF_NO = Number(form.CCIF_NO);
    this.GBASE_REFNO = form.GBASE_REFNO ? form.GBASE_REFNO.toUpperCase() : undefined;
    this.LC_NUMBER = form.LC_NUMBER ? form.LC_NUMBER.toUpperCase() : undefined;
    this.CCY = form.CCY ? form.CCY.toUpperCase() : undefined;
    this.AMOUNT = Number(form.AMOUNT);
    this.HSCODE = null;
    // const arr = form.REGISTERED_DATE.substring(5).split('T');
    //const year = form.REGISTERED_DATE.substring(0,4);
    // arr.splice(1, 0, '-', form.REGISTERED_DATE.substring(0,4), 'T');
    this.REGISTERED_DATE = form.REGISTERED_DATE; //? form.REGISTERED_DATE.split('T').join(' ');
    this.PORT_OF_LOADING = form.PORT_OF_LOADING ? form.PORT_OF_LOADING.toUpperCase() : undefined;
    this.PORT_OF_DISCHARGE = form.PORT_OF_DISCHARGE ? form.PORT_OF_DISCHARGE.toUpperCase() : undefined;
    this.GOODS_DESCRIPTION = form.GOODS_DESCRIPTION ? form.GOODS_DESCRIPTION.toUpperCase() : undefined; //.description;
    this.QUANTITY = form.QUANTITY;
    this.TYPE = form.TYPE ? form.TYPE.toUpperCase() : undefined;
    this.UNIT = form.UNIT ? form.UNIT.toUpperCase() : undefined;
    this.UNITPRICE = form.UNITPRICE;
    this.UNIT_OF_UNITPRICE = form.UNIT_OF_UNITPRICE ? form.UNIT_OF_UNITPRICE.toUpperCase() : undefined;
  }
}

export class AmlApiOutputDEVH {

  /*AMOUNT: 30000000
CCIF_NO: 871150
CCY: "USD"
HSCODE1: "no_HSCODE"
MIN_NO: 20311028
MODULE: "SHIP"
PORT_OF_DISCHARGE: "ONE OR MORE SAFE PORT(S) CHINA"
PORT_OF_LOADING: "PORT KOZMINO, RUSSIAN FEDERATION"
QUANTITY: "1. 20000 2. 20000"
REGISTERED_DATE: "2020-06-11 00:00:00.000000"
UNITPRICE: null
category_clean: "espo blend crude oil"
currency_conversion: null
data_type: "unseen product/missing description"
derived_unit_price: 30000000
flag_amount: "flag_amount"
flag_derived_unit_price: null
flag_quantity: null
flag_user_unitprice: null
flags: " flag_amount  "
index: 0
median_amt_per_mth: null
median_derived_unit_price_per_mth: null
median_qty_per_mth: null
perc_chg_amt_per_mth: null
perc_chg_derived_unit_price_per_mth: null
perc_chg_qty_per_mth: null
perc_chg_user_unitprice: null
recent_user_unitprice: null
risk_score_amount: 99
risk_score_derived_unit_price: null
risk_score_quantity: null
risk_score_user_unitprice: null
*/

  index: number; // 0
  CCIF_NO: number; // 871150
  CCY: string; // "USD"
  MIN_NO: number; // 20311028
  REGISTERED_DATE: string; // "2020-06-11 00:00:00.000000"
  MODULE: string; // "SHIP"
  category_clean: string; // "Crude Petroleum Oil"
  data_type: string; // "historical category,unit,CCY exist"
  HSCODE1: string; // "27090010.0"
  PORT_OF_DISCHARGE: string; // "ONE OR MORE SAFE PORT(S) CHINA"
  PORT_OF_LOADING: string; // "PORT KOZMINO, RUSSIAN FEDERATION"
  unit_clean: string; // "no_unit"

  flags: {
    benchmark: any,
    flag_type: string,
    flag_value: any,
    reason: string,
    risk: number,
    percentage_change: number,
  }[];
  overall_risk_score: number;

  AMOUNT: number; // 30000000
  Flag_amount: string; // null
  median_amt_per_mth: number; // 10687908.6
  percChgAmt_per_mth: number; // 0.64373638
  risk_score_amount: number; // null

  QUANTITY: string; // null
  Flag_quantity: string; // null
  median_qty_per_mth: number; // 180000
  percChgQty_per_mth: number; // null
  risk_score_quantity: number; // null

  unit_price: number; // null
  Flag_unit_price: string; // null
  median_unit_price_per_mth: number; // 59.37727
  percChgUnit_price_per_mth: number; // null
  risk_score_unit_price: number; // null

  Flags: string; // "  "
}

export class amlApiOutputGINC {
  label: string;
  probability: number;
}

/*

'index',  'CCIF_NO',  'MIN_NO',  'MODULE',  'CCY',  'unit_clean',  'category_clean',  'HSCODE1',  'AMOUNT',  'QUANTITY',  'unit_price',  'median_amt_per_mth',  'median_qty_per_mth',  'median_unit_price_per_mth',  'percChgAmt_per_mth: number',  'percChgQty_per_mth: number',
  'percChgUnit_price_per_mth',  'Risk_Score: number',  'Flag: string',  'Reason'
 */
