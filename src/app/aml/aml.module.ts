import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AmlHomeComponent} from './aml-home/aml-home.component';
import {TransactionComponent} from './transaction/transaction.component';
import {AmlCompanyComponent} from './aml-company/aml-company.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {SharedModule} from '../shared/shared.module';
import {SimplebarAngularModule} from 'simplebar-angular';
import { TransactionImportComponent } from './aml-home/transaction-import/transaction-import.component';
import { AmlMenuComponent } from './aml-menu/aml-menu.component';
import { TransactionsTableComponent } from './transactions-table/transactions-table.component';
import {NgxSliderModule} from '@angular-slider/ngx-slider';
import { FlagComponent } from './flag/flag.component';


@NgModule({
  declarations: [
    AmlHomeComponent,
    TransactionComponent,
    AmlCompanyComponent,
    TransactionImportComponent,
    AmlMenuComponent,
    TransactionsTableComponent,
    FlagComponent,
  ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        SharedModule,
        SimplebarAngularModule,
        NgxSliderModule
    ]
})
export class AmlModule {
}
