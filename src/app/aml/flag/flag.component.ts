import {Component, Input, OnInit} from '@angular/core';
import {AmlFlag, AmlTransaction} from '../classes/aml-classes';
import {
  faAngleDoubleDown,
  faAngleDoubleUp, faAngleDown,
  faAngleUp,
  faBuilding, faCheck, faCheckSquare,
  faExchangeAlt,
  faFlag, faHandPaper, faSort, faSortDown, faSortUp, faSquare, faThumbsDown, faThumbsUp,
  faThumbtack, faTimes
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-flag',
  templateUrl: './flag.component.html',
  styleUrls: ['./flag.component.scss']
})
export class FlagComponent implements OnInit {

  @Input() t: AmlTransaction;
  @Input() f: AmlFlag;
  @Input() full = false;
  comment: string;

  icons = {
    flagged: faFlag,
    monitored: faThumbtack,
    new: faExchangeAlt,
    company: faBuilding,
    dblUp: faAngleDoubleUp,
    up: faAngleUp,
    dblDn: faAngleDoubleDown,
    dn: faAngleDown,
    thumbsUp: faThumbsUp,
    cleared: faThumbsUp,
    thumbsDn: faThumbsDown,
    blocked: faHandPaper,
    block: faHandPaper,
    close: faTimes,
    check: faCheck,
    selected: faCheckSquare,
    notSelected: faSquare,
    noSort: faSort,
    sortUp: faSortUp,
    sortDn: faSortDown,
  };

  constructor() { }


  actOnFlag(t: AmlTransaction, flag: AmlFlag) {
    if (t.reporting === 'clear') {
      flag.cleared = true;
      if (t.flags.filter(f => f.cleared).length === t.flags.length) {
        t.status = 'cleared';
      }
    } else if (t.reporting === 'block') {
      t.status = 'blocked'
    }
    let newComment = this.comment ? this.comment.trim() : null;
    if (newComment && newComment.length) {
      flag.comments.push(newComment);
    }
    t.reporting = null;
    this.comment = null;
  }

  ngOnInit(): void {
  }

}
