import {Component, Input, OnInit, Output, EventEmitter, ViewChildren, QueryList, Directive} from '@angular/core';
import {AmlFlag, AmlTransaction} from '../classes/aml-classes';
import {
  faAngleDoubleDown,
  faAngleDoubleUp, faAngleDown, faAngleUp,
  faBookmark,
  faBuilding, faCheck, faCheckSquare,
  faExchangeAlt, faExclamation, faExclamationTriangle, faFileUpload,
  faFlag, faHandPaper, faSort, faSortDown, faSortUp, faSquare, faSync, faThumbsDown, faThumbsUp,
  faThumbtack, faTimes
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-transactions-table',
  templateUrl: './transactions-table.component.html',
  styleUrls: ['./transactions-table.component.scss']
})

export class TransactionsTableComponent implements OnInit {

  @Input() columns: string[];
  @Input() transactions: AmlTransaction[] = []
  columnToLabel = {
    minNo: 'min No',
    registeredDate: 'Date',
    id: 'CCIF',
    risk: 'Aggr. Risk',
    GINC: 'GINC Risk',
    DEVH: 'DEVH Risk'
  }
  page = 1;
  pageSize = 10;

  icons = {
    flagged: faFlag,
    monitored: faThumbtack,
    new: faExchangeAlt,
    company: faBuilding,
    dblUp: faAngleDoubleUp,
    up: faAngleUp,
    dblDn: faAngleDoubleDown,
    dn: faAngleDown,
    thumbsUp: faThumbsUp,
    cleared: faThumbsUp,
    thumbsDn: faThumbsDown,
    blocked: faHandPaper,
    block: faHandPaper,
    close: faTimes,
    check: faCheck,
    selected: faCheckSquare,
    notSelected: faSquare,
    noSort: faSort,
    sortUp: faSortUp,
    sortDn: faSortDown,
    error: faExclamationTriangle
  };

  comment: string;

  @Output() selected = new EventEmitter<AmlTransaction[]>();
  @Output() editThis = new EventEmitter<AmlTransaction>();
  @Input() selectable = false;
  // @Input() selectedTransaction: AmlTransaction;

  sortBy: string;
  sortAscending: boolean;




  constructor() {
  }

  compare = (v1, v2) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

  onSort(column: string) {
    if (this.sortBy === column) {
      this.sortAscending = !this.sortAscending;
    }
    else {
      this.sortBy = column;
      this.sortAscending = false;
    }
    this.transactions.sort((a, b) => {
      let sort;
      if (column === 'flags') {
        // console.log('sort by flags');
        sort = (a.flags.length !== b.flags.length) ? (a.flags.length - b.flags.length) : (a.flags.length ? (a.flags[0].key > b.flags[0].key ? 1 : -1) : 0)
      } else if (column === 'goods') {
        // console.log('sort by goods');
        sort = a.goods[0].description > b.goods[0].description ? 1 : -1
      } else if (column === 'company') {
        // console.log('sort by company');
        sort = a.company.id > b.company.id ? 1 : -1
      } else {
        sort = a[column] > b[column] ? 1 : -1
      }
      return this.sortAscending ? sort : -sort;
    })
    this.page = 0;
  }

  clickError(t: AmlTransaction) {
    this.editThis.emit(t);
  }

  actOnFlag(t: AmlTransaction, flag: AmlFlag) {
    if (t.reporting === 'clear') {
      flag.cleared = true;
      if (t.flags.filter(f => f.cleared).length === t.flags.length) {
        t.status = 'cleared';
      }
    } else if (t.reporting === 'block') {
      t.status = 'blocked'
    }
    let newComment = this.comment ? this.comment.trim() : null;
    if (newComment && newComment.length) {
      flag.comments.push(newComment);
    }
    t.reporting = null;
    this.comment = null;

    this.saveToLocal();
  }

  closeDropdown($event: boolean, t) {
    if ($event) {
      t.commenting = null;
    }
  }

  saveToLocal() {
    localStorage.setItem('amlTransactions', JSON.stringify(this.transactions));
  }

  selectionChange(t: AmlTransaction) {
    // if (this.selectedTransaction !== t) this.selectedTransaction = t
    // else this.selectedTransaction = null;
    t.selected = !t.selected;
    this.selected.emit(this.transactions);
  }

  ngOnInit(): void {
    if (!this.columns) {
      this.columns = ['risk', 'GINC', 'DEVH', 'minNo', 'company', 'registeredDate', 'amount', 'module', 'goods', 'flags', 'status']
    }
  }

}
