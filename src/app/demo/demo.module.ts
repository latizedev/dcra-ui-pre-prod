import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NetworkDemoComponent} from './network-demo/network-demo.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [
    NetworkDemoComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class DemoModule {
}
