import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-network-demo',
  templateUrl: './network-demo.component.html',
  styleUrls: ['./network-demo.component.scss']
})
export class NetworkDemoComponent implements OnInit {

  mode: 'marquee' | 'pan';

  constructor() { }

  changeMode(mode: 'marquee' | 'pan'): void {

  }

  ngOnInit(): void {
  }

}
