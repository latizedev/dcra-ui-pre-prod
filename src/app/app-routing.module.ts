import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GradingUiComponent} from './grading/grading-ui/grading-ui.component';
import {GradingHomeComponent} from './grading/home/grading-home.component';
import {LoginComponent} from './shared/login/login.component';
import {UserService} from './services/user.service';
import {EarlyWarningDashboadComponent} from './early-warning/early-warning-dashboad/early-warning-dashboad.component';
import {NetworkDemoComponent} from './demo/network-demo/network-demo.component';
import {AmlHomeComponent} from './aml/aml-home/aml-home.component';
import {AmlCompanyComponent} from './aml/aml-company/aml-company.component';
import {GlobalHomeComponent} from './shared/global-home/global-home.component';

const routes: Routes = [
  {path: '', component: GradingHomeComponent, canActivate: [UserService]},
  {path: 'login', component: LoginComponent},
  {path: 'cif/:id', component: GradingUiComponent, canActivate: [UserService]},
  {path: 'cif/:id/:overrideId', component: GradingUiComponent, canActivate: [UserService]},
  {path: 'ew/:id', component: EarlyWarningDashboadComponent, canActivate: [UserService]},
  // {path: 'demo/network', component: NetworkDemoComponent},
  // {path: 'aml', component: AmlHomeComponent},
  // {path: 'aml/:id', component: AmlCompanyComponent},
  // {path: 'aml/:id/:min', component: AmlCompanyComponent},
  // {path: '', component: GlobalHomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
