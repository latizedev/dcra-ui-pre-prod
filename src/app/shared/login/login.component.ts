import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {ApiService} from '../../services/api.service';
import {DcraUser} from "../../classes";
import jwtDecode from "jwt-decode";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [ApiService]
})
export class LoginComponent implements OnInit {

  constructor(
    private userManager: UserService,
    private apis: ApiService
  ) {
  }

  username: string;
  password: string;

  login(): void {
    this.userManager.logInAs(this.username);
  }

  serverLogin(): void {
    /*this.apis.getDremioToken().subscribe(r => {
      console.log('dremio login', r)
      // user.dremioToekn =

    })*/
    this.apis.login(this.username, this.password).subscribe(
      (r: {
        result: {
          jwt: string,
          user: { displayName: string, email: string, givenName: string }
        }
      }) => {
        console.log('login success', r);
        let user = new DcraUser(r.result.user, r.result.jwt);
        user.image = 'undefined';
        this.apis.token = r.result.jwt;
        let decoded: {steps: number[]} = jwtDecode(r.result.jwt);
        user.userRole = decoded.steps[0];
        console.log('jwt decoded', decoded);
        this.userManager.loginFromServer(user);
      }, (e) => {
        console.log('login error ', e);
        alert('username or password incorrect');
      });
  }

  ngOnInit(): void {
  }

}
