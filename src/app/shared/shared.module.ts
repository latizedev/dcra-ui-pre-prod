import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {LoginComponent} from './login/login.component';
import {MenuComponent} from './menu/menu.component';
import {GradingHomeComponent} from '../grading/home/grading-home.component';
import {SimpleModalComponent} from './simple-modal/simple-modal.component';
import {PlotlyVisualComponent} from './visuals/visual-trend/plotly-visual.component';
import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import {PlotlyModule} from 'angular-plotly.js';
import {VisualNetworkComponent} from './visuals/visual-network/visual-network.component';
import {GraphModule} from '@swimlane/ngx-graph';
import { DataImporterComponent } from './data-importer/data-importer.component';
import { LoaderComponent } from './loader/loader.component';
import {SimplebarAngularModule} from 'simplebar-angular';
import { CytoNetworkComponent } from './visuals/cyto-network/cyto-network.component';
import {PdfComponent} from './pdf/pdf.component';
import { GlobalHomeComponent } from './global-home/global-home.component';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';

PlotlyModule.plotlyjs = PlotlyJS;

@NgModule({
  declarations: [LoginComponent, MenuComponent, SimpleModalComponent, PlotlyVisualComponent, VisualNetworkComponent, DataImporterComponent, LoaderComponent, CytoNetworkComponent, PdfComponent, GlobalHomeComponent, DynamicFormComponent],
    imports: [
        CommonModule,
        FormsModule,
        FontAwesomeModule,
        NgbModule,
        PlotlyModule,
        GraphModule,
        SimplebarAngularModule
    ],
  exports: [
    FormsModule,
    FontAwesomeModule,
    NgbModule,
    MenuComponent,
    PlotlyVisualComponent,
    VisualNetworkComponent,
    CytoNetworkComponent,
    LoaderComponent,
    DataImporterComponent,
    PdfComponent
  ]
})
export class SharedModule {
}
