import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CytoNetworkComponent } from './cyto-network.component';

describe('CytoNetworkComponent', () => {
  let component: CytoNetworkComponent;
  let fixture: ComponentFixture<CytoNetworkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CytoNetworkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CytoNetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
