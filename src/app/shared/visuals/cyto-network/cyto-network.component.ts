import {Component, OnInit, ViewChild, ElementRef, AfterViewInit, Input} from '@angular/core';
import cytoscape from 'cytoscape';
import cola from 'cytoscape-cola';
import {CytoElement} from '../../../classes';

@Component({
  selector: 'app-cyto-network',
  templateUrl: './cyto-network.component.html',
  styleUrls: ['./cyto-network.component.scss']
})
export class CytoNetworkComponent implements OnInit, AfterViewInit {

  @ViewChild('container') container: ElementRef;

  cy = cytoscape;
  icons = {
    generic: 'star-of-life',
    undefined: 'star-of-life',
    location: 'map-marker-alt',
    product: 'cube',
    transaction: 'exchange-alt',
    company: 'building',
    literal: 'hashtag',
    industry: 'hard-hat'
  };
  containerCss = {
    left: '0px',
    top: '0px',
    right: '0px',
    bottom: '0px'
  }

  @Input() elements: CytoElement;
  @Input() relFinder = false;
  @Input() u = 6; // 1 unit to rescale the network

  textBackground = {
    'text-background-color': '#fff',
    'text-background-opacity': 0.6,
    'text-background-shape': 'rectangle',
    'text-background-padding': this.u * 0.2
  }

  style = [ // the stylesheet for the graph
    {
      selector: '.cluster',
      style: {
        'width': this.u * 5,
        'height': this.u * 5,
        'font-size': this.u * 2,
        'label': 'data(label)',
        'text-wrap': 'wrap',
        'text-margin-y': this.u * 0.4,
        'font-family': 'Roboto, sans-serif',
        'background-color': '#27ABE2',
        'text-valign': 'bottom',
        'text-halign': 'center',
        'background-image': ele => {
          return 'assets/images/icons/cluster/' + (this.icons[ele.data('icon')] || 'generic') + '.png'
        },
        // 'background-fit': 'contain'
        'background-width': this.u * 3,
        'background-height': this.u * 3,
        ...this.textBackground
        // 'background-position-x': '0',
        // 'background-position-y': '0',
      }
    },
    {
      selector: '.cluster.collapsed',
      style: {
        'label': node => node.data('children').nodes.length + ' x ' + node.data('label'),
        'background-color': '#29357F',
        'border-width': this.u * 0.2,
        'border-style': 'solid',
        'border-color': '#29357F',
        'text-margin-y': this.u * 0.4
      }
    },
    {
      selector: '.instance',
      style: {
        'width': this.u * 4,
        'height': this.u * 4,
        'font-size': this.u * 2,
        'label': 'data(label)',
        'text-wrap': 'wrap',
        'text-margin-y': this.u * 0.4,
        'font-family': 'Roboto, sans-serif',
        'background-color': '#ffffff',
        'border-width': this.u * 0.2,
        'border-color': '#27ABE2',
        'text-valign': 'bottom',
        'text-halign': 'center',
        'background-image': ele => {
          return 'assets/images/icons/instance/' + this.icons[ele.data('icon')] + '.png'
        },
        'background-width': this.u * 3,
        'background-height': this.u * 3,
        'font-weight': 'bold',
        ...this.textBackground
      }
    },
    {
      selector: '.instance.origin',
      style: {
        'width': this.u * 6,
        'height': this.u * 6,
        'background-width': this.u * 4,
        'background-height': this.u * 4,
        'font-size': this.u * 2
      }
    },
    {
      selector: '.box',
      style: {
        'label': 'data(label)',
        'font-family': 'Roboto, sans-serif',
        'background-color': '#27ABE2',
        'background-opacity': 0.1,
        'opacity': 1
      }
    },
    {
      selector: 'edge',
      style: {
        'label': 'data(label)',
        'width': this.u * 0.4,
        'font-size': this.u * 1.6,
        'color': '#666',
        'line-color': '#ccc',
        'target-arrow-color': '#ccc',
        'target-arrow-shape': 'triangle',
        'curve-style': 'bezier',
        ...this.textBackground
      }
    }
  ];

  layout = {
    name: 'cola',
    infinite: true,
    fit: false,
    avoidOverlap: true,
    nodeSpacing: node => {
      if (node.data('origin')) {
        return this.u * 8
      } else {
        return this.u * 4
      }
    },
    boundingBox: undefined // {x1: 0, y1: 0, x2: 600, y2: 600}
  }

  graph;
  graphLayout;
  graphLayoutState: 'auto' | 'manual' = 'auto';
  expandedSets = {};
  private resizeTimer: number;

  constructor() {
  }

  createNodesAndEdges(): void {
    this.elements = {
      nodes: [
        {
          data: {id: 'company', label: 'Aviato Pte Ltd', icon: 'company', origin: 'true'},
          classes: ['instance', 'origin'],
          position: {x: 0, y: 0}
        },
        {
          data: {
            id: 'products', label: 'Products', icon: 'product',
            collapsible: true,
            collapsed: true,
            children: {
              nodes: [
                {
                  data: {id: 'productA', label: 'Properties', icon: 'product', parent: 'productsBox'},
                  classes: 'instance'
                },
                {
                  data: {id: 'productB', label: 'Construction\nServices', icon: 'product', parent: 'productsBox'},
                  classes: 'instance'
                },
                {
                  data: {id: 'productC', label: 'Building\nMaterials', icon: 'product', parent: 'productsBox'},
                  classes: 'instance'
                }
              ],
              edges: [
                {
                  data: {id: 'toProductA', source: 'products', target: 'productA'}
                },
                {
                  data: {id: 'toProductB', source: 'products', target: 'productB'}
                },
                {
                  data: {id: 'toProductC', source: 'products', target: 'productC'}
                }
              ]
            }
          },
          classes: ['cluster', 'collapsed']
        },
        {
          data: {
            id: 'industries', label: 'Industries', icon: 'industry',
            collapsible: true,
            collapsed: true,
            children: {
              nodes: [
                {
                  data: {id: 'industryA', label: 'Constructions', icon: 'industry', parent: 'industryBox'},
                  classes: 'instance'
                },
                {
                  data: {id: 'industryB', label: 'Real\nEstate', icon: 'industry', parent: 'industryBox'},
                  classes: 'instance'
                }
              ],
              edges: [
                {
                  data: {id: 'toIndustryA', source: 'industries', target: 'industryA'}
                },
                {
                  data: {id: 'toIndustryB', source: 'industries', target: 'industryB'}
                }
              ]
            }
          },
          classes: ['cluster', 'collapsed']
        },
        {
          data: {
            id: 'affiliations', label: 'Companies', icon: 'company',
            collapsible: true,
            collapsed: true,
            children: {
              nodes: [
                {
                  data: {id: 'aff1', label: 'Giggle Pte Ltd', icon: 'company'},
                  classes: 'instance'
                },
                {
                  data: {id: 'aff2', label: 'Amazin Llc', icon: 'industry'},
                  classes: 'instance'
                }
              ],
              edges: [
                {
                  data: {id: 'toAff1', source: 'affiliations', target: 'aff1'}
                },
                {
                  data: {id: 'toAff2', source: 'affiliations', target: 'aff2'}
                }
              ]
            }
          },
          classes: ['cluster', 'collapsed']
        },
        {
          data: {
            id: 'ccif', label: 'AB1234', icon: 'generic'
          },
          classes: 'instance'
        },
        {
          data: {
            id: 'transactions', label: 'Transactions', icon: 'transaction',
            collapsible: true,
            collapsed: true,
            children: {
              nodes: [
                /*{
                  data: {id: 'transactionBox', label: 'Transactions'},
                  classes: 'box'
                },*/
                {
                  data: {id: 'transaction1', label: '200615 1402', icon: 'transaction', parent: 'transactionBox'},
                  classes: 'instance'
                },
                {
                  data: {id: 'transaction2', label: '200412 1251', icon: 'transaction', parent: 'transactionBox'},
                  classes: 'instance'
                },
                {
                  data: {id: 'transaction3', label: '190106 1811', icon: 'transaction', parent: 'transactionBox'},
                  classes: 'instance'
                }
              ],
              edges: [
                {
                  data: {id: 'toTransaction1', source: 'transactions', target: 'transaction1'}
                },
                {
                  data: {id: 'toTransaction2', source: 'transactions', target: 'transaction2'}
                },
                {
                  data: {id: 'toTransaction3', source: 'transactions', target: 'transaction3'}
                }
              ]
            }
          },
          classes: ['cluster', 'collapsed']
        }
        /*{
          data: {id: 'transactionBox', label: 'Transactions'},
          classes: 'box'
        },*/
      ],
      edges: [
        {
          data: {id: 'toProducts', source: 'company', target: 'products', label: 'developed'}
        },
        {
          data: {id: 'toIndustries', source: 'company', target: 'industries', label: 'part of'}
        },
        {
          data: {id: 'toAffiliations', source: 'company', target: 'affiliations', label: 'affiliated with'}
        },
        {
          data: {id: 'toCcif', source: 'company', target: 'ccif', label: 'has C_CIF'}
        },
        {
          data: {id: 'toTransactions', source: 'company', target: 'transactions', label: 'performed'}
        }
      ]
    };
  }

  createRelNodesAndEdges(): void {
    this.elements = {
      nodes: [
        {
          data: {id: 'company1', label: 'Aviato Pte Ltd', icon: 'company', origin: 'true'},
          classes: ['cluster', 'origin']
        },
        {
          data: {id: 'company2', label: 'Mizuho', icon: 'company', origin: 'true'},
          classes: ['cluster', 'origin']
        },
        {
          data: {id: 'exposure1', label: 'Giggle', icon: 'company'},
          classes: ['instance']
        },
        {
          data: {id: 'exposure2', label: 'Amazin', icon: 'company'},
          classes: ['instance']
        },
        {
          data: {id: 'exposure3', label: 'Pied Piper', icon: 'company'},
          classes: ['instance']
        }
      ],
      edges: [
        {
          data: {id: 'c1e1', source: 'company1', target: 'exposure1', label: '60M'}
        },
        {
          data: {id: 'c1e2', source: 'exposure2', target: 'company1', label: '10M'}
        },
        {
          data: {id: 'c1e3', source: 'company1', target: 'exposure3', label: '20M'}
        },
        {
          data: {id: 'c2e1', source: 'company2', target: 'exposure1', label: '10M'}
        },
        {
          data: {id: 'c2e2', source: 'exposure2', target: 'company2', label: '100M'}
        },
        {
          data: {id: 'c2e3', source: 'company2', target: 'exposure3', label: '30M'}
        }
      ]
    }
  }

  drawGraph(): void {
    console.log('drawing graph')
    this.graph = this.cy({
      container: this.container.nativeElement,
      elements: this.elements,
      style: this.style as any,
      wheelSensitivity: 0.5,
      layout: this.relFinder ? {name: 'preset'} : undefined
    })
    /*this.graph.zoom({
      level: 0.5, // the zoom level
      renderedPosition: { x: 400, y: 400 }
    });*/
    if (!this.relFinder) {
      this.setLayout();
      this.setUpClustering();
    } else {
      console.log(this.graph.nodes())
      this.lockOrigins();
      this.setLayout();
    }
  }

  lockOrigins(): void {
    // console.log(this.graph.nodes('#company1').position(), this.graph.nodes('#company2').position());
    // this.graph.nodes('#company1').position('x', 0);
    // this.graph.nodes('#company2').position('x', 800);
    this.graph.nodes('#company1').lock();
    this.graph.nodes('#company2').lock();
  }

  setUpClustering(): void {
    this.graph.on('tap', 'node', evt => {
      let node = evt.target;
      console.log('tapped', node.data());
      if (node.data('collapsible')) {
        if (node.data('collapsed')) {

          this.graphLayout.stop();

          const childrenObj = node.data('children');
          // childrenObj.nodes.forEach(n => n.position = node.position());

          const children = this.graph.add(childrenObj);
          node.data('collapsed', false);
          node.removeClass('collapsed');
          node.style('label', node.data('label'))
          this.expandedSets[node.data('id')] = children;

          this.setLayout();
          this.graphLayoutState = 'auto';

        } else {

          this.expandedSets[node.data('id')].remove();
          node.data('collapsed', true);
          node.addClass('collapsed');
          node.style('label', node.data('children').nodes.length + ' x ' + node.data('label'))
        }
      }
    });
  }

  setLayout(): void {
    this.graphLayout = this.graph.layout(this.layout);
    this.graphLayout.run();
  }

  addNode(): void {

  }

  setContainerStyle(): void {
    console.log(this.container);
    const w = this.container.nativeElement.offsetWidth;
    const h = this.container.nativeElement.offsetHeight;
    const d = Math.max(w, h);
    this.containerCss = {
      left: (d - w) + 'px',
      top: (h - w) + 'px',
      right: (d - w) + 'px',
      bottom: (h - w) + 'px'
    }
  }

  toggleLayout() {
    if (this.graphLayoutState === 'auto') {
      // this.graphLayoutState = 'manual';
      this.graphLayout.run();
    } else {
      // this.graphLayoutState = 'auto';
      this.graphLayout.stop();
    }
  }

  onResize(): void {
    clearTimeout(this.resizeTimer);
    this.resizeTimer = setTimeout(() => {
      this.drawGraph();
    }, 300);
  }

  ngOnInit(): void {
    this.cy.use(cola);
  }

  ngAfterViewInit(): void {
    this.setContainerStyle();
    if (!this.elements) {
      this.createNodesAndEdges();
      // this.createRelNodesAndEdges();
    }
    this.drawGraph();
  }


}
