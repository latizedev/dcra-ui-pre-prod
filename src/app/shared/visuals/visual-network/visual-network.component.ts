import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Edge, Node} from '@swimlane/ngx-graph';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-visual-network',
  templateUrl: './visual-network.component.html',
  styleUrls: ['./visual-network.component.scss']
})
export class VisualNetworkComponent implements OnInit {

  @Input() network: {
    nodes: Node [],
    links: Edge [];
  };
  @Output() NodeClicked = new EventEmitter<Node>();
  center$: Subject<boolean> = new Subject();
  zoomToFit$: Subject<boolean> = new Subject();

  icons = {
    generic: 'star-of-life',
    Location: 'map-marker-alt',
    Product: 'cube',
    Transaction: 'exchange-alt',
    Company: 'building',
    literal: 'hashtag',
    Industry: 'hard-hat',
  };

  showChildFor: string[] = []; // array of relations for which children should be added

  getNodeType(node: Node): string {
    return node.data.nodeType;
  }

  getNodeRelation(node: Node): string {
    return node.data.relation;
  }

  getNodeClass(node: Node): string {
    return node.data.class;
  }

  centerGraph(): void {
    this.center$.next(true);
  }

  fitGraph(): void {
    this.zoomToFit$.next(true);
  }

  click(node: Node): void {
    this.NodeClicked.emit(node);
  }

  constructor() {
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.centerGraph();
      this.fitGraph();
    }, 500);
  }

}
