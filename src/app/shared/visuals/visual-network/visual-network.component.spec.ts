import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualNetworkComponent } from './visual-network.component';

describe('VisualNetworkComponent', () => {
  let component: VisualNetworkComponent;
  let fixture: ComponentFixture<VisualNetworkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualNetworkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualNetworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
