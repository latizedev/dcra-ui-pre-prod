import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlotlyVisualComponent } from './plotly-visual.component';

describe('VisualTrendComponent', () => {
  let component: PlotlyVisualComponent;
  let fixture: ComponentFixture<PlotlyVisualComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlotlyVisualComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlotlyVisualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
