import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';
import {VisualMakerService} from '../../../services/visual-maker.service';

@Component({
  selector: 'app-plotly-visual',
  templateUrl: './plotly-visual.component.html',
  styleUrls: ['./plotly-visual.component.scss']
})
export class PlotlyVisualComponent implements OnInit, AfterViewInit {

  @ViewChild('chartContainer')
  chartContainer: ElementRef;

  resizeTimer;

  chartReady = false;
  fontSize = 14;

  @Input() chart: {
    data: any,
    layout: any,
  };
  chartToDraw: {
    data: any,
    layout: any,
  };

  @Output() clickEvent = new EventEmitter<{ i: number, points: number[], label: string }[]>();
  @Output() hoverEvent = new EventEmitter<{ i: number, label: string }>();

  clicked(event): void {
    console.log(event);
    this.clickEvent.emit( event.points.map(p => ({
      i: p.pointIndex,
      label:'',
      points: p.pointNumbers
    })));
  }

  hovered(event): void {
    this.hoverEvent.emit({i: event.points[0].pointIndex, label:''});
  }

  unHover(event): void {
    this.hoverEvent.emit(null);
  }

  getFontSize(): number {
    if (window.innerWidth >= 600 && window.innerWidth < 1200) {
      return 8;
    } else if (window.innerWidth >= 1200 && window.innerWidth < 1600) {
      return 12;
    } else if (window.innerWidth >= 1600 && window.innerWidth < 2400) {
      return 16;
    } else if (window.innerWidth >= 2400) {
      return 18;
    } else {
      return 16;
    }
  }

  redrawChart(): void {
    this.chartReady = false;
    this.chart.layout.width = this.chartContainer.nativeElement.offsetWidth;
    this.chart.layout.height = this.chartContainer.nativeElement.offsetHeight;
    this.chart.layout.font = {size: this.getFontSize()};
    setTimeout(() => {
      this.chartReady = true;
    }, 100);
  }

  onResize(): void {
    clearTimeout(this.resizeTimer);
    this.resizeTimer = setTimeout(() => {
      this.redrawChart();
    }, 300);
  }

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.redrawChart();
  }

}
