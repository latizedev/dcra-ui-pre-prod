import {Component, Input, OnInit} from '@angular/core';
import {FormInput} from '../../classes/form-input';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit {

  @Input() inputs: FormInput[];
  @Input() value: any;

  constructor() { }

  ngOnInit(): void {
  }

}
