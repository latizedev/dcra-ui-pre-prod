import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Papa} from 'ngx-papaparse';
import {CompanyData, SimpleNode} from '../../classes';
import {faCalendar, faFont, faHashtag, faTable} from '@fortawesome/free-solid-svg-icons';
import {AmlTransactionFormValue} from '../../aml/classes/aml-classes';

@Component({
  selector: 'app-data-importer',
  templateUrl: './data-importer.component.html',
  styleUrls: ['./data-importer.component.scss']
})
export class DataImporterComponent implements OnInit {
  @Output() companiesImported = new EventEmitter<CompanyData>();
  @Output() transactionsImported = new EventEmitter<AmlTransactionFormValue[]>();
  companyName: string;
  companyCIF: string;
  csvData;
  firstRowIsHeader = true;
  table: {
    columns: SimpleNode[],
    rows: any[]
  };
  showTablePreview = false;
  icons = {
    table: faTable,
    date: faCalendar,
    string: faFont,
    number: faHashtag
  };
  @Input() dataType: 'company' | 'transaction' = 'company';
  @Input() initColumns: { name: string, required: boolean, type: 'string' | 'number' | 'date' }[] = [
    {name: 'cif', required: true, type: 'string'},
    {name: 'company_name', required: false, type: 'string'},
    {name: 'date', required: true, type: 'date'},
    {name: 'fiscal_year', required: false, type: 'number'},
    {name: 'revenue', required: true, type: 'number'},
    {name: 'cash_flow', required: true, type: 'number'},
    {name: 'operating_cash_flow', required: true, type: 'number'},
    {name: 'ebitda', required: true, type: 'number'},
    {name: 'liabilities', required: true, type: 'number'},
    {name: 'shares', required: true, type: 'number'},
    {name: 'dow_jones', required: true, type: 'number'},
    {name: 'index_rsi', required: true, type: 'number'},
    {name: 'index_macd', required: true, type: 'number'},
    {name: 'exposure_amount', required: true, type: 'number'},
    {name: 'exposure_source', required: true, type: 'string'},
    {name: 'borrowed_amount', required: true, type: 'number'},
    {name: 'borrowed_amount_repaid', required: true, type: 'number'},
    {name: 'borrowing_purpose', required: true, type: 'string'},
    {name: 'news_title', required: false, type: 'string'},
    {name: 'news_url', required: false, type: 'string'},
    {name: 'news_blurb', required: false, type: 'string'},
    {name: 'relation_type', required: false, type: 'string'},
    {name: 'relation_target', required: false, type: 'string'},
    {name: 'country_debt_to_gdp', required: false, type: 'string'},
    {name: 'country_unemployment', required: false, type: 'string'},
  ];
  remainingColumns: { name: string, required: boolean, type: 'string' | 'number' | 'date' }[] = [];

  constructor(
    private papa: Papa
  ) {
  }

  getCSV(event?): void {
    if (event) {
      this.csvData = event.target.files[0];
    }
  }

  parseFile(): void {
    this.showTablePreview = false;
    /*this.table = {
      columns: [],
      rows: []
    };*/
    console.log(this.csvData);
    this.papa.parse(this.csvData, {
        header: this.firstRowIsHeader,
        preview: 200,
        /*worker: true,
        step: (results) => {
          console.log(results);
          if (results.meta.fields && results.meta.fields.length) {
            this.table.columns = results.meta.fields.map(f => {
              return {
                type: 'string',
                label: f,
                id: (this.initColumns.map(c => c.name).indexOf(f) > -1) ? f : null
              };
            });
            this.table.rows.push(results.data);
          }
          this.showTablePreview = true;
        },*/
        complete: (results, file) => {
          this.table = {
            columns: results.meta.fields.map(f => {
              return {
                type: 'string',
                label: f,
                id: (this.initColumns.map(c => c.name).indexOf(f) > -1) ? f : null
              };
            }),
            rows: results.data
          };
          if (this.dataType === 'company') {
            this.table.columns.sort((a, b) => this.initColumns.map(c => c.name).indexOf(b.id));
            this.companyName = this.table.rows[0].company_name;
            this.companyCIF = this.table.rows[0].cif;
          }
          if (this.dataType === 'transaction') {
            if (!this.table.columns.filter(c => c.id ==='GOODS_DESCRIPTION').length) {
              this.table.columns.push({id: 'GOODS_DESCRIPTION', label: 'GOODS_DESCRIPTION', type: 'string'})
            }
            this.table.rows = this.table.rows.filter(r => r.MIN_NO);
            this.table.rows.forEach(r => {
              let gdCols = Object.keys(r).filter(c => c.includes('GOODS_DESCRIPTION'));
              r.GOODS_DESCRIPTION = gdCols.map(c => r[c]).reduce((existing, value) => {
                if (!existing) return value;
              })
            });
            this.table.columns.sort((a, b) => this.initColumns.map(c => c.name).indexOf(b.id));

          }
          this.showTablePreview = true;
          console.log('completed');
          console.log(this.table);
          this.getRemainingColumns();
          if (this.csvData.name === 'data_template.csv') {
            this.remainingColumns = [];
          }
        }
      }
    );
  }

  assignColumn(column: SimpleNode, assign): void {
    console.log('assign: ' + column, assign);
    column.id = assign;
    this.getRemainingColumns();
  }

  getRemainingColumns(): void {
    const assignedColumns = this.table.columns.map(c => c.id);
    this.remainingColumns = this.initColumns.filter(c => !assignedColumns.includes(c.name));
  }

  importData(): void {
    if (this.dataType === 'company') {
      const company: CompanyData = {
        cif: this.companyCIF,
        fiscalYear: 2020,
        fiscalMonth: 12,
        name: this.companyName,
        flagged: true,
        gradings: {
          final: undefined,
          previous: '1.PASS',
          recommended: '1.PASS',
          review: undefined
        },
        comments: [],
        currentExposure: undefined,
        financialStanding: undefined,
        financialTrend: [],
        gradingReview: [],
        gradingTrend: undefined,
        industryTrend: [],
        latestFinancials: [],
        news: [],
        ratingTrend: undefined,
        relations: [],
        totalRiskExposure: {},
      };
      this.companiesImported.emit(company);
    }
    if (this.dataType === 'transaction') {
      this.transactionsImported.emit(this.table.rows);
    }
  }
  noneRequired(): boolean {
    return this.remainingColumns.filter(c => c.required).length === 0;
  }
  ngOnInit(): void {
    this.remainingColumns = this.initColumns;
  }

}
