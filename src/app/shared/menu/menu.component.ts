import {Component, OnInit} from '@angular/core';
import {faBars, faBuilding, faFlag, faList, faUserCircle} from '@fortawesome/free-solid-svg-icons';
import {faFlag as faFlag2} from '@fortawesome/free-regular-svg-icons';
import {UserService} from '../../services/user.service';
import {DataService} from '../../services/data.service';
import {CompanyData, DcraUser, SimpleNode} from '../../classes';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  open = false;
  icons = {
    companies: faBuilding,
    list: faBars,
    user: faUserCircle,
    flag: faFlag,
    flag2: faFlag2
  };
  activeUser: DcraUser;
  lists: {
    initial: CompanyData[];
    review: CompanyData[];
    final: CompanyData[];
  };
  list: CompanyData[];
  showList: string;

  constructor(
    private um: UserService,
    private ds: DataService
  ) {
  }

  logOut(): void {
    this.um.logOut();
  }

  ngOnInit(): void {
    this.activeUser = this.um.getActiveUser();
    // this.ds.updateWithLocalData();
    this.list = this.ds.getCompanies();
    // this.lists.initial = this.ds.getCompaniesByStatus('R1');
    // this.lists.review = this.ds.getCompaniesByStatus('R2');
    // this.lists.final = this.ds.getCompaniesByStatus('R3');
  }

}
